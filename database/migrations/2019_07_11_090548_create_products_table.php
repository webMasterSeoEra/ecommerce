<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('lang_id')->nullable();
            $table->string('slug')->unique(); 
            $table->enum('status',['yes','no'])->default('yes');
            $table->text('seo')->nullable();
            $table->integer('sort')->default(0);
            $table->bigInteger('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->bigInteger('sub_cat_id')->unsigned()->nullable();
            $table->bigInteger('size_id')->unsigned()->nullable();
            $table->string('fit')->nullable();
            $table->bigInteger('company_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('tags')->nullable();
            $table->string('img');
            $table->string('img2')->nullable();
            $table->double('price'); 
            $table->integer('offer')->nullable();
            $table->integer('stock')->default(10);
            $table->text('small_desc')->nullable();
            $table->text('desc')->nullable();
            $table->integer('views')->default(70); 
            $table->integer('selll_number')->default(123); 
            $table->enum('special',['yes','no'])->default('no'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
