<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippWaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipp_ways', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('lang_id')->nullable();
            $table->string('slug')->unique()->nullable();
            $table->enum('status',['yes','no'])->default('yes');
            $table->text('seo')->nullable();
            $table->string('name');
            $table->text('desc')->nullable();
            $table->string('img')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipp_ways');
    }
}
