<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('lang_id')->nullable();
            $table->string('slug')->unique(); 
            $table->enum('status',['yes','no'])->default('yes');
            $table->text('seo')->nullable();
            $table->enum('type',['info','service'])->default('info');
            $table->string('name')->nullable(); 
            $table->text('desc'); 
            $table->text('small_desc')->nullable(); 
            $table->string('img')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_pages');
    }
}
