<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned()->index();
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->string('address')->nullable();
            $table->string('name')->nullable();
            $table->string('mobile')->nullable();
            $table->string('bulding_number')->nullable(); 
            $table->string('street_name')->nullable(); 
            $table->string('city_name')->nullable(); 
            $table->string('district_name')->nullable(); 
            $table->enum('payment_method',['cash','online']);  
            $table->enum('status',['pending','shipping','accepted','refused']);  
            $table->text('notes')->nullable();
            $table->text('refused_notes')->nullable();
            $table->integer('cupon_id')->nullable();
            $table->integer('additional_value_now')->nullable();
            $table->integer('percent_now')->nullable();
            $table->integer('total_price')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
