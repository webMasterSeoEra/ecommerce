<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;


class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Setting();
        $data->name             = "بيت الحكمة";
        $data->logo1            = "logo.png";
        $data->email            = "info@info.com";
        $data->mobile1          = "01025236599";
        $data->facebook         = "https://www.facebook.com/";
        $data->instagram        = "https://www.facebook.com/";
        $data->twitter          = "https://www.facebook.com/";
        $data->youtube          = "https://www.facebook.com/";
        $data->save();
    }
}
