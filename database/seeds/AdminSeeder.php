<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;


class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Admin();
        $data->name = "mostafa";
        $data->email = "admin@ar.com";
        $data->password = bcrypt("123456");
        $data->save();

    }
}
