<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Category();
        $data->name = " منتجات خاصة  ";
        $data->slug = slug("منتجات خاصة");
        $data->save();



        $data = new Category();
        $data->name = "  اغذية صحية  ";
        $data->slug = slug("اغذية صحية");
        $data->save();




        $data = new Category();
        $data->name = "  عناية وتجميل  ";
        $data->slug = slug("عناية وتجميل");
        $data->save();



        $data = new Category();
        $data->name = "  أعشاب وعطارة  ";
        $data->slug = slug("أعشاب وعطارة");
        $data->save();



        $data = new Category();
        $data->name = "  زيوت طبيعية  ";
        $data->slug = slug("زيوت طبيعية");
        $data->save();
    }
}
