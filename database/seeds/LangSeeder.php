<?php

use Illuminate\Database\Seeder;
use App\Models\Lang;

class LangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Lang();
        $data->name = "arabic";
        $data->slug = "ar";
        $data->save();
    }
}
