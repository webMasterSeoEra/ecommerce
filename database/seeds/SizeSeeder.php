<?php

use Illuminate\Database\Seeder;
use App\Models\Size;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Size();
        $data->name=" جرام ";
        $data->save();



        $data = new Size();
        $data->name=" كيلو ";
        $data->save();



        $data = new Size();
        $data->name=" عبوة محددة ";
        $data->save();

        $data = new Size();
        $data->name=" متر ";
        $data->save();


        $data = new Size();
        $data->name=" باوند ";
        $data->save();
    }
}
