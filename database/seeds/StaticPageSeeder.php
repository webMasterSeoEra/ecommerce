<?php

use Illuminate\Database\Seeder;
use App\Models\StaticPage;

class StaticPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new StaticPage();
        $data->type = "info";
        $data->img = "1.jpg";
        $data->name = "عن روح الطبيعة";
        $data->slug = slug("عن روح الطبيعة");
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->save();


        $data = new StaticPage();
        $data->type = "info";
        $data->img = "1.jpg";
        $data->name = " سياسة الشحن والتوصيل";
        $data->slug = slug(" سياسة الشحن والتوصيل");
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->save();



        $data = new StaticPage();
        $data->type = "info";
        $data->img = "1.jpg";
        $data->name = " سياسة الخصوصية";
        $data->slug = slug(" سياسة الخصوصية");
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->save();


        $data = new StaticPage();
        $data->type = "info";
        $data->img = "1.jpg";
        $data->name = "شروط الاستخدام";
        $data->slug = slug("شروط الاستخدام");
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->save();



        $data = new StaticPage();
        $data->type = "info";
        $data->img = "1.jpg";
        $data->name = "سياسية الاستبدال والاسترجاع";
        $data->slug = slug("سياسية الاستبدال والاسترجاع");
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->save();





        //  services 


        $data = new StaticPage();
        $data->type = "service";
        $data->img = "1.jpg";
        $data->name = " الإستشارات";
        $data->slug = slug("الإستشارات");
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->save();


        $data = new StaticPage();
        $data->type = "service";
        $data->img = "1.jpg";
        $data->name = " إرجاع الطلب";
        $data->slug = slug("إرجاع الطلب");
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->save();



        $data = new StaticPage();
        $data->type = "service";
        $data->img = "1.jpg";
        $data->name = "خريطة الموقع";
        $data->slug = slug("خريطة الموقع");
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->save();















    }
}
