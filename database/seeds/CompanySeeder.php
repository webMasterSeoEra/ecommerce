<?php

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Company();
        $data->name = "الامل للتجارة ";
        $data->img = "1.png";
        $data->slug = slug("الامل للتجارة");
        $data->save();



        $data = new Company();
        $data->name = " سيو ايرا ";
        $data->img = "1.png";
        $data->slug = slug("سيو ايرا");
        $data->save();



        $data = new Company();
        $data->name = "  المتحدة للتوريدات ";
        $data->img = "1.png";
        $data->slug = slug("المتحدة للتوريدات");
        $data->save();



        $data = new Company();
        $data->name = " جرين فوودز ";
        $data->img = "1.png";
        $data->slug = slug("جرين فوودز");
        $data->save();


        $data = new Company();
        $data->name = "  مكانك  ";
        $data->img = "1.png";
        $data->slug = slug(" مكانك ");
        $data->save();


        $data = new Company();
        $data->name = " يونيليفر ";
        $data->img = "1.png";
        $data->slug = slug("يونيليفر");
        $data->save();

    }
}
