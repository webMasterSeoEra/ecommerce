<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {




        $data = new Product();
        $data->category_id = "1";
        $data->sub_cat_id = "1";
        $data->size_id = rand(1,3);
        $data->name = "المنتج الرابع ";
        $data->tags = "المنتج الرابع ";
        $data->slug = slug("المنتج الرابع");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->save();


        $data = new Product();
        $data->category_id = "1";$data->size_id = rand(1,3);
        $data->sub_cat_id = "1";
        $data->name = "المنتج الخامس ";
        $data->tags = "المنتج الخامس ";
        $data->slug = slug("المنتج الخامس");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->save();



        $data = new Product();
        $data->category_id = "1";$data->size_id = rand(1,3);
        $data->sub_cat_id = "1";
        $data->name = "المنتج السادس ";
        $data->tags = "المنتج السادس ";
        $data->slug = slug("المنتج السادس");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->save();




        $data = new Product();
        $data->category_id = "1";$data->size_id = rand(1,3);
        $data->sub_cat_id = "1";
        $data->name = "المنتج السابع عشر ";
        $data->tags = "المنتج السابع عشر ";
        $data->slug = slug("المنتج السابع عشر");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->save();



        $data = new Product();
        $data->category_id = "1";$data->size_id = rand(1,3);
        $data->sub_cat_id = "1";
        $data->name = "المنتج السادس عشر  ";
        $data->tags = "المنتج السادس عشر  ";
        $data->slug = slug("المنتج السادس عشر ");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->save();





        $data = new Product();
        $data->category_id = "1";$data->size_id = rand(1,3);
        $data->sub_cat_id = "1";
        $data->name = "المنتج الخامس عشر  ";
        $data->tags = "المنتج الخامس عشر  ";
        $data->slug = slug("المنتج الخامس عشر ");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->save();



        $data = new Product();
        $data->category_id = "1";$data->size_id = rand(1,3);
        $data->sub_cat_id = "1";
        $data->name = "المنتج الرابع عشر ";
        $data->tags = "المنتج الرابع عشر ";
        $data->slug = slug("المنتج الرابع عشر");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->size_id = rand(1,5);
        $data->save();








        












        $data = new Product();
        $data->category_id = "2";
        $data->name = "المنتج الاول ";
        $data->tags = "المنتج الاول ";
        $data->slug = slug("المنتج الاول");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->save();


        $data = new Product();
        $data->category_id = "2";
        $data->name = "المنتج الثانى ";
        $data->tags = "المنتج الثانى ";
        $data->slug = slug("المنتج الثانى");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->size_id = rand(1,5);
        $data->save();



        $data = new Product();
        $data->category_id = "2";
        $data->name = "المنتج الثالث ";
        $data->tags = "المنتج الثالث ";
        $data->slug = slug("المنتج الثالث");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->size_id = rand(1,5);
        $data->save();












        $data = new Product();
        $data->category_id = "3";
        $data->name = "المنتج السابع ";
        $data->tags = "المنتج السابع ";
        $data->slug = slug("المنتج السابع");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->company_id = "1";
        $data->size_id = rand(1,5);
        $data->save();


        $data = new Product();
        $data->category_id = "3";
        $data->name = "المنتج الثامن ";
        $data->tags = "المنتج الثامن ";
        $data->slug = slug("المنتج الثامن");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->company_id = "1";    
        $data->save();





        $data = new Product();
        $data->category_id = "4";
        $data->name = "المنتج التاسع ";
        $data->tags = "المنتج التاسع ";
        $data->slug = slug("المنتج التاسع");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->company_id = "1";
        $data->save();


        $data = new Product();
        $data->category_id = "4";
        $data->name = "المنتج العاشر ";
        $data->tags = "المنتج العاشر ";
        $data->slug = slug("المنتج العاشر");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->company_id = "1";
        $data->size_id = rand(1,5);
        $data->save();










        $data = new Product();
        $data->category_id = "5";
        $data->name = "المنتج الحادى عشر ";
        $data->tags = "المنتج الحادى عشر ";
        $data->slug = slug("المنتج الحادى عشر");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->company_id = "1";
        $data->save();


        $data = new Product();
        $data->category_id = "5";
        $data->name = "المنتج الثانى عشر ";
        $data->tags = "المنتج الثانى عشر ";
        $data->slug = slug("المنتج الثانى عشر");
        $data->img= rand(1,5).".jpg";
        $data->img2= rand(1,5).".jpg";
        $data->price = rand(1,50);
        $data->desc = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ";
        $data->views = rand(1,50);
        $data->selll_number	 = rand(1,50);
        $data->special = "yes";
        $data->company_id = "1";
        $data->size_id = rand(1,5);
        $data->save();







    }
}
