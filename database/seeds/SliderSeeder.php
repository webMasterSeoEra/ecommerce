<?php

use Illuminate\Database\Seeder;
use App\Models\Slider;


class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Slider();
        $data->name = " بيت الحكمة للعطارة ";
        $data->small_desc = " هناك حقيقة مثبتة ";
        $data->link = "https://www.facebook.com/";
        $data->img = "1.jpg";
        $data->save();


        $data = new Slider();
        $data->name = " بيت الحكمة للعطارة ";
        $data->small_desc = " هناك حقيقة مثبتة ";
        $data->link = "https://www.facebook.com/";
        $data->img = "2.jpg";
        $data->save();



        $data = new Slider();
        $data->name = " بيت الحكمة للعطارة ";
        $data->small_desc = " هناك حقيقة مثبتة ";
        $data->link = "https://www.facebook.com/";
        $data->img = "3.jpg";
        $data->save();
    }
}
