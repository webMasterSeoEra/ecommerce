<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LangSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(SizeSeeder::class);
        $this->call(CatSeeder::class);
        $this->call(SubCatSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(StaticPageSeeder::class);
        $this->call(BranchesSeeder::class);
    }
}
