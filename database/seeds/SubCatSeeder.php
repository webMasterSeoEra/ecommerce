<?php

use Illuminate\Database\Seeder;
use App\Models\SubCat;


class SubCatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new SubCat();
        $data->category_id = "1";
        $data->name = "قهوة";
        $data->slug = slug("قهوة");
        $data->save();


        $data = new SubCat();
        $data->category_id = "1";
        $data->name = "زيوت واعشاب";
        $data->slug = slug("زيوت واعشاب");
        $data->save();



        $data = new SubCat();
        $data->category_id = "1";
        $data->name = "اعشاب صحية";
        $data->slug = slug("اعشاب صحية");
        $data->save();



        $data = new SubCat();
        $data->category_id = "1";
        $data->name = "تركيبات";
        $data->slug = slug("تركيبات");
        $data->save();


        $data = new SubCat();
        $data->category_id = "1";
        $data->name = "بذر كتان بنى";
        $data->slug = slug("بذر كتان بنى");
        $data->save();


        $data = new SubCat();
        $data->category_id = "1";
        $data->name = "كورن فلكس شوكلاته";
        $data->slug = slug("كورن فلكس شوكلاته");
        $data->save();

        $data = new SubCat();
        $data->category_id = "1";
        $data->name = "";
        $data->slug = slug("الوسمة الفاخرة");
        $data->save();



    }
}
