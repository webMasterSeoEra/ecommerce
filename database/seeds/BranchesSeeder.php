<?php

use Illuminate\Database\Seeder;
use App\Models\Branch;

class BranchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Branch();
        $data->name = " الفرع الاول  ";
        $data->mobile1 = "009666584521";
        $data->map = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6902.722358716533!2d31.3411865!3d30.1124768!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145815dcc3e4e599%3A0x6c67c22e4aa39206!2sMasr+Al+Jadidah%2C+Al+Matar%2C+El+Nozha%2C+Cairo+Governorate!5e0!3m2!1sen!2seg!4v1563805196056!5m2!1sen!2seg" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>';
        $data->address = "الرياض - حي الروضة - شارع خالد بن الوليد - أسواق التميمي";
        $data->save();


        $data = new Branch();
        $data->name = " الفرع الثانى  ";
        $data->mobile1 = "009666584521";
        $data->map = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6902.722358716533!2d31.3411865!3d30.1124768!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145815dcc3e4e599%3A0x6c67c22e4aa39206!2sMasr+Al+Jadidah%2C+Al+Matar%2C+El+Nozha%2C+Cairo+Governorate!5e0!3m2!1sen!2seg!4v1563805196056!5m2!1sen!2seg" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>';
        $data->address = "الرياض - حي الروضة - شارع خالد بن الوليد - أسواق التميمي";
        $data->save();




        $data = new Branch();
        $data->name = " الفرع الثالث  ";
        $data->mobile1 = "009666584521";
        $data->map = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6902.722358716533!2d31.3411865!3d30.1124768!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145815dcc3e4e599%3A0x6c67c22e4aa39206!2sMasr+Al+Jadidah%2C+Al+Matar%2C+El+Nozha%2C+Cairo+Governorate!5e0!3m2!1sen!2seg!4v1563805196056!5m2!1sen!2seg" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>';
        $data->address = "الرياض - حي الروضة - شارع خالد بن الوليد - أسواق التميمي";
        $data->save();



        $data = new Branch();
        $data->name = " الفرع الرابع  ";
        $data->mobile1 = "009666584521";
        $data->map = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6902.722358716533!2d31.3411865!3d30.1124768!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145815dcc3e4e599%3A0x6c67c22e4aa39206!2sMasr+Al+Jadidah%2C+Al+Matar%2C+El+Nozha%2C+Cairo+Governorate!5e0!3m2!1sen!2seg!4v1563805196056!5m2!1sen!2seg" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>';
        $data->address = "الرياض - حي الروضة - شارع خالد بن الوليد - أسواق التميمي";
        $data->save();



        $data = new Branch();
        $data->name = " الفرع الخامس  ";
        $data->mobile1 = "009666584521";
        $data->map = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6902.722358716533!2d31.3411865!3d30.1124768!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145815dcc3e4e599%3A0x6c67c22e4aa39206!2sMasr+Al+Jadidah%2C+Al+Matar%2C+El+Nozha%2C+Cairo+Governorate!5e0!3m2!1sen!2seg!4v1563805196056!5m2!1sen!2seg" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>';
        $data->address = "الرياض - حي الروضة - شارع خالد بن الوليد - أسواق التميمي";
        $data->save();
    }
}
