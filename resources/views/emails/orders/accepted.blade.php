<!DOCTYPE html>
 <html lang="zxx">

    
<head>

    	<!-- Metas -->
        <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name="keywords" content="HTML5 Template Coco onepage themeforest" />
		<meta name="description" content="Coco - Onepage Multi-Purpose HTML5 Template" />
		<meta name="author" content="" />

		<!-- Title  -->
		<title>بيت الحكمة</title>

    </head>

    <body>
<table style="width:650px;margin:0 auto;border:5px solid #ddd;line-height:1.8">
      <tbody>
          <tr>
              <td style="text-align:center;background:#f5f5f5">
				  <font face="Tahoma">
					<img src="{{getImg(SETTINGS_PATH.$set->logo1)}}" class="CToWUd a6T" tabindex="0">
						<div class="a6S" dir="rtl" style="opacity: 0.01; left: 300px; top: 163px;">
							<div aria-label="" data-tooltip-class="a1V" data-tooltip="">
								<div class="aSK J-J5-Ji aYr"></div>
							</div>
						</div>	
					</font>
				</td>
          </tr>
          <tr>
              <?php $order = $data['order']; ?>
              <td style="padding:15px">
                  <p style="text-align:right">
					  <font face="Tahoma"><span style="background-color:transparent">&nbsp;</span>
                        <span style="background-color:transparent"> : مرحباً</span>
						<strong style="background-color:transparent"> {{$order->client->name}} </strong>
					  </font>
                  </p>
                  


                  <hr style="margin-top:50px;"> 

                    <div style="text-align: center;
                        display: block;
                        width: 100%;
                        margin: auto;
                        margin-top: 50px;
                        border: 2px solid #222;
                        padding: 10px;
                        border-radius: 10px;
                        background-color: #70AE06;
                        color: #fff;
                        text-decoration: none;
                        font-size: 20px;">
                                    
                                    {{ $data['code'] }}   تم استلام الطلب رقم 
                                    
                    </div>

                    <hr style="margin-top:50px;"> 






                  <div dir="rtl" style="color:rgb(34,34,34);font-family:Arial,Helvetica,sans-serif;font-size:small">
					<font face="Tahoma">   لقد قمت بطلب هذه المنتجات من متجر بيت الحكمة    :</font>
                  </div>
                  



                  <div style="text-align:center"><font face="Tahoma"><br></font></div>
                 
				 <table >
                      <tbody>
                          <tr>
                              <td>
							  <font face="Tahoma">
								<table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
									<thead>
										<tr>
											<td style="width:50px;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:center;padding:7px;color:#222222">Image</td>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Product</td>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Quantity</td>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">Price</td>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">Total</td>
										</tr>
									</thead>
									<tbody>
                                        @foreach($data['orderContent'] as $item)
										<tr>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:center;padding:7px">
                                                <img src="{{getImg(PRODUCT_PATH.$item->product->img)}}" alt="{{$item->product->name}}" class="CToWUd">
                                            </td>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px"> {{\Str::Words($item->product->name)}}   </td>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">{{$item->qty}}</td>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">{{$item->price}} SR </td>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">{{$item->price * $item->qty}} SR </td>
                                        </tr>
                                        @endforeach
									</tbody>
								</table>
                                <br>
                                <hr>
                                
 
                    
                    
							  </font>
							  </td>
                          </tr>
                      </tbody>
                  </table>



                


                  
            

				  
                  <div style="text-align:center;clear:both;direction: rtl;"><font face="Tahoma"><br></font></div>
				  
                  <p><font face="Tahoma"><br class="m_1106356198092140559Apple-interchange-newline"></font></p>
				  
                  <div style="text-align:right">
					<span style="color:rgb(34,34,34);font-size:small;text-align:start">
						<font face="Tahoma">إذا كنت تواجه مشكلة في إكمال طلبك يسرنا مساعدتك .</font>
					</span>
				  </div>
  
                  
                  <div style="text-align:right"><font face="Tahoma"><br></font></div>			
                  <div style="text-align:right"><font face="Tahoma"><br></font></div>			
                

				  
				  
                  <div style="text-align:right"><font face="Tahoma"><br></font></div>
                  <div style="text-align:right"><font face="Tahoma"><br></font></div>
				  
                  <p style="text-align:center">
					<font face="Tahoma">
                        <a href="{{route('front.get.client.page',['myorders'])}}" style="text-decoration:none;background:rgb(112,174,6);color:rgb(255,255,255);padding:10px 50px;border:2px solid rgb(112,174,6);font-size:15px;font-weight:bold;text-transform:uppercase" target="_blank">
                            اضغط هنا لعرض الطلبات 
                        </a>
						<br>
					</font>
				  </p>
				  
                  <p style="text-align:right"><span style="color:rgb(34,34,34);font-size:small"><font face="Tahoma"><br></font></span></p>				  
                  <p style="text-align:right"><span style="color:rgb(34,34,34);font-size:small"><font face="Tahoma"><br></font></span></p>
				  
				  
                  <h2 style="font-family:&quot;Open Sans&quot;,sans-serif;color:rgb(113,172,6);font-size:24px;text-align:center;margin-top:20px!important"><b><font face="Tahoma"><br>تواصل معنا</font></b></h2>
				  
                  <h5 style="font-family:&quot;Open Sans&quot;,sans-serif;color:rgb(134,151,126);margin-top:8.5px;font-size:16px;text-align:center"><b><font face="Tahoma">يسعدنا تواصلك معنا على الهاتف</font></b></h5>
                  <h1 style="font-size:35px;font-family:&quot;Open Sans&quot;,sans-serif;color:rgb(33,45,11);text-align:center;margin-top:20px!important">
                  <font face="Tahoma">
                     {{$set->mobile1}}
                    </font></h1>
                  <p style="text-align:right"><font face="Tahoma"><br></font></p>
              </td>
          </tr>
      </tbody>
  </table>

    </body>

</html>