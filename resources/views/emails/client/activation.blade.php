<!DOCTYPE html>
 <html lang="zxx">

    
<head>

    	<!-- Metas -->
        <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name="keywords" content="HTML5 Template Coco onepage themeforest" />
		<meta name="description" content="Coco - Onepage Multi-Purpose HTML5 Template" />
		<meta name="author" content="" />

		<!-- Title  -->
		<title>بيت الحكمة</title>

    </head>

    <body>
		<table style="width:650px;margin:0 auto;border:5px solid #ddd;line-height:1.8">
			<tbody>
				<tr>
					<td style="text-align:center;background:#f5f5f5">
						<font face="Tahoma">
							<img src="{{getImg(SETTINGS_PATH.$set->logo1)}}" class="CToWUd a6T" tabindex="0">
								<div class="a6S" dir="rtl" style="opacity: 0.01; left: 300px; top: 163px;">
									<div aria-label="" data-tooltip-class="a1V" data-tooltip="">
										<div class="aSK J-J5-Ji aYr"></div>
									</div>
								</div>	
							</font>
						</td>
				</tr>
				
			</tbody>
		</table>


		<div>
			<a href="{{route('front.get.home.activation',[$data['token']])}}" style="text-align: center;
			display: block;
			width: 250px;
			margin: auto;
			margin-top: 50px;
			border: 2px solid #222;
			padding: 10px;
			border-radius: 10px;
			background-color: #72d07c;
			color: #fff;
			text-decoration: none;
			font-size: 20px;">
					تفعيل الحساب 
			</a>
		</div>

		<hr style="margin-top:50px;"> 

		<h2 style="font-family:&quot;Open Sans&quot;,sans-serif;color:rgb(113,172,6);font-size:24px;text-align:center;margin-top:20px!important"><b><font face="Tahoma"><br>تواصل معنا</font></b></h2>
				  
		<h5 style="font-family:&quot;Open Sans&quot;,sans-serif;color:rgb(134,151,126);margin-top:8.5px;font-size:16px;text-align:center"><b><font face="Tahoma">يسعدنا تواصلك معنا على الهاتف</font></b></h5>
		<h1 style="font-size:35px;font-family:&quot;Open Sans&quot;,sans-serif;color:rgb(33,45,11);text-align:center;margin-top:20px!important">
		<font face="Tahoma">
			{{$set->mobile1}}
		</font>
		</h1>

    </body>

</html>