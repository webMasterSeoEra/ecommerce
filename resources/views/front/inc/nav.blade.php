
<!-- Navigation
================================================== -->
<div class="main_menu" id="main_menu" >
	<div class="container">
		<div class="col-md-12 p-0">
			<a href="#menu" class="menu-trigger"><i class="fa fa-bars"></i> @lang('front.menu') </a>
			<nav id="navigation">
				<ul class="menu" id="responsive">
					<li><a href="{{route('front.get.home.index')}}" class="current homepage" id="current">@lang('front.home')</a></li>
					
                    
                    @foreach($cats as $cat)
                        @if(count($cat->sub))
                        <li class="dropdown">
                            <a href="{{route('front.get.cat.showCat',[$cat->slug])}}"> {{$cat->name}}  </a>
                            <ul>
                                @foreach($cat->sub as $sub)
                                <li><a href="{{route('front.get.cat.showSubCat',[$sub->slug])}}">{{$sub->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        @else
                        <li class="demo-button"><a href="{{route('front.get.cat.showCat',[$cat->slug])}}"> {{$cat->name}}  </a></li>
                        @endif
                    @endforeach
                    
                    
					<?php /* ?><li class="demo-button"><a href="{{route('front.get.home.about')}}"> @lang('front.whoUs') </a></li><?php */ ?>
					<li class="demo-button"><a href="{{route('front.get.home.contact')}}">  @lang('front.contactUs') </a></li>

				</ul>
			</nav>
		</div>
	</div>
</div>
