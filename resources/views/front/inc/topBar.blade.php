<!-- Top Bar
================================================== -->
<div id="top-bar">
	<div class="container">

		<!-- Top Bar Menu -->
		<div class="col-md-6 p-0">
			<div id="additional-menu">
				<ul>
					@if(!client())
					<li><a href="{{route('front.get.authClient.login')}}"> {{json_data($sc,'topBaritem9')}} </a></li>
					<li><a href="{{route('front.get.authClient.register')}}">{{json_data($sc,'topBaritem10')}} </a></li>
					@else  
					<li>{{json_data($sc,'topBaritem1')}} {{client()->name}} </li>
					<li><a href="{{route('front.get.client.account')}}"> {{json_data($sc,'topBaritem2')}}</a></li>
					<li><a href="{{route('front.get.client.page',['myorders'])}}"> {{json_data($sc,'topBaritem3')}}</a></li>
					<li><a href="{{route('front.get.client.page',['myfavorits'])}}"> {{json_data($sc,'topBaritem4')}} </a></li>
					<li><a href="{{route('front.get.authClient.logout')}}"> {{json_data($sc,'topBaritem5')}} </a></li>
					@endif
					
				</ul>
			</div>
			<!-- <ul class="top-bar-menu">
				<li>
					<div class="top-bar-dropdown">
						<span>English</span>
						<ul class="options">
							<li><div class="arrow"></div></li>
							<li><a href="#">English</a></li>
							<li><a href="#">Polish</a></li>
							<li><a href="#">Deutsch</a></li>
						</ul>
					</div>
				</li>
				<li>
					<div class="top-bar-dropdown">
						<span>USD</span>
						<ul class="options">
							<li><div class="arrow"></div></li>
							<li><a href="#">USD</a></li>
							<li><a href="#">PLN</a></li>
							<li><a href="#">EUR</a></li>
						</ul>
					</div>
				</li>
			</ul> -->
		</div> 

		<!-- Social Icons -->
		<div class="col-md-6 p-0">
			<ul class="social-icons">
					<li><a class="twitter" href="{{$set->twitter}}"><i class="icon-twitter"></i></a></li>
					<li><a class="facebook" href="{{$set->facebook}}"><i class="icon-facebook"></i></a></li>
					<li><a class="pinterest" href="{{$set->pinterest}}"><i class="icon-pinterest"></i></a></li>
					<li><a class="instagram" href="{{$set->instagram}}"><i class="icon-instagram"></i></a></li>
			</ul>
		</div>
		
	</div>
</div>

<div class="clearfix"></div>


<!-- Header
================================================== -->

<div class="social_me">
    
	<a href="{{$set->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>
	<a href="{{$set->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a>
	<a  href="{{$set->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a>
	<a  href="{{$set->pinterest}}" target="_blank"><i class="fa fa-pinterest"></i></a>
	<a  href="{{$set->youtube}}" target="_blank"><i class="fa fa-youtube"></i></a>
	
</div>
<div class="container">


	<!-- Logo -->
	<div class="col-md-4 col-xs-6 p-0">
		<div id="logo">
			<h1>
				<a href="{{route('front.get.home.index')}}">
					<img src="{{getImg(SETTINGS_PATH.$set->logo1)}}" alt="Trizzy" />
				</a>
			</h1>
		</div>
	</div>


	<!-- Additional Menu -->
	<div class="col-md-8 col-xs-6 p-0">



		<!-- Shopping Cart -->
		<div class="col-md-12 col-xs-12 p-0">

			<div id="cart">
				<!-- Button -->
				
				<div class="cart-btn">
					<a href="#" class="button adc">@lang('front.namePrice') {{Cart::getSubTotal()}}</a>
				</div>
				<div class="cart-list">

					<div class="arrow"></div>

					<div class="cart-amount">
						<span> @lang('front.prodInCart') {{Cart::getContent()->count()}} </span>
					</div>
					@if(Cart::getContent()->count())
					<ul>
						@foreach(Cart::getContent() as $item)
						<li>
							<a href="#"><img src="{{getImg(PRODUCT_PATH.$item->attributes->img)}}" alt="" style="height:55px !important;" /></a>
							<a href="#">{{\Str::Words($item->name)}}</a>
							<span>@lang('front.namePrice') {{$item->quantity}} x {{\Str::Words($item->price)}} </span>
							<div class="clearfix"></div>
						</li>
						@endforeach

					</ul>

					<div class="cart-buttons button">
						<a href="{{route('front.get.cart.show')}}" class="view-cart" ><span data-hover="View Cart"><span>
						{{json_data($sc,'topBaritem7')}}
						</span></span></a>
						<a href="{{route('front.get.checkout.show')}}" class="checkout"><span data-hover="Checkout">{{json_data($sc,'topBaritem8')}}</span></a>
					</div>
					@endif

					<div class="clearfix">

					</div>
				</div>

			</div>

			<!-- Search -->
			<nav class="top-search">
				<form action="{{route('front.get.product.search')}}" method="get">
					<button><i class="fa fa-search"></i></button>
					<input class="search-field autosearch" autocomplete="off" type="text" name="search" placeholder="{{json_data($sc,'topBaritem6')}}" />
					<ul class="content-autosearch"></ul>
				</form>
			</nav>

		</div>


	</div>



</div>
