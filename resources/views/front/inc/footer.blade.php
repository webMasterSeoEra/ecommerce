

	<!-- Footer
    ================================================== -->
	<div id="footer">

		<!-- Container -->
		<div class="container">

			<div class="four columns">

				<!-- Headline -->
				<h3 class="headline footer"> {{json_data($sc,'footeritem4')}}   </h3>
				<span class="line"></span>
				<div class="clearfix"></div>

				<ul class="footer-links">
					<li><a href="{{route('front.get.client.account')}}">  {{json_data($sc,'footeritem7')}}  </a></li>
					<li><a href="{{route('front.get.client.page',['myorders'])}}"> {{json_data($sc,'footeritem8')}} </a></li>
					<li><a href="{{route('front.get.client.page',['myfavorits'])}}">  {{json_data($sc,'footeritem9')}} </a></li>
				</ul>

			</div>

			<div class="four columns">

				<!-- Headline -->
				<h3 class="headline footer">   {{json_data($sc,'footeritem3')}}  </h3>
				<span class="line"></span>
				<div class="clearfix"></div>

				<ul class="footer-links">
					@foreach($staticInfo as $sp)
						<li><a href="{{route('front.get.home.site',[$sp->slug])}}">  {{ $sp->name }} </a></li>
					@endforeach
					<li><a href="{{route('front.get.home.cond')}}" >  @lang('front.policy') </a></li>

				</ul>

			</div>

			<div class="four columns">

				<!-- Headline -->
				<h3 class="headline footer">  {{json_data($sc,'footeritem2')}}  </h3>
				<span class="line"></span>
				<div class="clearfix"></div>

				<ul class="footer-links">
					<li><a href="{{route('front.get.home.contact')}}">  @lang('front.contactUs') </a></li>
					@foreach($staticService as $sp)
						<li><a href="{{route('front.get.home.site',[$sp->slug])}}">  {{ $sp->name }} </a></li>
					@endforeach
					<li><a href="{{route('front.home.get.branches')}}"> @lang('front.branches') </a></li>

				</ul>

			</div>

			<div class="four columns">
				<!-- Headline -->
				<h3 class="headline footer">  {{json_data($sc,'footeritem1')}}</h3>
				<span class="line"></span>
				<div class="clearfix"></div>
				<p>{{json_data($sc,'footeritem5')}}</p>

				<form  method="post" id="subscribe-form">
					<button class="newsletter-btn btn-submit" type="submit">{{json_data($sc,'footeritem6')}}</button>
					<input class="newsletter sub-input" name="email"   type="email" required placeholder="mail@example.com" value=""/>
					@csrf
				</form>
			</div>

		</div>
		<!-- Container / End -->

	</div>
	<!-- Footer / End -->

	<!-- Footer Bottom / Start -->
	<div id="footer-bottom">

		<!-- Container -->
		<div class="container">

			<div class="eight columns">© Copyright 2019 by <a href="#">Seoera</a>. All Rights Reserved.</div>
			<div class="eight columns">

				<ul class="payment-icons">
					<li class="text-light"> {{$set->email}} -  </li>
					<li class="text-light">  - {{$set->mobile1}} </li>
				</ul>



				<!-- <ul class="payment-icons">
					<li><img src="{{furl()}}/images/visa.png" alt="" /></li>
					<li><img src="{{furl()}}/images/mastercard.png" alt="" /></li>
					<li><img src="{{furl()}}/images/skrill.png" alt="" /></li>
					<li><img src="{{furl()}}/images/moneybookers.png" alt="" /></li>
					<li><img src="{{furl()}}/images/paypal.png" alt="" /></li>
				</ul> -->
			</div>

		</div>
		<!-- Container / End -->

	</div>
	<!-- Footer Bottom / End -->



	<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div>


<!-- Java Script
================================================== -->
<script data-cfasync="false" src="{{furl()}}/scripts/email-decode.min.js"></script>
<script src="{{furl()}}/scripts/jquery-1.11.0.min.js"></script>
<script src="{{furl()}}/scripts/jquery-migrate-1.2.1.min.js"></script>
<script src="{{furl()}}/scripts/bootstrap.min.js"></script>

<script src="{{furl()}}/scripts/jquery.jpanelmenu.js"></script>
<script src="{{furl()}}/scripts/jquery.themepunch.plugins.min.js"></script>
<script src="{{furl()}}/scripts/jquery.themepunch.revolution.min.js"></script>
<script src="{{furl()}}/scripts/jquery.themepunch.showbizpro.min.js"></script>
<script src="{{furl()}}/scripts/jquery.magnific-popup.min.js"></script>
<script src="{{furl()}}/scripts/hoverIntent.js"></script>
<script src="{{furl()}}/scripts/superfish.js"></script>
<script src="{{furl()}}/scripts/jquery.pureparallax.js"></script>
<script src="{{furl()}}/scripts/jquery.pricefilter.js"></script>
<script src="{{furl()}}/scripts/jquery.selectric.min.js"></script>
<script src="{{furl()}}/scripts/jquery.royalslider.min.js"></script>
<script src="{{furl()}}/scripts/SelectBox.js"></script>
<script src="{{furl()}}/scripts/modernizr.custom.js"></script>
<script src="{{furl()}}/scripts/waypoints.min.js"></script>
<script src="{{furl()}}/scripts/jquery.flexslider-min.js"></script>
<script src="{{furl()}}/scripts/jquery.counterup.min.js"></script>
<script src="{{furl()}}/scripts/jquery.tooltips.min.js"></script>
<script src="{{furl()}}/scripts/jquery.isotope.min.js"></script>
<script src="{{furl()}}/scripts/puregrid.js"></script>
<script src="{{furl()}}/scripts/stacktable.js"></script>
<script src="{{furl()}}/scripts/custom.js"></script>


<!-- sweet alert  -->
<script src="{{furl()}}/js/sweetalert2@8.js"></script>
<script>
	
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

//    subscribe form 

    $("#subscribe-form").submit(function(e) 
    {
         e.preventDefault();
        var formData  = new FormData(jQuery('#subscribe-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.message.subscribe')}}",
           data:formData,
           contentType: false,
           processData: false,
			beforeSend:function()
			{
				$(".btn-submit").attr("disabled","disabled");
				
				
			},
           success:function(data)
           {
				Swal.fire(
				{
					position: 'top-end',
					type: 'success',
					title: "{{trans('front.msg.newsletterSuccess')}}",
					showConfirmButton: false,
					timer: 2000
				})
				$(".btn-submit").removeAttr("disabled");
				$(".sub-input").val("");
             
           },
            error: function(xhr, status, error) 
            {

              $.each(xhr.responseJSON.errors, function (key, item) 
              {
					Swal.fire(
					{
						position: 'top-end',
						type: 'error',
						title: " " + item + " ",
						showConfirmButton: false,
						timer: 2000
					})
              });
			  $(".btn-submit").removeAttr("disabled");
            }

        });

  	});







// start auto search 

//  get data of search from database

	$(".autosearch").keyup(function(){

		var el = $(this);
		$.ajax({
		type:'GET',
		url:"{{route('front.get.home.autosearch')}}",
		data:{name:el.val()},
		success:function(data)
		{
			$(".content-autosearch").css('display','block');
			$(".content-autosearch").html('');
			$(".content-autosearch").html(data)
		
		}
		})
		

	});


//  put vaue from auto search content to input search value
	$(document).on("click",'.value-autoSearch',function(){

		$(".autosearch").val($(this).attr("data-value"));
	});

	  

//  remove auto search container if user clicked out box
	$(document).on("click",'body',function(e){
		
		if($(".content-autosearch").is(":visible")){

			if(e.target.className !== "content-autosearch")
			{
				$(".content-autosearch").hide();
			}
		
		}
		
	});





//  end auto search 
</script>


<script>


//  make nave bar  fixed at the top of window

	$(document).ready(function() {
		// grab the initial top offset of the navigation 
		var stickyNavTop = $('#main_menu').offset().top;
		var stickyNav = function(){
			var scrollTop = $(window).scrollTop(); // our current vertical position from the top
			if (scrollTop > stickyNavTop) { 
					$('#main_menu').addClass('sticky');
			} else {
					$('#main_menu').removeClass('sticky'); 
			}
		};

		stickyNav();
		// and run it again every time you scroll
		$(window).scroll(function() {
			stickyNav();
		});
	});



</script>




@yield('script')







</body>
</html>