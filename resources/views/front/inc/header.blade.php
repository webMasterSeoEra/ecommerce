<!DOCTYPE html>

<html lang="en">

<head>

<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title>bytalhekma</title>
@yield('seo')

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->

<link rel="stylesheet" href="{{furl()}}/css/bootstrap_rtl.css">
<link rel="stylesheet" href="{{furl()}}/css/colors/green.css" id="colors">
<link rel="stylesheet" href="{{furl()}}/css/style_rtl.css">
<link rel="stylesheet" href="{{furl()}}/css/helper.css">
<link rel="stylesheet" href="{{furl()}}/css/font/font.css">
<link rel="stylesheet" href="{{furl()}}/css/end.css">

<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


@yield('style')

</head>

<body>
<div id="wrapper">
