
<script>






$(document).on("click",".addToCart",function(e)
{

    var el = $(this);
    var prodId = el.attr("data-prod-id")
    if($("#qtyProd").length)
    { var qty = $("#qtyProd").val(); }
    else {var qty = 1; }
    $.ajax({
        type: "GET",
        url: "{{ route('front.get.cart.add') }}",
        data:{prodId:prodId,qty:qty},
        cache: false,
        beforeSend:function()
        {
            el.html("جارى الاضافة الى السلة")
        },
        success: function (data) 
        {
            el.html(" اضافة الى السلة ")
            if(data.message)
            {
                Swal.fire(
				{
					position: 'center',
					type: 'error',
					title: data.message,
					showConfirmButton: false,
					timer: 2000
				})
            }
            else  
            {
                Swal.fire(
                {
                    position: 'top-end',
                    type: 'success',
                    title: "{{trans('front.msg.cartSuccess')}}",
                    showConfirmButton: false,
                    timer: 2000
                })
                $("#cart").html(data);
            }
            // $(".cart-list").html(data)
            

        },error: function (data) 
        {
            
        }
    });
    e.preventDefault;




});





</script>