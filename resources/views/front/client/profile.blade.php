@extends('front.main')

@section('content')


<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

		<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
		<div class="parallax-overlay"></div>

		<div class="container">
			<div class="parallax-content">

				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{route('front.get.home.index')}}">@lang('front.home')</a></li>
						<li>@lang('front.myaccount')</li>
					</ul>
				</nav>


			</div>
		</div>
	</section>


	<div class="container">
		<div class="row">
			@include('front.client.inc.sideBar')
			<div class="col-md-9 col-xs-12">
				<div class="det_profile_page">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th> {{json_data($sc,'profileitem8')}}</th>
								<td>  {{client()->name}}  </td>
							</tr>
							<tr>
								<th> {{json_data($sc,'profileitem9')}}</th>
								<td>{{client()->email}} </td>
							</tr>

							<tr>
								<th> {{json_data($sc,'profileitem10')}}</th>
								<td>{{client()->mobile}}  </td>
							</tr>

							<tr>
								<th> {{json_data($sc,'profileitem11')}}</th>
								<td>{{client()->phone}}  </td>
							</tr>

							<tr>
								<th> {{json_data($sc,'profileitem12')}}</th>
								<td>{{client()->address}} </td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>



@endsection


