@extends('front.main')


@section('content')


<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

		<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
		<div class="parallax-overlay"></div>

		<div class="container">
			<div class="parallax-content">

				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{route('front.get.home.index')}}">@lang('front.home')</a></li>
						<li>@lang('front.myaccount')</li>
					</ul>
				</nav>


			</div>
		</div>
	</section>


	<div class="container">
		<div class="row">
            @include('front.client.inc.sideBar')
			<div class="col-md-9 col-xs-12">
				<div class="det_profile_page">
                    @if(Request::segment('2') == 'edit-information')
                        @include('front.client.inc.editInfo')
                    @endif

                    @if(Request::segment('2') == 'change-password')
                        @include('front.client.inc.changePassword')
                    @endif

                    @if(Request::segment('2') == 'edit-address')
                        @include('front.client.inc.editAddress')
                    @endif

                    @if(Request::segment('2') == 'myorders')
                        @include('front.client.inc.myorders')
					@endif
					
					@if(Request::segment('2') == 'myfavorits')
                        @include('front.client.inc.myfavorits')
					@endif
					@if(Request::segment('2') == 'reverced')
                        @include('front.client.inc.reverced')
                    @endif


				</div>
			</div>
		</div>
	</div>



@endsection



@if(Request::segment('2') == 'edit-information')
	@section('script')

		@include('front.client.ajax.editInfo')

	@endsection
@endif



@if(Request::segment('2') == 'change-password')
	@section('script')

		@include('front.client.ajax.changePassword')

	@endsection
@endif




@if(Request::segment('2') == 'edit-address')
	@section('script')

		@include('front.client.ajax.editAddress')

	@endsection
@endif




@if(Request::segment('2') == 'myorders')
	@section('script')

	<script>
        
        $(".delete-item").click(function(){

            if (confirm('هل انت متأكد من الغاء الطلب  ! ')) 
            {
                
            } 
            else 
            {
                return false;
            }
        })
    
    </script>

	@endsection
@endif

