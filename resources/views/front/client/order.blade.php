@extends('front.main')

@section('style')

<link  rel="stylesheet" href="{{furl()}}/css/rating.css" >

@endsection
@section('content')

<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

    <img src="{{furl()}}/images/parallax-2.jpg" alt="" />
    <div class="parallax-overlay"></div>

    <div class="container">
        <div class="parallax-content">

            <nav id="breadcrumbs">
                <ul>
                    <li><a href="#">الرئيسية</a></li>
                    <li>رقم الطلب : {{$row->id }}</li>
                </ul>
            </nav>


        </div>
    </div>
</section>




<div class="container">
		<div class="row">
		
			<div class="col-md-12 col-xs-12">
				<div class="cart">
					<!-- Cart -->
					<table class="cart-table responsive-table">
						<thead class="head_table">
						<tr>
							<th>{{json_data($sc,'orContentitem1')}}</th>
							<th> {{json_data($sc,'orContentitem2')}}</th>
							<th> {{json_data($sc,'orContentitem3')}}</th>
							<th>{{json_data($sc,'orContentitem4')}}</th>
							<th style="text-align: right"> {{json_data($sc,'orContentitem5')}}</th>
							@if($row->status == "accepted")
							<th style="text-align: right"> {{json_data($sc,'orContentitem12')}} </th>
							@endif
						</tr>
						</thead>

                        <!-- Item #1 -->
                        <?php $price=0; ?>
                        @foreach($products as $prod)
                            @if($prod->product) 
                            <tr>
                                <td><img src="{{getImg(PRODUCT_PATH.$prod->product->img)}}" style="height:70px;" alt=""/></td>
                                <td class="cart-title">
                                
                                    {{$prod->product->name}}
                                
                                </td>
                                <td>@lang('front.namePrice') {{$prod->price}}</td>
                                <td>
                                    
                                    <input type='text' name="quantity" value='{{$prod->qty}}'  disabled style="width: 50px">
                                    
                                </td>
								<td class="cart-total"> @lang('front.namePrice') {{$prod->price * $prod->qty}}</td>
								@if($row->status == "accepted")
								<td>
									<fieldset class="rating">
										<label class="rate @if($prod->product->rating->avg('rate') >= 5) gold @endif " for="star5" data-val="5" title="5 stars">5 stars</label>
										<label class="rate @if($prod->product->rating->avg('rate') >= 4) gold @endif" for="star4" data-val="4" title="4 stars">4 stars</label>
										<label class="rate @if($prod->product->rating->avg('rate') >= 3) gold @endif" for="star3" data-val="3" title="3 stars">3 stars</label>
										<label class="rate @if($prod->product->rating->avg('rate') >= 2) gold @endif" for="star2" data-val="2" title="2 stars">2 stars</label>
										<label class="rate @if($prod->product->rating->avg('rate') >= 1) gold @endif" for="star1" data-val="1" title="1 stars">1 star</label>
										<input type="hidden" value="{{$prod->product_id}}" >
									</fieldset>
								</td>
								@endif
                            </tr>
                            <?php $price +=$prod->price * $prod->qty; ?>
                            @endif
                        @endforeach
						


					</table>




					<!-- Cart Totals -->

					<div class="col-md-6 col-xs-12 cart-totals p-0">
						<table class="cart-table margin-top-5">

							<tr>
								<th>{{json_data($sc,'orContentitem6')}}</th>
								<td><strong>@lang('front.namePrice') {{$price}}</strong></td>
							</tr>

							<tr>
								<th> {{json_data($sc,'orContentitem7')}}</th>
								<td>@lang('front.namePrice') {{$set->additional_value}}</td>
							</tr>


							@if($row->cupon)
							<tr>
								<th> {{json_data($sc,'orContentitem11')}}</th>
								<td> {{$row->percent_now}} % </td>
							</tr>
							

							<tr>
								<th> {{json_data($sc,'orContentitem8')}}</th>
								<td><strong>{{($price + $set->additional_value) - ($price * $row->percent_now/100)}} @lang('front.namePrice') </strong></td>
							</tr>
							@else  
							<tr>
								<th> {{json_data($sc,'orContentitem8')}}</th>
								<td><strong>{{$price + $set->additional_value}} @lang('front.namePrice') </strong></td>
							</tr>
							@endif

						</table>
						<br>
						<!--<a href="#" class="calculate-shipping"><i class="fa fa-arrow-circle-down"></i> أنهاء الطلب </a>-->
					</div>

					<!-- Apply Coupon Code / Buttons -->
					<table class="cart-table copon_table bottom">
						<tr>
							<th>
								
								<div class="cart-btns">
									<a href="{{route('front.get.client.page',['myorders'])}}" class="button  cart-btns">  {{json_data($sc,'orContentitem10')}} </a>
								</div>

								@if($row->status == "accepted")
									@if(!$row->reverce($row->id,client()->id))
									<div class="cart-btns">
										<a href="{{route('front.get.order.reverce',$row->id)}}" class="btn btn-danger ">   {{json_data($sc,'orContentitem9')}}   </a>
									</div>
									@endif
								@endif
							</th>
						</tr>

					</table>

				</div>
			</div>
		</div>
	</div>


@endsection




@section('script')
<!-- Start rating -->
@include('front.inc.ajaxRating')
<!-- End rating -->

@if(session('status') !== null)
	<script>
	
		Swal.fire(
		{
			position: 'top-end',
			type: 'success',
			title: "{{trans('front.msg.reverceOrderSuccess')}}",
			showConfirmButton: false,
			timer: 2000
		})
	
	</script>
@endif

@endsection