@if($orders->count())

<div class="det_profile_page">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>  {{json_data($sc,'refOrderitem1')}}</th>
                <th> {{json_data($sc,'refOrderitem2')}}</th>
                <th> {{json_data($sc,'refOrderitem3')}}</th>
                <th> {{json_data($sc,'refOrderitem4')}}</th>
                <th>  {{json_data($sc,'refOrderitem5')}} </th>
                <th>    {{json_data($sc,'refOrderitem6')}} </th>
                <th>{{json_data($sc,'refOrderitem7')}}</th>
                <th> {{json_data($sc,'refOrderitem8')}}</th>
            </tr>
        </thead>
        <tbody>


            <tr class="alert alert-info">
                <td colspan="8" class="text-center"> @lang('front.msg.orderRefused') </td>
            </tr>

        @foreach($orders as $ord)
            <tr>
                <th>{{$ord->id}}</th>
                <th><a href="{{route('front.get.order.showOrder',[$ord->order->id])}}">{{$ord->order->id}}</a></th>
                <td>{{$ord->order->content->count()}}</td>
                <td> <strong> {{$ord->order->content->sum('price')}} </strong> @lang('front.namePrice')</td>
                <td>{{ date('Y-m-d',strtotime($ord->order->created_at)) }}</td>
                <td>{{ date('Y-m-d',strtotime($ord->created_at)) }}</td>
                <td> {{trans('front.'.$ord->order->status)}} </td>
                <td class="text-center">  
                    <a href="{{route('front.get.order.showOrder',[$ord->order->id])}}"> <i class="fa fa-eye fa-2x"></i> </a> 
                </td>
            </tr>
        @endforeach

            <tr class="alert alert-info">
                <td colspan="8" class="text-center"> @lang('front.msg.orderReverced') </td>
            </tr>
        @foreach($cancelled as $ord)
            <tr>
                <th>{{$ord->id}}</th>
                <th><a href="{{route('front.get.order.showOrder',[$ord->id])}}">{{$ord->id}}</a></th>
                <td>{{$ord->content->count()}}</td>
                <td> <strong> {{$ord->content->sum('price')}} </strong> @lang('front.namePrice')</td>
                <td>{{ date('Y-m-d',strtotime($ord->created_at)) }}</td>
                <td>{{ date('Y-m-d',strtotime($ord->created_at)) }}</td>
                <td> {{trans('front.'.$ord->status)}} </td>
                <td class="text-center">  
                    <a href="{{route('front.get.order.showOrder',[$ord->id])}}"> <i class="fa fa-eye fa-2x"></i> </a> 
                </td>
            </tr>
        @endforeach



        
        </tbody>
    </table>
</div>


@else  


<div class="alert alert-info text-center">@lang('front.msg.notFoundData')</div>


@endif