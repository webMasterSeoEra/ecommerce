<div class="col-md-3 col-xs-12">
    <div class="col-right_profile">
        <a href="{{route('front.get.client.account')}}" class="@if(Request::segment('2') == '') active @endif">     {{json_data($sc,'profileitem1')}}  <i class="fa fa-user" aria-hidden="true"></i> </a>
        <a href="{{route('front.get.client.page',['edit-information'])}}" class="@if(Request::segment('2') == 'edit-information') active @endif">  {{json_data($sc,'profileitem2')}}<i class="fa fa-pencil-square-o" aria-hidden="true"></i>  </a>
        <a href="{{route('front.get.client.page',['change-password'])}}" class="@if(Request::segment('2') == 'change-password') active @endif"> {{json_data($sc,'profileitem3')}}<i class="fa fa-unlock-alt" aria-hidden="true"></i> </a>
        <a href="{{route('front.get.client.page',['edit-address'])}}" class="@if(Request::segment('2') == 'edit-address') active @endif">   {{json_data($sc,'profileitem4')}} <i class="fa fa-map-marker" aria-hidden="true"></i></a>
        <a href="{{route('front.get.client.page',['myorders'])}}" class="@if(Request::segment('2') == 'myorders') active @endif">  {{json_data($sc,'profileitem5')}} <i class="fa fa-bookmark" aria-hidden="true"></i> </a>
        <a href="{{route('front.get.client.page',['myfavorits'])}}" class="@if(Request::segment('2') == 'myfavorits') active @endif">  {{json_data($sc,'profileitem6')}} <i class="fa fa-heart" aria-hidden="true"></i> </a>
        <a href="{{route('front.get.client.page',['reverced'])}}" class="@if(Request::segment('2') == 'reverced') active @endif">   {{json_data($sc,'profileitem7')}} <i class="fa fa-times-circle" aria-hidden="true"></i> </a>
    </div>
</div>