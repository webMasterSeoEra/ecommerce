@if($favorits->count())
		
<div class="col-md-12 col-xs-12">
    <div class="cart">
        <!-- Cart -->
        <table class="cart-table responsive-table">
            <thead class="head_table">
            <tr>
                <th>صورة</th>
                <th class="text-center" style="text-align:center !important">أسم المنتج</th>

            </tr>
            </thead>

            @foreach($favorits as $prod)
                @if($prod->product) 
                <tr>
                    <td>
                        <a href="{{route('front.get.product.showProd',[$prod->product->slug,$prod->product->cat->slug])}}">
                            <img src="{{getImg(PRODUCT_PATH.$prod->product->img)}}" style="height:70px;" alt=""/>
                        </a>
                    </td>
                    <td class="cart-title ">
                    
                        {{$prod->product->name}}
                    
                    </td>
                </tr>
                
                @endif
            @endforeach
            


        </table>




    </div>
</div>
@else  


<div class="alert alert-info text-center">@lang('front.msg.notFoundData')</div>


@endif





