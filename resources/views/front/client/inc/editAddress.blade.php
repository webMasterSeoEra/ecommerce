<form  id="editAddress">
    <ul id="errorsAddress"></ul>
    @csrf
  
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <label> {{json_data($sc,'addressitem1')}} </label>
            <textarea class="form-control" required name="address"  rows="3" >{{client()->address}}</textarea>
            <br>
    
        </div>
    </div>

    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <label> {{json_data($sc,'addressitem2')}} </label>
            <textarea class="form-control" name="address2" rows="3" >{{client()->address2}}</textarea>
            <br>
            
        </div>
    </div>

    <div class="col-md-12 col-xs-12">
        <button type="submit" class="btn btn-danger"> {{json_data($sc,'addressitem3')}}</button>
    </div>
</form>