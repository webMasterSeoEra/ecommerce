<form  id="editInfo">
    <ul id="errorsInfo"></ul>
    @csrf
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <label> {{json_data($sc,'eProfileitem1')}}</label>
            <input type="text" name="name" value="{{client()->name}}" class="form-control edit_pro" id="exampleInputPassword1" >
            <br>
        </div>
    </div>


    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <label> {{json_data($sc,'eProfileitem2')}} </label>
            <input type="text" name="phone" value="{{client()->phone}}" class="form-control edit_pro" id="exampleInputPassword">
            <br>
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <label> {{json_data($sc,'eProfileitem3')}} </label>
            <input type="text" name="mobile" value="{{client()->mobile}}" class="form-control edit_pro" id="exampleInputPassword1">
            <br>
        </div>
    </div>


    <div class="col-md-12 col-xs-12">
        <button type="submit" class="btn btn-danger"> {{json_data($sc,'eProfileitem4')}} </button>
    </div>
</form>