@if($orders->count())

<div class="det_profile_page">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th> {{json_data($sc,'ordersitem1')}} </th>
                <th> {{json_data($sc,'ordersitem2')}}</th>
                <th> {{json_data($sc,'ordersitem3')}}</th>
                <th>{{json_data($sc,'ordersitem4')}}</th>
                <th>{{json_data($sc,'ordersitem5')}}</th>
                <th> {{json_data($sc,'ordersitem6')}}</th>
                <th> {{json_data($sc,'ordersitem7')}}</th>
                <th> الغاء الطلب </th>
  
            </tr>
        </thead>
        <tbody>

        @foreach($orders as $order)
            <tr>
                <th><a href="{{route('front.get.order.showOrder',[$order->id])}}">{{$order->id}}  </a></th>
                <td>{{$order->content->count()}}</td>
                <td> <strong> {{$order->content->sum('price')}} + {{ $set->additional_value}}  </strong> @lang('front.namePrice')</td>
                <td>{{ date('Y-m-d',strtotime($order->created_at)) }}</td>
                <td> {{trans('front.'.$order->status)}} </td>
                <td class="text-center">  
                    <a href="{{route('front.get.order.showOrder',[$order->id])}}"> <i class="fa fa-eye fa-2x"></i> </a> 
                </td>
                <td>
                    @if($order->cupon)
                    <strong>{{ $order->cupon->cupon_number }}  ( {{ $order->percent_now }}  %  ) </strong>
                    @endif
                </td>
                <td class="text-center">
                    @if($order->status == "pending")
                        <a href="{{route('front.get.order.delete',[$order->id])}}" class="btn btn-danger delete-item" > 
                            <i class="fa fa-times "></i> 
                        </a> 
                    @endif
                </td>
            </tr>
        @endforeach
        
        </tbody>
    </table>
</div>


@else  


<div class="alert alert-info text-center">@lang('front.msg.notFoundData')</div>


@endif