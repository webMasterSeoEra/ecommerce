<form  id="editPass">
    <ul id="errorsPass"></ul>
    @csrf
    <div class="form-group">
        <label>  {{json_data($sc,'cPassworditem1')}} </label>
        <input type="password" required name="old_password"  class="form-control chan_pass"  >
        <br>
        <br>
    </div>

    <div class="form-group">
        <label>  {{json_data($sc,'cPassworditem2')}}</label>
        <input type="password" required  name="password" class="form-control chan_pass" >
    </div>

    <div class="form-group">
        <label>  {{json_data($sc,'cPassworditem3')}} </label>
        <input type="password" required name="confirm_password" class="form-control chan_pass" >
    </div>


    <div class="col-md-12 col-xs-12">
        <button type="submit" class="btn btn-danger"> {{json_data($sc,'cPassworditem4')}}</button>
    </div>
</form>