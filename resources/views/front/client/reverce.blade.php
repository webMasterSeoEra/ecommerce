@extends('front.main')

@section('content')

<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

    <img src="{{furl()}}/images/parallax-2.jpg" alt="" />
    <div class="parallax-overlay"></div>

    <div class="container">
        <div class="parallax-content">

            <nav id="breadcrumbs">
                <ul>
                    <li><a href="#">الرئيسية</a></li>
                    <li>رقم الطلب : {{$row->id * 250}}</li>
                </ul>
            </nav>


        </div>
    </div>
</section>




<div class="container">
		<div class="row">
		
			<div class="col-md-12 col-xs-12">
				<div class="cart">
				
                    @include('front.msg._errors')
                    <form method="post" class="login" action="{{route('front.post.order.doReverce')}}">
                        @csrf

                        <div class="form-group form-row-wide">
                            <label class="">{{json_data($sc,'refuseitem1')}}</label>

                                <select class="form-control" name="type" style="height:40px;">
                                    
                                    <option value=""> @lang('front.prductStatusError') </option>
                                    <option value="productErrorRespond"> @lang('front.productErrorRespond') </option>
                                    <option value="productExpired"> @lang('front.productExpired') </option>
                                    <option value="ProductError"> @lang('front.ProductError') </option>
                                    <option value="productErrorExist"> @lang('front.productErrorExist') </option>
                                    <option value="productErrorOther"> @lang('front.productErrorOther') </option>
                                 
                                </select>
                        </div>


                        <p class="form-row form-row-wide">
                            <label for="username">  {{json_data($sc,'refuseitem2')}} <span class="required">*</span></label>
                            <textarea class="form-control" rows="7" name="message"></textarea>
                            <input type="hidden" class="input-text" name="order"  value="{{$row->id}}">
                        </p>


                        <p class="form-row">
                            <input type="submit" class="button" name="login" value="{{json_data($sc,'refuseitem3')}}">
                        </p>
                    </form>

				</div>
			</div>
		</div>
	</div>
</div>


@endsection


