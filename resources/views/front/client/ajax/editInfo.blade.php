<script type="text/javascript">

  
$("#editInfo").submit(function(e) 
  {
    var el = $(this); 

      e.preventDefault();
      var formData  = new FormData(jQuery('#editInfo')[0]);


    $.ajax({
        type: "POST",
        url: "{{ route('front.post.client.editInfo')}}",
        data:formData,
        contentType: false,
        processData: false,
        beforeSend:function()
        {
            el.find("button").prop( "disabled", true );
        },
        success: function (data) 
        {


            el.find("button").prop( "disabled", false );
            $("#errorsInfo").html('');
            $("#errorsInfo").append("<li class='alert alert-success text-center'>{{trans('admin.msg.editedSuccess')}}</li>")

            Swal.fire(
            {
                position: 'top-end',
                type: 'success',
                title: "{{trans('admin.msg.editedSuccess')}}",
                showConfirmButton: false,   
                timer: 2000
            })
            $('.edit-modal').modal('toggle');
            window.location.reload()
            //  

        }, error: function(xhr, status, error) 
        {
            el.find("button").prop( "disabled", false );
            $("#errorsInfo").html('');
            $.each(xhr.responseJSON.errors, function (key, item) 
            {
              $("#errorsInfo").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
            });
          
        }
    });

});

</script>