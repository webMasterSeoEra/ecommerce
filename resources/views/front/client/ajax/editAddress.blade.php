<script type="text/javascript">

  
$("#editAddress").submit(function(e) 
  {
    var el = $(this); 

      e.preventDefault();
      var formData  = new FormData(jQuery('#editAddress')[0]);


    $.ajax({
        type: "POST",
        url: "{{ route('front.post.client.editAddress')}}",
        data:formData,
        contentType: false,
        processData: false,
        beforeSend:function()
        {
            el.find("button").prop( "disabled", true );
        },
        success: function (data) 
        {


            el.find("button").prop( "disabled", false );
            $("#errorsAddress").html('');
            $("#errorsAddress").append("<li class='alert alert-success text-center'>{{trans('admin.msg.editedSuccess')}}</li>")

            Swal.fire(
            {
                position: 'top-end',
                type: 'success',
                title: "{{trans('admin.msg.editedSuccess')}}",
                showConfirmButton: false,   
                timer: 2000
            })
            $('.edit-modal').modal('toggle');
            window.location.reload()
            //  

        }, error: function(xhr, status, error) 
        {
            el.find("button").prop( "disabled", false );
            $("#errorsAddress").html('');
            
            if(xhr.responseJSON.errors)
            {
                $.each(xhr.responseJSON.errors, function (key, item) 
                {
                $("#errorsAddress").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
                });
            }
            else  
            {
                if(xhr.responseJSON.message)
                {
                    $("#errorsAddress").append("<li class='alert alert-danger show-errors'>"+xhr.responseJSON.message+"</li>")
                }
            }
          
        }
    });

});

</script>