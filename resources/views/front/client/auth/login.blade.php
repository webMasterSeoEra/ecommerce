@extends('front.main')
@section('content')



<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
<div class="parallax-overlay"></div>

<div class="container">
    <div class="parallax-content">

        <nav id="breadcrumbs">
            <ul>
                <li><a href="{{route('front.get.home.index')}}">الرئيسية</a></li>
                <li>تسجيل الدخول / أنشاء حساب جديد</li>
            </ul>
        </nav>


    </div>
</div>
</section>


<div class="container">

<div class="col-md-6 centered">

    <div class="page_login">
        <ul class="tabs-nav my-account">
            <li class="@if($page == 'login') active @endif    "><a href="#tab1"> {{json_data($sc,'loginitem1')}}  </a></li>
            <li class="@if($page == 'reg') active @endif"><a href="#tab2">  {{json_data($sc,'loginitem7')}} </a></li>
        </ul>
        <div class="tabs-container">

            @include('front.msg._errors')
            <!-- Login -->
            <div class="tab-content" id="tab1" style="@if($page=='login') display: block; @else display: none; @endif">

                <form method="post" class="login" action="{{route('front.get.authClient.do_login')}}">
                    @csrf

                    <p class="form-row form-row-wide">
                        <label for="username">  {{json_data($sc,'loginitem2')}}  <span class="required">*</span></label>
                        <input type="email" required class="input-text" name="email" id="username" value="">
                    </p>

                    <p class="form-row form-row-wide">
                        <label for="password"> {{json_data($sc,'loginitem3')}} <span class="required">*</span></label>
                        <input class="input-text" required type="password" name="password" id="password">
                    </p>

                    <p class="form-row">
                        <input type="submit" class="button" name="login" value=" {{json_data($sc,'loginitem4')}} ">
                        <label for="rememberme" class="rememberme">
                        <input name="rememberme" type="checkbox" id="rememberme" value="forever">   {{json_data($sc,'loginitem5')}} </label>
                    </p>

                    <p class="lost_password">
                        <a href="{{route('front.get.authClient.forgot')}}">  {{json_data($sc,'loginitem6')}}  </a>
                    </p>


                </form>
            </div>

            <!-- Register -->
            <div class="tab-content" id="tab2" style="@if($page=='reg') display: block; @else display: none; @endif">



                <form method="POST" class="register" action="{{route('front.get.authClient.do_register')}}" >
                    @csrf
                    <p class="form-row form-row-wide">
                        <label for="reg_name">   {{json_data($sc,'loginitem8')}}  <span class="required">*</span></label>
                        <input type="text" required class="input-text" name="name" id="reg_name" value="">
                    </p>


                    <p class="form-row form-row-wide">
                        <label for="reg_email"> {{json_data($sc,'loginitem9')}} <span class="required">*</span></label>
                        <input type="email" required class="input-text" name="email" id="reg_email" value="">
                    </p>


                    <p class="form-row form-row-wide">
                        <label for="reg_password"> {{json_data($sc,'loginitem10')}} <span class="required">*</span></label>
                        <input type="password" required class="input-text" name="password" id="reg_password">
                    </p>

                    <p class="form-row form-row-wide">
                        <label for="reg_password2">  {{json_data($sc,'loginitem11')}} <span class="required">*</span></label>
                        <input type="password" required class="input-text" name="confirm_password" id="reg_password2">
                    </p>


                    <p class="form-row">
                        <input type="submit" class="button" name="register" value=" {{json_data($sc,'loginitem12')}}">
                    </p>

                </form>
            </div>
        </div>
    </div>
</div>
</div>








@endsection