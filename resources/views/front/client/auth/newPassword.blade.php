@extends('front.main')
@section('content')



<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
<div class="parallax-overlay"></div>

<div class="container">
    <div class="parallax-content">

        <nav id="breadcrumbs">
            <ul>
                <li><a href="{{route('front.get.home.index')}}">الرئيسية</a></li>
                <li>  تغيير كلمة المرور  </li>
            </ul>
        </nav>


    </div>
</div>
</section>


<div class="container">

<div class="col-md-6 centered">

    <div class="page_login">
    
        <div class="tabs-container">

            @include('front.msg._errors')
            <form method="post" class="login" action="{{route('front.post.authClient.newPassword')}}">
                    @csrf

                    <p class="form-row form-row-wide">
                        <label for="username">   كلمة المرور الجديدة <span class="required">*</span></label>
                        <input type="password" class="input-text" name="password"  required>
                        <input type="hidden" name="email" value="{{$email}}" >
                        <input type="hidden" name="token" value="{{$token}}" >
                    </p>

                    <p class="form-row form-row-wide">
                        <label for="username">   تأكيد كلمة المرور  <span class="required">*</span></label>
                        <input type="password" class="input-text" name="confirm_password"  required>
                    </p>


                    <p class="form-row">
                        <input type="submit" class="button" name="login" value="أرســـــــــال">
                    </p>
                </form>
            
        </div>
    </div>
</div>
</div>








@endsection