@extends('front.main')

@section('seo')

	<?php $seo = json_decode($set->seo); ?>
	{!! str_replace("<br />", "", json_data($seo,'seoHeader')) !!}

@endsection


@section('content')

	<!-- Slider
    ================================================== -->
	<div class="home-slider">

		<div class="tp-banner-container">
			<div class="tp-banner">
				<ul>

					<!-- Slide 1  -->

					@foreach($sliders as $sl)
					<li data-transition="fade" data-slotamount="7" data-masterspeed="1000">
						
						<img src="{{getImg(SLIDER_PATH.$sl->img)}}"  alt=""  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
						<div class="caption sfb fadeout" data-x="145" data-y="170" data-speed="400" data-start="800"  data-easing="Power4.easeOut">
							@if($sl->link)
							<h2>   {{$sl->name}} </h2>
							@endif 
							@if($sl->small_desc)
							<h3> {{$sl->small_desc}}  </h3>
							@endif
							<!-- <a href="#" class="caption-btn"> المزيد من قسم النكهات </a> -->
						</div>
						
					</li>
					@endforeach



				</ul>
			</div>
		</div>
	</div>


	<div class="clearfix"></div>


	<!-- New Arrivals
    ================================================== -->
	<div class="container">

		<div class="row">
		<!-- Headline -->
		<div class="col-md-12 col-xs-12">
			<h3 class="headline"> {{json_data($sc,'homeitem1')}}</h3>
			<span class="line margin-bottom-0"></span>
		</div>

		<!-- Carousel -->
		<div id="new-arrivals" class="showbiz-container col-md-12 col-xs-12 p-0">

			<!-- Navigation -->
			<div class="showbiz-navigation">
				<div id="showbiz_left_1" class="sb-navigation-left"><i class="fa fa-angle-left"></i></div>
				<div id="showbiz_right_1" class="sb-navigation-right"><i class="fa fa-angle-right"></i></div>
			</div>
			<div class="clearfix"></div>

			<!-- Products -->
			<div class="showbiz" data-left="#showbiz_left_1" data-right="#showbiz_right_1" data-play="#showbiz_play_1" >
				<div class="overflowholder">

					<ul>

						<!-- Product #1 -->

						@foreach($specials as $prs)
						<li>
							<figure class="product">
                                <div class="mediaholder">
                                    <a href="{{route('front.get.product.showProd',[$prs->slug,$prs->cat->slug])}}">
                                        <img alt="{{$prs->name}}" src="{{getImg(PRODUCT_PATH.$prs->img)}}" class="img-show" />
                                        <div class="cover">
											<img alt="{{$prs->name}}" src="{{getImg(PRODUCT_PATH.$prs->img)}}" class="img-show" />
										</div>
                                    </a>
                                    <a class="product-button pointer addToCart" data-prod-id="{{$prs->id}}">
                                        <i class="fa fa-shopping-cart"></i> أضف الى السلة 
                                    </a>
                                </div>

                                <a href="{{route('front.get.product.showProd',[$prs->slug,$prs->cat->slug])}}">
                                    <section>
                                        <span class="product-category"> @lang('front.productsSpecial') </span>
                                        <h5>  {{\Str::Words($prs->name,'4',' ')}} </h5>
                                        <span class="product-price">@lang('front.namePrice')  {{$prs->price}} </span>
                                    </section>
                                </a>
                                @if(client())
									@if(!$prs->checkWishlist($prs->id,client()->id))
									<button class="btn  addToWishlist wishlist" data-route="{{route('front.get.client.wishlist',[$prs->id])}}"> <i class="fa fa-heart"></i>  </button>
									@else  
									<button class="btn   wishlist" > <i class="fa fa-heart active"></i>  </button>
									@endif
								@endif
                            </figure>
						</li>
						@endforeach



					</ul>
					<div class="clearfix"></div>

				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		</div>
	</div>



	<!-- Parallax Banner
    ================================================== -->
	<div class="parallax-banner fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="200">
		<img src="{{furl()}}/images/parallax.jpg" alt="" />
		<div class="parallax-overlay"></div>
		<div class="parallax-title"> بيت الحكمة للعطارة <span> عروض وتخفيضات على كل منتجاتنا عن الطلب اونلاين </span></div>
	</div>



	<!-- Container -->
	<div class="container">

		<div class="row">

			<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 padd-right">
				<!-- Filters -->
				<div class="col-md-12 col-xs-12 p-0">
					<div id="filters" class="filters-dropdown headline"><span></span>
						<ul class="option-set" data-option-key="filter">
							<li><a href="#filter" class="selected" data-filter="*">  {{json_data($sc,'homeitem2')}} </a></li>
							<li><a href="#filter" data-filter=".products_1">  {{json_data($sc,'homeitem3')}} </a></li>
							<li><a href="#filter" data-filter=".products_2">   {{json_data($sc,'homeitem4')}} </a></li>
							<li><a href="#filter" data-filter=".products_3">  {{json_data($sc,'homeitem5')}} </a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12 col-xs-12 p-0">

				<!-- Portfolio Wrapper-->
				<div id="portfolio-wrapper">

					@foreach($viewProducts as $prv)
					<!-- Portfolio Item -->
					<div class="one-third col-md-3 p-0 col-xs-12 portfolio-item products_2 products_2">
						<figure>
							<div class="portfolio-holder">
								<a href="{{route('front.get.product.showProd',[$prv->slug,$prv->cat->slug])}}">
									<img alt="{{$prv->name}}" title="{{$prv->name}}" 
									src="{{getImg(PRODUCT_PATH.$prv->img)}}"/>
									<div class="hover-cover"></div>
									<a href="{{route('front.get.product.showProd',[$prv->slug,$prv->cat->slug])}}"><div class="hover-icon"></div></a>
								</a>
							</div>

							<a href="{{route('front.get.product.showProd',[$prv->slug,$prv->cat->slug])}}">
								<section class="item-description">
									<span>  {{\Str::Words($prv->name,'4',' ')}} </span>
									<h5>{{$prv->price}} @lang('front.namePrice') </h5>
								</section>
							</a>

						</figure>
					</div>
					@endforeach



					@foreach($sellNum as $prsell)
					<!-- Portfolio Item -->
					<div class="one-third col-md-3 p-0 col-xs-12 portfolio-item products_1 products_1">
						<figure>
							<div class="portfolio-holder">
								<a href="{{route('front.get.product.showProd',[$prsell->slug,$prsell->cat->slug])}}">
									<img alt="{{$prsell->name}}" title="{{$prsell->name}}" 
									src="{{getImg(PRODUCT_PATH.$prsell->img)}}"/>
									<div class="hover-cover"></div>
									<a href="{{route('front.get.product.showProd',[$prsell->slug,$prsell->cat->slug])}}"><div class="hover-icon"></div></a>
								</a>
							</div>

							<a href="{{route('front.get.product.showProd',[$prsell->slug,$prsell->cat->slug])}}">
								<section class="item-description">
									<span>  {{\Str::Words($prsell->name,'4',' ')}} </span>
									<h5>{{$prsell->price}} @lang('front.namePrice') </h5>
								</section>
							</a>

						</figure>
					</div>
					@endforeach



					@foreach($latest as $prl)
					<!-- Portfolio Item -->
					<div class="one-third col-md-3 p-0 col-xs-12 portfolio-item products_3 products_3">
						<figure>
							<div class="portfolio-holder">
								<a href="{{route('front.get.product.showProd',[$prl->slug,$prl->cat->slug])}}">
									<img alt="{{$prl->name}}" title="{{$prl->name}}" 
									src="{{getImg(PRODUCT_PATH.$prl->img)}}"/>
									<div class="hover-cover"></div>
									<a href="{{route('front.get.product.showProd',[$prl->slug,$prl->cat->slug])}}"><div class="hover-icon"></div></a>
								</a>
							</div>

							<a href="{{route('front.get.product.showProd',[$prl->slug,$prl->cat->slug])}}">
								<section class="item-description">
									<span>  {{\Str::Words($prl->name,'4',' ')}} </span>
									<h5>{{$prl->price}} @lang('front.namePrice') </h5>
								</section>
							</a>

						</figure>
					</div>
					@endforeach

				</div>
				<!-- Portfolio Wrapper / End -->
				</div>
			</div>


			<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 padd-left">

				<div class="block_left">

					<h3 class="tit_new_block">   {{json_data($sc,'homeitem6')}} </h3>

					<ul class="widget-tabs">


						@foreach($sellNum as $prsell)
						<!-- Post #1 -->
						<li>
							<div class="widget-thumb">
								<a href="{{route('front.get.product.showProd',[$prsell->slug,$prsell->cat->slug])}}">
									<img src="{{getImg(PRODUCT_PATH.$prl->img)}}" alt="{{$prsell->name}}">
								</a>
							</div>

							<div class="widget-text">
								<h4>
									<a href="{{route('front.get.product.showProd',[$prsell->slug,$prsell->cat->slug])}}"> 
									{{\Str::Words($prsell->name,'3',' ')}}
									</a>
								</h4>
								<span>{{$prsell->price}} @lang('front.namePrice')</span>
							</div>
							<div class="clearfix"></div>
						</li>
						<!-- Post #2 -->
						@endforeach
		

					</ul>
				</div>


			</div>


		</div>
	</div>
	<!-- Container / End -->




	<!-- Container -->
	<div class="block_cleints">
	<div class="container margin-top-40">

		<!-- Navigation / Left -->
		<div class="one carousel column">
			<div id="showbiz_left_2" class="sb-navigation-left-2">
				<i class="fa fa-angle-left"></i>
			</div>
		</div>

		<!-- ShowBiz Carousel -->
		<div id="our-clients" class="showbiz-container fourteen carousel columns" >

			<!-- Portfolio Entries -->
			<div class="showbiz our-clients" data-left="#showbiz_left_2" data-right="#showbiz_right_2">
				<div class="overflowholder">

					<ul>
						<!-- Item -->
						@foreach($companies as $comp)
						<li><img src="{{getImg(COMPANY_PATH.$comp->img)}}" alt="" /></li>
						@endforeach
					
					</ul>
					<div class="clearfix"></div>

				</div>
				<div class="clearfix"></div>

			</div>
		</div>

		<!-- Navigation / Right -->
		<div class="one carousel column">
			<div id="showbiz_right_2" class="sb-navigation-right-2">
				<i class="fa fa-angle-right"></i>
			</div>
		</div>

	</div>
	</div>
	<!-- Container / End -->

@endsection


@section('script')
	@include('front.inc.ajaxCart')

	@if(client())
		@include('front.inc.ajaxWishlist')
	@endif

@endsection