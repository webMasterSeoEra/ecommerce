@extends('front.main')
@section('content')


<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
<div class="parallax-overlay"></div>

<div class="container">
    <div class="parallax-content">

        <nav id="breadcrumbs">
            <ul>
                <li><a href="{{route('front.get.home.index')}}">الرئيسية</a></li>
                <li> شروط الاستخدام </li>
            </ul>
        </nav>


    </div>
</div>
</section>


<div class="container">

<div class="row">


    <div class="col-xs-12">
        <div class=""> 
            <p>
                {!!$set->cond_used!!}
            </p>
        </div>
    </div>


</div>
</div>


@endsection