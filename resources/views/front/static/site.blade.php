@extends('front.main')
@section('content')


<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
<div class="parallax-overlay"></div>

<div class="container">
    <div class="parallax-content">

        <nav id="breadcrumbs">
            <ul>
                <li><a href="{{route('front.get.home.index')}}">الرئيسية</a></li>
                <li> {{$row->name}}</li>
            </ul>
        </nav>


    </div>
</div>
</section>


<div class="container">

<div class="row">
    <div class="col-md-12 col-xs-12">
        <h3 class="headline"> {{$row->name}} </h3><span class="line margin-bottom-30"></span><div class="clearfix"></div>
    </div>

    <div class="col-md-8 col-xs-12">
        <div class="des_about_P"> 
            <p>
                {!!$row->desc!!}
            </p>
        </div>
    </div>

    <div class="col-md-4 col-xs-12">
        <div class="img_p_about">
            <img src="{{getImg(STATIC_PATH.$row->img)}}">
        </div>
    </div>

</div>
</div>


@endsection