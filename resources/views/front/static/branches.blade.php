@extends('front.main')
@section('content')




<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

		<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
		<div class="parallax-overlay"></div>

		<div class="container">
			<div class="parallax-content">

				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{route('front.get.home.index')}}">@lang('front.home')</a></li>
						<li> الفروع </li>
					</ul>
				</nav>


			</div>
		</div>
	</section>
    



<div class="container">

        @foreach($branches as $branch)
		<div class="row">
			<div class="bran_page">
				<div class="col-md-8 col-xs-12">
					<div class="des_about_P">
						<h3>  {{$branch->name}}  </h3>
						<label> {{$branch->address}}    </label>
						<p>رقم الهاتف:</p>
						<p>{{$branch->mobile1}}  </p>
					</div>
				</div>
				<div class="col-md-4 col-xs-12">
					@if($branch->map)
						{!! $branch->map !!} 
					@endif
				</div>
			</div>
        </div>
        @endforeach


	</div>

@endsection