@extends('front.main')
@section('content')


<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

		<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
		<div class="parallax-overlay"></div>

		<div class="container">
			<div class="parallax-content">

				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{route('front.get.home.index')}}">@lang('front.home')</a></li>
						<li> اتصل بنا </li>
					</ul>
				</nav>


			</div>
		</div>
	</section>
    


<div class="container">

<div class="row">
    <div class="col-md-12 col-xs-12">
        <h3 class="headline">أتصل بنا</h3><span class="line margin-bottom-30"></span><div class="clearfix"></div>
    </div>

    <div class="col-md-3 col-xs-12">
        <!-- Information -->
        <div class="widget margin-top-10">
            <div class="accordion ui-accordion ui-widget ui-helper-reset">
                <!-- Section 1 -->
                <h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all"><span class="ui-accordion-header-icon ui-icon ui-accordion-icon"></span><i class="fa fa-user"></i> البيانات الشخصية </h3>
                <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" style="display: block;">
                    <ul class="contact-informations second">
                        <li><i class="fa fa-phone"></i> <p> {{$set->mobile1}} </p></li>
                        <li><i class="fa fa-envelope"></i> <p>{{$set->email}} </p></li>
                        <li><i class="fa fa-fax"></i> <p>{{$set->fax}}</p></li>
                    

                    </ul>
                </div>

                <!-- Section 2 -->
                <h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all"><span class="ui-accordion-header-icon ui-icon ui-accordion-icon"></span><i class="fa fa-thumb-tack"></i> التواصل الاجتماعي</h3>
                <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" style="display: none;">
                    <ul class="contact-informations second social-icons">
                            <li><a class="twitter" href="{{$set->twitter}}"><i class="icon-twitter"></i></a></li>
                            <li><a class="facebook" href="{{$set->facebook}}"><i class="icon-facebook"></i></a></li>
                            <li><a class="pinterest" href="{{$set->pinterest}}"><i class="icon-pinterest"></i></a></li>
                            <li><a class="instagram" href="{{$set->instagram}}"><i class="icon-instagram"></i></a></li>
                         
                    </ul>
                </div>
            </div>
        </div>
    </div>



    <!-- Contact Form -->
    <div class="col-md-9 col-xs-12">
        <div class="extra-padding left">
            <!-- Contact Form -->
            <section id="contact">

                <!-- Success Message -->
                <mark id="message"></mark>

                <!-- Form -->
                <form id="sendMessage" novalidate>
                    <ul id="errorsContact"></ul>
                    <fieldset>
                        @csrf
                        <div>
                            <label>الاسم :</label>
                            <input name="name" type="text" id="name"  required  />
                        </div>

                        <div>
                            <label >البريد الالكتروني : <span>*</span></label>
                            <input name="email" type="email" id="email"   required pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" />
                        </div>

                        <div>
                            <label>أكتب تعليقك : <span>*</span></label>
                            <textarea name="message" cols="40" rows="3"  required id="comment" spellcheck="true"></textarea>
                        </div>

                    </fieldset>
                    <div id="result"></div>
                    <input type="submit" class="submit pull-right button" id="submit" value=" ارسال الرسالة " />
                    <div class="clearfix"></div>
                </form>

            </section>
            <!-- Contact Form / End -->
        </div>
    </div>



</div>
</div>


@endsection


@section('script')

    
<script type="text/javascript">

  
$("#sendMessage").submit(function(e) 
  {
    var el = $(this); 

      e.preventDefault();
      var formData  = new FormData(jQuery('#sendMessage')[0]);


    $.ajax({
        type: "POST",
        url: "{{route('front.post.message.sendMessage')}}",
        data:formData,
        contentType: false,
        processData: false,
        beforeSend:function()
        {
            el.find(".button").prop( "disabled", true );
        },
        success: function (data) 
        {
            el.find("button").prop( "disabled", false );
            Swal.fire(
            {
              position: 'top-end',
              type: 'success',
              title: "{{trans('front.msg.messageSuccess')}}",
              showConfirmButton: false,
              timer: 5000
            })

            setTimeout(function(){ window.location.reload(); }, 3000);
            

        }, error: function (xhr, status, error) 
        {
            
            $("#errorsContact").html('');
            $.each(xhr.responseJSON.errors, function (key, item) 
            {
              $("#errorsContact").append("<li class='alert alert-danger text-center show-errors'>"+item+"</li>")
            });
            el.find(".button").prop( "disabled",false);
          
        }
    });

});

</script>

@endsection