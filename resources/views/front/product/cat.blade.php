@extends('front.main')

@section('seo')

	<?php $seo = json_decode($row->seo); ?>
	{!! str_replace("<br />", "", json_data($seo,'seoHeader')) !!}

@endsection
@section('content')

<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">
@if($row->img)
    <img src="{{getImg(CATEGORY_PATH.$row->img)}}" alt="{{$row->name}}" /> 
@else
    <img src="{{furl()}}/images/parallax-2.jpg" alt="{{$row->name}}" /> 
@endif
<div class="parallax-overlay"></div>

<div class="container">
    <div class="parallax-content">

        <nav id="breadcrumbs">
            <ul>
                <li><a href="{{route('front.get.home.index')}}">الرئيسية</a></li>
                <li>  {{$row->name}} </li>
                <!-- <li> بهارات مشكلة فاخرة </li> -->
            </ul>
        </nav>


    </div>
</div>
</section>


<div class="container">
<div class="col-md-3 col-xs-12">

    <div class="sidebar_page">
        <!-- Categories -->
        <div class="widget margin-top-0">
            <h3 class="headline"> الاقسام الرئيسية </h3>
            <ul id="categories">

                @foreach($cats as $cat)
                <li><a href="#" class="@if($row->id == $cat->id) active @endif"> {{$cat->name}} <span>{{$cat->product->count()}}</span></a>
                    <ul>
                        @foreach($cat->product as $prod)
                        <li>
                            <a href="{{route('front.get.product.showProd',[$prod->slug,$cat->slug])}}"> 
                            {{ \Str::Words($prod->name,3,' ')}} 
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @endforeach
     

            </ul>
            <div class="clearfix"></div>
        </div>
  
    </div>

</div>

<div class="col-md-9 col-xs-12 p-0">

    <div class="col-md-12 col-xs-12">
        <!-- Ordering -->
        <form method="get" action="{{route('front.get.cat.filter')}}" >
            <select class="orderby" name="value" onchange="this.form.submit()">
                <option value="">@lang('front.choose') </option>
                <option value="order">   @lang('front.moreOrder')  </option>
                <option value="price">   @lang('front.priceLess') </option>
                <option value="view">  @lang('front.moreViews')  </option>
            </select>
        </form>
    </div>

    <div class="col-md-12 col-xs-12 p-0">
        <!-- Product #1 -->
        <div class="products">



                @foreach($products as $prs)
                <div class="shop col-md-4">
                    <figure class="product">
                        <div class="mediaholder">
                            <a href="{{route('front.get.product.showProd',[$prs->slug,$prs->cat->slug])}}">
                                <img alt="{{$prs->name}}" src="{{getImg(PRODUCT_PATH.$prs->img)}}" class="img-show" />
                                <div class="cover">
                                    <img alt="{{$prs->name}}" src="{{getImg(PRODUCT_PATH.$prs->img)}}" class="img-show" />
                                </div>
                            </a>
                            <a class="product-button pointer addToCart" data-prod-id="{{$prs->id}}">
                                <i class="fa fa-shopping-cart"></i> أضف الى السلة 
                            </a>
                        </div>

                        <a href="{{route('front.get.product.showProd',[$prs->slug,$prs->cat->slug])}}">
                            <section>
                                <span class="product-category"> @lang('front.productsSpecial') </span>
                                <h5>  {{\Str::Words($prs->name,'4',' ')}} </h5>
                                <span class="product-price">@lang('front.namePrice')  {{$prs->price}} </span>
                            </section>
                        </a>
                        @if(client())
                            @if(!$prs->checkWishlist($prs->id,client()->id))
                            <button class="btn  addToWishlist wishlist" data-route="{{route('front.get.client.wishlist',[$prs->id])}}"> <i class="fa fa-heart"></i>  </button>
                            @else  
                            <button class="btn   wishlist" > <i class="fa fa-heart active"></i>  </button>
                            @endif
                        @endif
                    </figure>
                </div>
                @endforeach
 

            <div class="clearfix"></div>

            
            <!-- Pagination -->
            <div class="pagination-container">
                {{$products->appends(request()->query())->links()}}
            </div>

        </div>



    </div>

</div>




</div>

@endsection

@section('script')
    @include('front.inc.ajaxCart')
    @if(client())
		@include('front.inc.ajaxWishlist')
	@endif
@endsection