@extends('front.main')

@section('style')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d5a79f7ee108b2e"></script>


@endsection
@section('seo')

	<?php $seo = json_decode($row->seo); ?>
	{!! str_replace("<br />", "", json_data($seo,'seoHeader')) !!}

@endsection


@section('content')





<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
<div class="parallax-overlay"></div>

<div class="container">
    <div class="parallax-content">

        <nav id="breadcrumbs">
            <ul>
                <li><a href="{{route('front.get.home.index')}}">الرئيسية</a></li>
                <li><a href="{{route('front.get.cat.showCat',[$cat->slug])}}">  {{$cat->name}} </a></li>
                <li>   {{$row->name}} </li>
            </ul>
        </nav>


    </div>
</div>
</section>


<div class="container">
<div class="col-md-6 col-xs-12 p-0">
    <div class="product-page">

        <!-- Headline -->
        <section class="title">
            <h2>  {{$row->name}} </h2>
            <span class="product-price-discount"><i> {{$row->price}}  @lang('front.namePrice') </i></span>

            <p>
            <hr>
                 رقم المنتج : {{$row->id}} 
            </p>

            @if($row->size_id)
                @if($row->size)
                    <p>
                        
                            الوزن : {{$row->fit }} {{ $row->size->name}}
                    </p>
                @endif
            @else  
                @if($row->fit)
                <p>       
                     الوزن : {{$row->fit }} 
                </p>
                @endif
            @endif



            @if($row->stock)
                <p>
                       حالة التوفر : المنتج متوفر 
                </p>
            @else  
                <p>
                     حالة التوفر  : المنتج غير متوفر 
                </p>
            @endif
                <p>
                     السعر : {{$row->price}}  @lang('front.namePrice') 
                </p>

            


            <div class="reviews-counter">
                <div class="rating five-stars">
                    <div class="star-rating">

                        <i class="fa fa-star" style="@if($row->rating->avg('rate') >= 1) color:red; @endif "  ></i>
                        <i class="fa fa-star" style="@if($row->rating->avg('rate') >= 2) color:red; @endif "></i>
                        <i class="fa fa-star" style="@if($row->rating->avg('rate') >= 3) color:red; @endif "></i>
                        <i class="fa fa-star" style="@if($row->rating->avg('rate') >= 4) color:red; @endif "></i>
                        <i class="fa fa-star" style="@if($row->rating->avg('rate') >= 5) color:red; @endif "></i>
                    </div>

                    



                </div>
                <span>التقييمات</span>
            </div>
        </section>

        <section class="linking">

            <form class="num_view" action='#'>
                <div class="qtyminus"></div>
                <input type='text' name="quantity" value='1' class="qty" id="qtyProd" />
                <div class="qtyplus"></div>
            </form>

            @if($row->stock)
            <a  class="button adc pointer addToCart" data-prod-id="{{$row->id}}" >أضف الى السلة</a>
            @endif
            <div class="clearfix"></div>

        </section>

        <!-- Text Parapgraph -->
        <section>
            <p class="margin-reset">
                {!! $row->desc !!}
            </p>

            <!-- Share Buttons -->
            <!-- <div class="share-buttons">
                <ul>
                    <li><a href="#">مشاركة</a></li>
                    <li class="share-facebook"><a href="#">فيس بوك</a></li>
                    <li class="share-twitter"><a href="#">تويتر</a></li>
                    <li class="share-gplus"><a href="#">جوجل بلس</a></li>
                </ul>
            </div> -->
            <div class="clearfix"></div>

        </section>


       

    </div>
</div>
<div class="col-md-6 col-xs-12 p-0">
        <!-- Slider
        ================================================== -->
        <div class="slider-padding">
            <div id="product-slider" class="royalSlider rsDefault">
                <img class="rsImg" style="width:100%; height:350px;" src="{{getImg(PRODUCT_PATH.$row->img)}}" data-rsTmb="{{getImg(PRODUCT_PATH.$row->img)}}" alt="" />
                @foreach($row->images as $img)
                <img class="rsImg" style="width:100%; height:350px;" src="{{getImg(PRODUCT_PATH.$img->name)}}" data-rsTmb="{{getImg(PRODUCT_PATH.$img->name)}}" alt="" />
                @endforeach
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12 ">
        
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox text-center"></div>
            

        </div>
    </div>
</div>

<div class="container">

<div class="row">
    <!-- Headline -->
    <div class="col-md-12 col-xs-12">
        <h3 class="headline">المنتجات المميزة</h3>
        <span class="line margin-bottom-0"></span>
    </div>

    <!-- Carousel -->
    <div id="new-arrivals" class="showbiz-container col-md-12 col-xs-12">

        <!-- Navigation -->
        <div class="showbiz-navigation">
            <div id="showbiz_left_1" class="sb-navigation-left"><i class="fa fa-angle-left"></i></div>
            <div id="showbiz_right_1" class="sb-navigation-right"><i class="fa fa-angle-right"></i></div>
        </div>
        <div class="clearfix"></div>

        <!-- Products -->
        <div class="showbiz" data-left="#showbiz_left_1" data-right="#showbiz_right_1" data-play="#showbiz_play_1" >
            <div class="overflowholder">

                <ul>

                @foreach($specials as $prs)
                <li>
                    <figure class="product">
                        <div class="mediaholder">
                            <a href="{{route('front.get.product.showProd',[$prs->slug,$prs->cat->slug])}}">
                                <img alt="{{$prs->name}}" src="{{getImg(PRODUCT_PATH.$prs->img)}}" class="img-show" />
                                <div class="cover">
                                    <img alt="{{$prs->name}}" src="{{getImg(PRODUCT_PATH.$prs->img)}}" class="img-show" />
                                </div>
                            </a>
                            
                            <a  class="product-button addToCart" data-prod-id="{{$prs->id}}">
                                <i class="fa fa-shopping-cart"></i> أضف الى السلة 
                            </a>
                        </div>

                        <a href="{{route('front.get.product.showProd',[$prs->slug,$prs->cat->slug])}}">
                            <section>
                                <span class="product-category"> @lang('front.productsSpecial') </span>
                                <h5>  {{\Str::Words($prs->name,'4',' ')}} </h5>
                                <span class="product-price">@lang('front.namePrice')  {{$prs->price}} </span>
                            </section>
                        </a>

                        @if(client())
                            @if(!$prs->checkWishlist($prs->id,client()->id))
                            <button class="btn  addToWishlist wishlist" data-route="{{route('front.get.client.wishlist',[$prs->id])}}"> <i class="fa fa-heart"></i>  </button>
                            @else  
                            <button class="btn   wishlist" > <i class="fa fa-heart active"></i>  </button>
                            @endif
                        @endif
                    </figure>
                </li>
                @endforeach


                



                </ul>
                <div class="clearfix"></div>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>








@endsection

@section('script')
    @include('front.inc.ajaxCart')
    @if(client())
		@include('front.inc.ajaxWishlist')
	@endif
@endsection
