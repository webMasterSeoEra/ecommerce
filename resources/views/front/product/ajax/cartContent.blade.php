

<div class="cart-btn">
    <a href="#" class="button adc">@lang('front.namePrice') {{Cart::getSubTotal()}}</a>
</div>
<div class="cart-list">

    <div class="arrow"></div>

    <div class="cart-amount">
        <span> @lang('front.prodInCart') {{Cart::getContent()->count()}} </span>
    </div>
    @if(Cart::getContent()->count())
    <ul>
        @foreach(Cart::getContent() as $item)
        <li>
            <a href="#"><img src="{{getImg(PRODUCT_PATH.$item->attributes->img)}}" alt="" style="height:55px !important;" /></a>
            <a href="#">{{\Str::Words($item->name)}}</a>
            <span>@lang('front.namePrice') {{$item->quantity}} x {{\Str::Words($item->price)}} </span>
            <div class="clearfix"></div>
        </li>
        @endforeach
    </ul>

    <div class="cart-buttons button">
        <a href="{{route('front.get.cart.show')}}" class="view-cart" ><span data-hover="View Cart"><span>@lang('front.showCart')</span></span></a>
        <a href="{{route('front.get.checkout.show')}}" class="checkout"><span data-hover="Checkout">أنهاء الشراء</span></a>
    </div>
    @endif

    <div class="clearfix">

    </div>
</div>