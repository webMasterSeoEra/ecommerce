<script>


$("#btn-submint").click(function()
    {
        var el = $(this); 

        var name            = $("#name").val();
        var mobile          = $("#mobile").val();
        var city            = $("#city").val();
        var district        = $("#district_name").val();
        var street          = $("#street_name").val();
        var bulding         = $("#bulding_number").val();
        var notes           = $("#notes").val();
        var shippCompany    = $("#shippCompany").val();
        var shippway        = $("#shippway").val();



        if(!name)
        {
            $("#name").parents("#collapseTwo").addClass("in");
            $("#name").addClass("border-red");
            $("#name").next("div").text("هذا الحقل مطلوب");
            $('html, body').animate({
                scrollTop: $("#collapseTwo").offset().top
            },500);
            return false;
        }

        if(!mobile)
        {
            $("#mobile").parents("#collapseTwo").addClass("in");
            $("#mobile").addClass("border-red");
            $("#mobile").next("div").text("هذا الحقل مطلوب");
            $('html, body').animate({
                scrollTop: $("#collapseTwo").offset().top
            },500);
            return false;
        }

        if(!city)
        {
            $("#city").parents("#collapseTwo").addClass("in");
            $("#city").addClass("border-red");
            $("#city").next("div").text("هذا الحقل مطلوب");
            $('html, body').animate({
                scrollTop: $("#collapseTwo").offset().top
            },500);
            return false;
        }
        if(!district)
        {
            $("#district_name").parents("#collapseTwo").addClass("in");
            $("#district_name").addClass("border-red");
            $("#district_name").next("div").text("هذا الحقل مطلوب");
            $('html, body').animate({
                scrollTop: $("#collapseTwo").offset().top
            },500);
            return false;
        }
        if(!street)
        {
            $("#street_name").parents("#collapseTwo").addClass("in");
            $("#street_name").addClass("border-red");
            $("#street_name").next("div").text("هذا الحقل مطلوب");
            $('html, body').animate({
                scrollTop: $("#collapseTwo").offset().top
            },500);
            return false;
        }
        if(!bulding)
        {
            $("#bulding_number").parents("#collapseTwo").addClass("in");
            $("#bulding_number").addClass("border-red");
            $("#bulding_number").next("div").text("هذا الحقل مطلوب");
            $('html, body').animate({
                scrollTop: $("#collapseTwo").offset().top
            },500);
            return false;
        }

        if(!notes)
        {
            $("#notes").parents("#collapseThree").addClass("in");
            $("#notes").addClass("border-red");
            $("#notes").next("div").text("هذا الحقل مطلوب");
            $('html, body').animate({
                scrollTop: $("#collapseThree").offset().top
            },500);
            return false;
        }


        if(!shippCompany)
        {
            $("#shippCompany").parents("#collapseThree").addClass("in");
            $("#shippCompany").addClass("border-red");
            $("#shippCompany").next("div").text("هذا الحقل مطلوب");
            $('html, body').animate({
                scrollTop: $("#collapseThree").offset().top
            },500);
            return false;
        }


        if(!shippway)
        {
            $("#shippway").parents("#collapseThree").addClass("in");
            $("#shippway").addClass("border-red");
            $("#shippway").next("div").text("هذا الحقل مطلوب");
            $('html, body').animate({
                scrollTop: $("#collapseThree").offset().top
            },500);
            return false;
        }

        else
        {

            //  all right 
            // $("select,input,textarea").removeClass("border-red");
            // $("select,input,textarea").next("div").text("");
            // ajax code   
            var formData  = new FormData(jQuery('#FormCheckout')[0]);


            $.ajax({
                type: "POST",
                url: "{{route('front.post.checkout.do_checkout')}}",
                data:formData,
                contentType: false,
                processData: false,
                beforeSend:function()
                {
                    $(".submit-order").attr("disabled","disabled");
                },
                success: function (data) 
                {
                    Swal.fire(
                    {
                    position: 'top-end',
                    type: 'success',
                    title: "{{trans('front.msg.orderSuccess')}}",
                    showConfirmButton: false,
                    timer: 5000
                    })

                //   $(".submit-order").prop( "disabled", false );
                window.location.href = "{{route('front.get.home.index')}}";
                }, error: function (xhr, status, error) 
                {
                    
                    $("#errors").html('');
                    $.each(xhr.responseJSON.errors, function (key, item) 
                    {
                    $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
                    });
                //   $(".submit-order").prop( "disabled",false);
                    $(".submit-order").removeAttr("disabled");
                
                }
            });
        }

    });


</script>