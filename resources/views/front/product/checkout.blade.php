@extends('front.main')
@section('content')


<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
<div class="parallax-overlay"></div>

<div class="container">
    <div class="parallax-content">

        <nav id="breadcrumbs">
            <ul>
                <li><a href="{{ route('front.get.home.index') }}">@lang('front.home')</a></li>
                <li>    @lang('front.CompleetBuy')   </li>
            </ul>
        </nav>


    </div>
</div>
</section>


<div class="container">

		<div class="row">
			<div class="col-md-12 col-xs-12">
				<h3 class="headline">  {{json_data($sc,'checkitem1')}} </h3><span class="line margin-bottom-35"></span><div class="clearfix"></div>

                <form id="FormCheckout" novalidate>
                @csrf

                

				<div class="panel-group acc_payment" id="accordion" role="tablist" aria-multiselectable="true">
                    
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                {{json_data($sc,'checkitem2')}}
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
									<section class="col-md-12 col-xs-12 p-0">
                                        
										<h4 class="col-xs-12">  {{json_data($sc,'checkitem3')}}  </h4>
										<div class="form-row form-row-wide col-md-12 col-xs-12">
											<input class="inp_fato"   type="text" name="name" id="name" value="{{client()->name}}" placeholder="الاسم بالكامل ">
                                            <div class="text-right"></div>
                                        </div>
                                        
                                        <h4 class="col-xs-12">  {{json_data($sc,'checkitem4')}}  </h4>
										<div class="form-row form-row-wide col-md-12 col-xs-12">
											<input class="inp_fato"   type="text" name="mobile" id="mobile" value="{{client()->mobile}}" placeholder=" رقم الهاتف ">
                                            <div class="text-right"></div>
                                        </div>
                                        
                                        <h4 class="col-xs-12">     المدينة  </h4>
										<div class="form-row form-row-wide col-md-12 col-xs-12">
                                            
                                            <select class="inp_fato form-control"  name="city" id="city">
                                                <option value=""> اختار المدينة </option>
                                                @foreach($cities as $city)
                                                    <option value="{{$city->id}}"> {{$city->name}}  </option>
                                                @endforeach
                                            </select>
                                            <div class="text-right"></div>
                                        </div>

                                        

                                        <h4 class="col-xs-12">   الحى    </h4>
										<div class="form-row form-row-wide col-md-12 col-xs-12">
                                            <input class="inp_fato"  type="text" name="district_name" id="district_name" >
                                            <div class="text-right"></div>
                                        </div>


                                        <h4 class="col-xs-12">   اسم الشارع  </h4>
										<div class="form-row form-row-wide col-md-12 col-xs-12">
                                            <input class="inp_fato"  type="text" name="street_name" id="street_name"  >
                                            <div class="text-right"></div>
                                        </div>

									
										<h4 class="col-xs-12">    رقم المبنى  </h4>
										<div class="form-row form-row-wide col-md-12 col-xs-12">
                                            <input class="inp_fato"  type="text" name="bulding_number" id="bulding_number">
                                            <div class="text-right"></div>
                                        </div>

                                        


                                        
                                        


									</section>
									<!-- <p class="form-row col-md-12 col-xs-12">
										<br>
										<input type="submit" class="button" name="login" value="متابعة">
									</p> -->
								
							</div>
						</div>
                    </div>
                    
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingThree">
							<h4 class="panel-title">
								<a class="collapsed secondCollabse" data-toggle="collapse" role="button"  data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                {{json_data($sc,'checkitem6')}} 
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
								

									<section class="login login_payment col-xs-12 col-md-12">
										<h4> {{json_data($sc,'checkitem7')}}  </h4>
										<br>
                                        <textarea class="form-control" name="notes" id="notes" rows="5"></textarea>
                                        <div class="text-right"></div>
                                    </section>
                                    
                                    <h4 class="col-xs-12">     شركة الشحن  </h4>
                                    <div class="form-row form-row-wide col-md-12 col-xs-12">
                                        
                                        <select class="inp_fato form-control" name="shippCompany" id="shippCompany">
                                            <option value=""> اختار شركة الشحن </option>
                                            @foreach($shippCompanies as $row)
                                                <option value="{{$row->id}}"> {{$row->name}}  </option>
                                            @endforeach
                                        </select>
                                        <div class="text-right"></div>
                                    </div>



                                    <h4 class="col-xs-12">     طريقة الشحن  </h4>
                                    <div class="form-row form-row-wide col-md-12 col-xs-12">
                                        
                                        <select class="inp_fato form-control" name="shippWay" id="shippway">
                                            <option value="">  اختار طريقة الشحن </option>
                                            @foreach($shippWay as $row)
                                                <option value="{{$row->id}}"> {{$row->name}}  </option>
                                            @endforeach
                                        </select>
                                        <div class="text-right"></div>
                                    </div>
                                

							</div>
						</div>
                    </div>
                    
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingfor">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefor" aria-expanded="false" aria-controls="collapsefor">
                                {{json_data($sc,'checkitem8')}}
								</a>
							</h4>
						</div>
						<div id="collapsefor" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfor">
							<div class="panel-body">
								<section class="checkout-summary col-md-12 col-xs-12 p-0">

									<!-- Cart -->
                                    <table class="cart-table responsive-table">
                                        <thead class="head_table">
                                            <tr>
                                                <th> {{json_data($sc,'checkitem9')}}</th>
                                                <th> {{json_data($sc,'checkitem10')}}</th>
                                                <th>{{json_data($sc,'checkitem11')}}</th>
                                                <th class="text-center"> {{json_data($sc,'checkitem12')}}</th>
                                     
                                            </tr>
                                        </thead>
                                        


                                        @foreach(Cart::getContent() as $item)
                                        <!-- Item #1 -->
                                        <tr>
                                            <td class="cart-title"><a href="#">  {{\Str::Words($item->name)}}  </a></td>
                                            <td> @lang('front.namePrice') {{$item->price}} </td>
                                            <td>
                                                {{$item->quantity}}
                                            </td>
                                            <td class="cart-total ">@lang('front.namePrice') {{$item->price * $item->quantity}}</td>
                                        </tr>
                                        @endforeach


                                    </table>

                                    <!-- Cart Totals -->

                                    <div class="col-md-4 col-xs-12 cart-totals p-0">
                                        <table class="cart-table margin-top-5">

                                            <tr>
                                                <th>{{json_data($sc,'checkitem13')}}</th>
                                                <td><strong id="total">SR {{Cart::getSubTotal()}} </strong></td>
                                            </tr>

                                            <tr>
                                                <th>{{json_data($sc,'checkitem14')}}  </th>
                                                <td>SR {{$set->additional_value}}</td>
                                            </tr>

                                            <tr>
                                                <th> {{json_data($sc,'checkitem15')}}   </th>
                                                <td><strong  id="supTotal">SR {{Cart::getSubTotal() + $set->additional_value}}</strong></td>
                                            </tr>

                                        </table>
                                        <br>
                                        <!--<a href="#" class="calculate-shipping"><i class="fa fa-arrow-circle-down"></i> أنهاء الطلب </a>-->
                                    </div>


                                    <h4 class="col-xs-12">   {{json_data($sc,'checkitem16')}} </h4>
                                    <p class="form-row form-row-wide col-md-12 col-xs-12">
                                        <input class="inp_fato"   type="text" name="cupon_number" id=""  placeholder="   {{json_data($sc,'checkitem17')}}  ">
                                    </p>

                                    <hr> 
                                    <label>
                                            <input type="radio" name="accept" id="blankRadio1" required  > 
                                            <a href="{{route('front.get.home.cond')}}">  {{json_data($sc,'checkitem18')}}</a> 
                                    </label>

                                    <hr>

                                    <h4 class="col-xs-12">     طريقة الدفع  </h4>
                                    <p class="form-row form-row-wide col-md-12 col-xs-12">
                                        
                                        <select class="inp_fato form-control" >
                                            <option value="">  كاش </option>
                                            <option value="">  اونلاين </option>
                                        </select>
                                    </p>

                                    <label style="color: #215d0e; font-weight: bold;">
                                    {{json_data($sc,'checkitem19')}}
                                    </label>
                                    

                                    <ul id="errors"></ul>
                                    



								</section>

								<p class="form-row col-md-12 col-xs-12 p-0">
                                    <br>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="submit" id="btn-submint"  class="button submit-order"  value=" {{json_data($sc,'checkitem20')}} " >
								</p>
							</div>
						</div>
                    </div>
                    

                    
                </div>
                
                </form>

			</div>
		</div>

	</div>

@endsection



@section('script')


    @include('front.product.ajax.ajaxCheckout')


<script type="text/javascript">

</script>

@endsection