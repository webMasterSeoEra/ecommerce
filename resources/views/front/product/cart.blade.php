@extends('front.main')
@section('content')



<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

		<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
		<div class="parallax-overlay"></div>

		<div class="container">
			<div class="parallax-content">

				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{route('front.get.home.index')}}">@lang('front.home')</a></li>
						<li> @lang('front.cart') </li>
					</ul>
				</nav>


			</div>
		</div>
	</section>


	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div class="cart">

					@if(Cart::getContent()->count())
					<!-- Cart -->
					<table class="cart-table responsive-table">
						<thead class="head_table">
							<tr>
								<th>{{json_data($sc,'cartitem1')}}  </th>
								<th> {{json_data($sc,'cartitem2')}}</th>
								<th> {{json_data($sc,'cartitem3')}} </th>
								<th>{{json_data($sc,'cartitem4')}}</th>
								<th> {{json_data($sc,'cartitem5')}}</th>
								<th> {{json_data($sc,'cartitem6')}}</th>
							</tr>
                        </thead>
                        


                        @foreach(Cart::getContent() as $item)
						<!-- Item #1 -->
						<tr>
							<td><img src="{{getImg(PRODUCT_PATH.$item->attributes->img)}}" style="height:100px;" alt=""/></td>
							<td class="cart-title"><a href="#">  {{\Str::Words($item->name)}}  </a></td>
							<td> @lang('front.namePrice') {{$item->price}} </td>
							<td>
								<form action='#' class="quant_table"  >
									<div class="qtyminus qtyCart" data-cart-id="{{$item->id}}" data-type="m"></div>
									<input type='text' name="quantity" value="{{$item->quantity}}" class="qty"  />
									<div class="qtyplus qtyCart" data-cart-id="{{$item->id}}" data-type="p" ></div>
								</form>
							</td>
							<td class="cart-total">@lang('front.namePrice') {{$item->price * $item->quantity}}</td>
							<td><a href="{{route('front.get.cart.delete',[$item->id])}}" class="cart-remove"></a></td>
                        </tr>
                        @endforeach


					</table>




					<!-- Cart Totals -->

					<div class="col-md-4 col-xs-12 cart-totals p-0">
						<table class="cart-table margin-top-5">

							<tr>
								<th>{{json_data($sc,'cartitem7')}}</th>
								<td><strong id="total">SR {{Cart::getSubTotal()}} </strong></td>
							</tr>

							<tr>
								<th>{{json_data($sc,'cartitem8')}}  </th>
								<td>SR {{$set->additional_value}}</td>
							</tr>

							<tr>
								<th> {{json_data($sc,'cartitem9')}}   </th>
								<td><strong  id="supTotal">SR {{Cart::getSubTotal() + $set->additional_value}}</strong></td>
							</tr>

						</table>
						<br>
						<!--<a href="#" class="calculate-shipping"><i class="fa fa-arrow-circle-down"></i> أنهاء الطلب </a>-->
					</div>

					<!-- Apply Coupon Code / Buttons -->
					<table class="cart-table copon_table bottom">
						<tr>
							<th>
								<div class="cart-btns">
									<a href="{{route('front.get.checkout.show')}}" class="button color cart-btns proceed">  {{json_data($sc,'cartitem10')}} </a>
									<a href="{{route('front.get.home.index')}}" class="button gray cart-btns">{{json_data($sc,'cartitem11')}}</a>
								</div>
								<!-- <form action="#" method="get" class="apply-coupon">
									<input class="search-field" type="text" placeholder="الرجاء ادخال رمز قسيمة التخفيض" value=""/>
									<a href="#" class="button btn_copon"> أعتمد التخفيض</a>
								</form> -->
							</th>
						</tr>

					</table>

					@else  
					 
					<div class="alert alert-info text-center ">لا يوجد منتجات بالسلة !</div>

					@endif

				</div>
			</div>
		</div>
    </div>
    






@endsection


@section('script')

<script>


$(document).on("click",".qtyCart",function(e)
{

    var el = $(this);
    // var prodId = el.attr("data-cart-id")
    var prodId = el.attr("data-cart-id");
    var type = el.attr("data-type");
    
    $.ajax({
        type: "GET",
        url: "{{ route('front.get.cart.update') }}",
        data:{prodId:prodId,type:type},
        cache: false,
        success: function (data) 
        {   
            $("#total").html("SR "+data.total)
            $("#supTotal").html("SR "+data.subtotal)
            el.parents("tr").find(".cart-total").html("SR "+data.itemPrice)
        },error: function (data) 
        {
            
        }
    });
    e.preventDefault;




});





</script>

@endsection