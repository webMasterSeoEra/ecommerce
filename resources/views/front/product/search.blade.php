@extends('front.main')

@section('content')





<section class="parallax-titlebar fullwidth-element"  data-background="#000" data-opacity="0.45" data-height="160">

<img src="{{furl()}}/images/parallax-2.jpg" alt="" />
<div class="parallax-overlay"></div>

<div class="container">
    <div class="parallax-content">

        <nav id="breadcrumbs">
            <ul>
                <li><a href="{{route('front.get.home.index')}}">الرئيسية</a></li>
                <li> بحث  </li>
            </ul>
        </nav>


    </div>
</div>
</section>






<div class="container">

    <div class="row">
        <!-- Headline -->
        <div class="col-md-12 col-xs-12">
            <h3 class="headline"> نتائج البحث  </h3>
            <span class="line margin-bottom-0"></span>
        </div>



        @if($products->count())
        <div class="col-md-12 col-xs-12">
            <!-- Ordering -->
            <form method="get" action="{{route('front.get.cat.filter')}}" >
                <select class="orderby" name="value" onchange="this.form.submit()">
                    <option value="">@lang('front.choose') </option>
                    <option value="order">   @lang('front.moreOrder')  </option>
                    <option value="price">   @lang('front.priceLess') </option>
                    <option value="view">  @lang('front.moreViews')  </option>
                </select>
            </form>
        </div>

        <div class="col-md-12 col-xs-12 p-0">
                <!-- Product #1 -->
                <div class="products">



                        @foreach($products as $prs)
                        <div class="shop col-md-3">
                            <figure class="product">
                                <div class="mediaholder">
                                    <a href="{{route('front.get.product.showProd',[$prs->slug,$prs->cat->slug])}}">
                                        <img alt="{{$prs->name}}" src="{{getImg(PRODUCT_PATH.$prs->img)}}" class="img-show" />
                                        <div class="cover">
                                            <img alt="{{$prs->name}}" src="{{getImg(PRODUCT_PATH.$prs->img)}}" class="img-show" />
                                        </div>
                                    </a>
                                    <a class="product-button pointer addToCart" data-prod-id="{{$prs->id}}">
                                        <i class="fa fa-shopping-cart"></i> أضف الى السلة 
                                    </a>
                                </div>

                                <a href="{{route('front.get.product.showProd',[$prs->slug,$prs->cat->slug])}}">
                                    <section>
                                        <span class="product-category"> @lang('front.productsSpecial') </span>
                                        <h5>  {{\Str::Words($prs->name,'4',' ')}} </h5>
                                        <span class="product-price">@lang('front.namePrice')  {{$prs->price}} </span>
                                    </section>
                                </a>
                                @if(client())
									@if(!$prs->checkWishlist($prs->id,client()->id))
									<button class="btn  addToWishlist wishlist" data-route="{{route('front.get.client.wishlist',[$prs->id])}}"> <i class="fa fa-heart"></i>  </button>
									@else  
									<button class="btn   wishlist" > <i class="fa fa-heart active"></i>  </button>
									@endif
								@endif
                            </figure>
                        </div>
                        @endforeach
        

                    <div class="clearfix"></div>

                    
                    <!-- Pagination -->
                    <div class="pagination-container">
                        {{$products->appends(request()->query())->links()}}
                    </div>

                </div>



            </div>
        @else  
            <div class="alert alert-success text-center"> @lang('front.searchNotFound') </div>
        @endif

        </div>
    </div>









@endsection

@section('script')
    @include('front.inc.ajaxCart')
    @if(client())
		@include('front.inc.ajaxWishlist')
	@endif
@endsection
