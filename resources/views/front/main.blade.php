@include('front.inc.header')
@include('front.inc.topBar')
@include('front.inc.nav')

@yield('content')

@include('front.inc.footer')