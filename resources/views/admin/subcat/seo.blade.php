
@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">

                <form id="FormEdit" enctype="multipart/form-data" >
                        <div class="coverLoading"><img src="{{aurl()}}/loader.gif"></div>
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="form-group row">
                            <ul id="errorsEdit"></ul>
                        </div>
                        <input type="hidden" name="id" class="id" value="{{$row->id}}">



                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.seoHeader')</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" dir="ltr"   name="seoHeader" rows="15" >{!! str_replace("<br />", "", json_data($seoData,'seoHeader')) !!}</textarea>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.seoFooter')</label>
                            <div class="col-sm-10">
                                <textarea class="form-control"dir="ltr"   name="seoFooter" rows="10" >{!!  str_replace("<br />" , "", json_data($seoData,'seoFooter'))!!}</textarea>
                            </div>
                        </div>


                        



                        



                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary fontLight submit-btn"  type="submit"  >
                                        @lang('admin.save')
                                </button>
                            </div>
                        </div>
                </form>



                
                
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->





@endsection


@section('script')


<?php $routeEdit = route('admin.put.subcat.seoUpdate'); ?>
@include('admin.inc.ajax.update')


@endsection