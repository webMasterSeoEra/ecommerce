@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">
                <h4 class="mt-0 header-title">  @lang('admin.category')  </h4>
                <h4 class="mt-0 header-title"> 
                    <button type="button" data-toggle="modal" data-target=".add-modal" class="btn btn-secondary btn-sm waves-effect fontLight">@lang('admin.addNew')</button> 
                </h4>
                <div class="table-responsive">
                    <div id="coverloadingTable">
                        <img src="{{aurl()}}/loader.gif">
                    </div>
                    <table class="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('admin.name')</th>
                                <th>@lang('admin.category')</th>
                                <th>@lang('admin.actions')</th>
                                <th>@lang('admin.seo') </th>

                            </tr>
                        </thead>
                        <tbody>

                            @foreach($subcats as $row)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$row->name}}</td>
                                <td>{{$row->cat->name}}</td>
                                <td class="text-center">
                                    <a href="javascript:void(0);"  data-cat="{{$row->category_id}}" data-id="{{$row->id}}" data-name="{{$row->name}}"  data-slug="{{$row->slug}}"  class="m-r-15 text-muted edit-row" data-toggle="modal" data-target=".edit-modal" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-pencil font-18"></i></a>
                                    <a href="javascript:void(0);" data-route="{{route('admin.get.subcat.delete',[$row->id])}}" class="text-muted  delete-row " data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-close font-18"></i></a>
                                </td>
                                <td>
                                    <a href="{{route('admin.get.subcat.seo',[$row->id])}}" class="btn btn-info btn-sm">  @lang('admin.seo') <i class="fa fa-search-plus"></i> </a> 

                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->


    <div class="row">
        <div class="col-sm-12">
        {{$subcats->appends(request()->query())->links()}}
        </div>
    </div>





    @include('admin.subcat.forms.add')
    @include('admin.subcat.forms.edit')

@endsection

    <?php $routeAdd = route('admin.post.subcat.store'); ?>
    <?php $routeEdit = route('admin.put.subcat.update'); ?>

@section('script')



    @include('admin.inc.ajax.store')
    @include('admin.inc.ajax.delete')

    <script>
        $(document).on("click",".edit-row",function(){
            
            var el = $(this);
            $(".name").val(el.attr("data-name"));
            $(".slug").val(el.attr("data-slug"));
            $("#cat").val(el.attr("data-cat"));
            $(".id").val(el.attr("data-id"));

        }); 

    </script>

    @include('admin.inc.ajax.update')

@endsection