@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">
                <h4 class="mt-0 header-title">  @lang('admin.cities')  </h4>
                <h4 class="mt-0 header-title"> 
                    <button type="button" data-toggle="modal" data-target=".add-modal" class="btn btn-secondary btn-sm waves-effect fontLight">@lang('admin.addNew')</button> 
                </h4>
                <div class="table-responsive">
                    <div id="coverloadingTable">
                        <img src="{{aurl()}}/loader.gif">
                    </div>
                    <table class="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('admin.name')</th>
                                <th>@lang('admin.minNumberDiscount')</th>
                                <th>@lang('admin.discountValue')</th>
                                <th>@lang('admin.textDiscount')</th>
                                <th>@lang('admin.actions')</th>
               
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($rows as $row)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$row->name}}</td>
                                <td><b>{{$row->min_number}} </b> @lang('admin.langPrice')</td>
                                <td><b>{{$row->discount_value}} </b> @lang('admin.langPrice')</td>
                                <td>{{$row->discount_text}}</td>
                                <td class="text-center">
                                    <a href="javascript:void(0);"  data-id="{{$row->id}}" 
                                        data-name="{{$row->name}}" 
                                        data-min-number="{{$row->min_number}}" 
                                        data-discount="{{$row->discount_value}}"  data-toggle="modal" data-target=".edit-modal"  class="m-r-15 text-muted edit-row"  title="Edit"><i class="mdi mdi-pencil font-18"></i></a>
                                    <textarea style="display:none;">{!!$row->discount_text!!}</textarea>    
                                    <a href="javascript:void(0);" data-route="{{route('admin.get.city.delete',[$row->id])}}" class="text-muted  delete-row "  title="Delete"><i class="mdi mdi-close font-18"></i></a>

                                </td>
                         
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
        {{$rows->appends(request()->query())->links()}}
        </div>
    </div>



    <!-- end col -->


    @include('admin.city.forms.add')
    @include('admin.city.forms.edit')

@endsection

<?php $routeAdd = route('admin.post.city.store'); ?>
<?php $routeEdit = route('admin.put.city.update'); ?>
@section('script')
    @include('admin.inc.ajax.store')
    @include('admin.inc.ajax.delete')

    <script>
        $(document).on("click",".edit-row",function(){
            
            var el = $(this);
            $("#name").val(el.attr("data-name"));
            $("#minNumberDiscount").val(el.attr("data-min-number"));
            $("#discountValue").val(el.attr("data-discount"));
            $("#textDiscount").val(el.next("textarea").val());
            $("#id").val(el.attr("data-id"));

        }); 

    </script>

    @include('admin.inc.ajax.update')

@endsection