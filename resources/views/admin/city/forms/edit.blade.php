<!--  Modal content for the above example -->
<div class="modal fade edit-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">  @lang('admin.editData') </h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form id="FormEdit">
                        <div class="coverLoading"><img src="{{aurl()}}/loader.gif"></div>
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="form-group row">
                            <ul id="errorsEdit"></ul>
                        </div>
                        <input type="hidden" name="id" id="id">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">@lang('admin.name') * </label>
                            <div class="col-sm-10">
                                <input class="form-control name" name="name" type="search"  id="name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="minNumberDiscount" class="col-sm-2 col-form-label">@lang('admin.minNumberDiscount')</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="min_number" type="search"  id="minNumberDiscount">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="discountValue" class="col-sm-2 col-form-label">@lang('admin.discountValue')</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="discount_value" type="search"  id="discountValue">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="textDiscount" class="col-sm-2 col-form-label">@lang('admin.textDiscount')</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="discount_text" type="search"  id="textDiscount">
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-success fontLight"  type="submit"  >
                                        @lang('admin.save')
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
