@extends('admin.main')

@section('content')

 
<h4 class="mt-0 header-title">  @lang('admin.clients')  </h4>
    <!-- end col -->
          <!-- end row -->
        <div class="row">
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <!-- <h4 class="mt-0 header-title"> @lang('admin.export') </h4> -->
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('admin.name')</th>
                                <th>@lang('admin.email')</th>
                                <th>@lang('admin.mobile')</th>
                                <th>@lang('admin.address')</th>
                                <th>@lang('admin.actions')</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($clients as $row)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$row->name}}</td>
                                <td>{{$row->email}}</td>
                                <td>
                                    @if($row->mobile)
                                        {{$row->mobile}}
                                    @else  
                                        <span class="text-danger"> 
                                            @lang('admin.notFoundData') 
                                        </span>
                                    @endif
                                    
                                </td>
                                <td>
                                    @if($row->address)
                                    
                                        {{$row->address}}
                                 
                                    @else  
                                    <span class="text-danger">
                                        @lang('admin.notFoundData')
                                    </span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($row->order->count())
                                    <a href="{{route('admin.get.order.client',[$row->id])}}" class="text-muted " data-toggle="tooltip" data-placement="top" title="" data-original-title="Show Orders"><i class="mdi mdi-eye font-18"></i></a>
                                    @endif
                                    <a href="javascript:void(0);" data-route="{{route('admin.get.client.delete',[$row->id])}}" class="text-muted  delete-row " data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-close font-18"></i></a>
                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
        {{$clients->appends(request()->query())->links()}}
        </div>
    </div>


@endsection

@section('script')


    <!-- Required datatable js -->
    <script src="{{aurl()}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="{{aurl()}}/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/jszip.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/pdfmake.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/vfs_fonts.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.html5.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.print.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="{{aurl()}}/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/responsive.bootstrap4.min.js"></script>
    <!-- Datatable init js -->
    <script src="{{aurl()}}/assets/pages/datatables.init.js"></script>

    
    @include('admin.inc.ajax.delete')


@endsection