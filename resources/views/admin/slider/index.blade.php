@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">
                <h4 class="mt-0 header-title">  @lang('admin.slider')  </h4>
                <h4 class="mt-0 header-title"> 
                    <button type="button" data-toggle="modal" data-target=".add-modal" class="btn btn-secondary btn-sm waves-effect fontLight">@lang('admin.addNew')</button> 
                </h4>
                <div class="table-responsive">
                    <div id="coverloadingTable">
                        <img src="{{aurl()}}/loader.gif">
                    </div>
                    <table class="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                {!!create_th(trans('admin.img'))!!}
                                {!!create_th(trans('admin.name'))!!}
                                {!!create_th(trans('admin.link'))!!}
                                {!!create_th(trans('admin.actions'))!!}
                                <?php /* <th>@lang('admin.img')</th>
                                <th>@lang('admin.name')</th>
                                <th>@lang('admin.link')</th>
                                <th>@lang('admin.actions')</th> */ ?>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($sliders as $sl)
                            <tr>
                                <td scope="row">{{$loop->iteration}}</td>
                                {!!create_td(getImg(SLIDER_PATH.$sl->img),true)!!}
                                {!!create_td($sl->name)!!}
                                {!!create_td($sl->link)!!}
                                <td class="text-center">
                                    <a href="javascript:void(0);"  data-id="{{$sl->id}}" data-name="{{$sl->name}}" data-link="{{$sl->link}}" data-img="{{getImg(SLIDER_PATH.$sl->img)}}"  class="m-r-15 text-muted edit-row" data-toggle="modal" data-target=".edit-modal" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-pencil font-18"></i></a>
                                    <textarea style="display:none;">{{$sl->small_desc}}</textarea>
                                    <a href="javascript:void(0);" data-route="{{route('admin.get.slider.delete',[$sl->id])}}" class="text-muted  delete-row " data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-close font-18"></i></a>
                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->


    @include('admin.slider.forms.add')
    @include('admin.slider.forms.edit')

@endsection

<?php $routeAdd = route('admin.post.slider.store'); ?>
<?php $routeEdit = route('admin.put.slider.update'); ?>
@section('script')
    @include('admin.inc.ajax.store')
    @include('admin.inc.ajax.delete')

    <script>
        $(document).on("click",".edit-row",function(){
            
            var el = $(this);
            $("#name").val(el.attr("data-name"));
            $("#link").val(el.attr("data-link"));
            $("#small_desc").val(el.next("textarea").val());
            $("#id").val(el.attr("data-id"));

            // alert(el.attr("data-img"));
            $("#imgContainer").html('');
            $("#imgContainer").append('<img src="'+ el.attr("data-img") +'" class="img-thumbnail" >');

        }); 

    </script>

    @include('admin.inc.ajax.update')

@endsection