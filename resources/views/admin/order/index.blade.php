@extends('admin.main')

@section('content')


        
     
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <!-- <h4 class="mt-0 header-title"> @lang('admin.export') </h4> -->
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>@lang('admin.orderCode')</th>
                                        <th>@lang('admin.orderClient')</th>
                                        <th>@lang('admin.orderCity')</th>
                                        <th>@lang('admin.shippCampany')</th>
                                        <th>@lang('admin.shippWay')</th>
                                        <th>@lang('admin.paymentMethod')</th>
                                        <th>@lang('admin.orderStatus')</th>
                                        <th>@lang('admin.orderComponent')</th>
                                        <th>@lang('admin.orderPrice')</th>
                                        <th>@lang('admin.orderDate')</th>
                                        <th>@lang('admin.cupon')</th>
                                        <th>@lang('admin.changeStatus')</th>
                                        
                                        <th>@lang('admin.actions')</th>
                                        <th>@lang('admin.print')</th>
                                   
                                    </tr>
                                </thead>
                                <tbody>


                                @foreach($orders as $ord)
                                    <tr>
                                        <td>{{$ord->id}}</td>
                                        <td>{{$ord->client->name}}</td>
                                        <td>{{$ord->cityName->name}}</td>
                                        <td>{{$ord->shippCampany->name }} ({{$ord->shippCampany->price }} @lang('admin.langPrice')   ) </td>
                                        <td>{{$ord->shippWay->name}}</td>
                                        <td>{{trans('admin.'.$ord->payment_method)}}</td>
                                        <td>{{trans('admin.'.$ord->status)}} </td>
                                        <td>{{$ord->content->count()}}</td>
                                        <td class="text-center"> 
                                            <b> {{$ord->total_price +  $set->additional_value}} </b>
                                            @lang('admin.langPrice')  
                                        </td>
                                        <td>{{date('Y-m-d',strtotime($ord->created_at))}}</td>
                                        <td>
                                            @if($ord->cupon)
                                            {{ $ord->cupon->cupon_number }} ({{ $ord->percent_now }} % )
                                            @endif
                                        </td>

                                        <td>
                                            <form metthod="get" action="{{route('admin.get.order.changeStatus')}}" >
                                                <select class="form-control" name="status" required style="width:150px;" onchange='this.form.submit()'>
                                                    <option value="" >@lang('admin.changeStatus')</option>
                                                    <option value="pending" >@lang('admin.pending')</option>
                                                    <option value="shipping">@lang('admin.shipping')</option>
                                                    <option value="accepted">@lang('admin.accepted')</option>
                                                    <option value="cancelled">@lang('admin.refused')</option>
                                                </select>
                                                <input type="hidden" name="id" value="{{$ord->id}}" >
                                            </form>
                                        </td>



                                        @if(Request::segment('3') == 'pending')
                                            @include('admin.order.inc.pending')
                                        @endif

                                        @if(Request::segment('3') == 'shipping')
                                            @include('admin.order.inc.shipping')
                                        @endif

                                        @if(Request::segment('3') == 'accepted')
                                            @include('admin.order.inc.accepted')
                                        @endif

                                        @if(Request::segment('3') == 'refused')
                                            @include('admin.order.inc.refused')
                                        @endif


                                        <td class="text-center">
                                            <a href="{{route('admin.get.order.print',[$ord->id])}}"    class="text-muted"   title="Print This Order" > <i class="mdi mdi-cloud-print font-18"></i> </a>

                                        </td>

                                    </tr>
                                @endforeach

                                

                                

                                
                                    
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>

            <div class="row">
                <div class="col-sm-12">
                {{$orders->appends(request()->query())->links()}}
                </div>
            </div>
  


@endsection


@section('script')


    <!-- Required datatable js -->
    <script src="{{aurl()}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="{{aurl()}}/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/jszip.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/pdfmake.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/vfs_fonts.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.html5.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.print.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="{{aurl()}}/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/responsive.bootstrap4.min.js"></script>
    <!-- Datatable init js -->
    <script src="{{aurl()}}/assets/pages/datatables.init.js"></script>


    @include('admin.order.ajax')


@endsection