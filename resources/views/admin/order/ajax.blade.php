<script>
    

    // shipping
    $(".add-to-shipping").click(function(){

        var el = $(this);
        var route = $(this).attr("data-route")
        $.ajax(
        {
            type: "GET",url: route,cache: false,
            beforeSend:function()
            {
                el.parents("td").find('a').html('');
                el.html('<img src="{{aurl()}}/loader.gif" class="tdLoader" > ')
            },
            success: function (data) 
            {
                el.parents("tr").remove();
                Swal.fire(
                {
                    position: 'top-end',
                    type: 'success',
                    title: "{{trans('admin.msg.orderShipping')}}",
                    showConfirmButton: false,
                    timer: 2000
                })
            }, error: function (data) 
            {el.prop( "disabled", false );}
        });
    });



    // accept
    $(".add-to-accepted").click(function()
    {
        var el = $(this);
        var route = $(this).attr("data-route")
        $.ajax(
        {
            type: "GET",url: route,cache: false,
            beforeSend:function()
            {
                el.parents("td").find('a').html('');
                el.html('<img src="{{aurl()}}/loader.gif" class="tdLoader" > ')
            },
            success: function (data) 
            {
                el.parents("tr").remove();
                Swal.fire(
                {
                    position: 'top-end',
                    type: 'success',
                    title: "{{trans('admin.msg.orderAccepted')}}",
                    showConfirmButton: false,
                    timer: 2000
                })
            }, error: function (data) 
            {el.prop( "disabled", false );}
        });
    })




    // refuse
    $(".add-to-refused").click(function()
    {
        var el = $(this);
        var route = $(this).attr("data-route")
        $.ajax(
        {
            type: "GET",url: route,cache: false,
            beforeSend:function()
            {
                el.parents("td").find('a').html('');
                el.html('<img src="{{aurl()}}/loader.gif" class="tdLoader" > ')
            },
            success: function (data) 
            {
                el.parents("tr").remove();
                Swal.fire(
                {
                    position: 'top-end',
                    type: 'success',
                    title: "{{trans('admin.msg.orderRefused')}}",
                    showConfirmButton: false,
                    timer: 2000
                })
            }, error: function (data) 
            {el.prop( "disabled", false );}
        });
    })


    // delete
    $(".add-to-delete").click(function()
    {
        var el = $(this);
        var route = $(this).attr("data-route")
        $.ajax(
        {
            type: "GET",url: route,cache: false,
            beforeSend:function()
            {
                el.parents("td").find('a').html('');
                el.html('<img src="{{aurl()}}/loader.gif" class="tdLoader" > ')
            },
            success: function (data) 
            {
                el.parents("tr").remove();
                Swal.fire(
                {
                    position: 'top-end',
                    type: 'success',
                    title: "{{trans('admin.msg.deletedSuccess')}}",
                    showConfirmButton: false,
                    timer: 2000
                })
            }, error: function (data) 
            {el.prop( "disabled", false );}
        });
    })



</script>