<?php /* ?>
@foreach($orders as $ord)
    <tr>
        <td>{{$ord->id * 250}}</td>
        <td>{{$ord->client->name}}</td>
        <td>{{$ord->address}}</td>
        <td>{{trans('admin.'.$ord->payment_method)}}</td>
        <td>{{trans('admin.'.$ord->status)}} </td>
        <td>{{$ord->content->count()}}</td>
        <td>{{date('Y-m-d',strtotime($ord->created_at))}}</td>
        <td>
            @if($ord->cupon)
            {{ $ord->cupon->cupon_number }}
            @endif
        </td>
        <?php */ ?>
        <td>
            <a href="javascript:void(0);"    class="text-muted   add-to-delete"  data-route="{{route('admin.get.ordere.addToDelete',[$ord->id])}}" title="Delete" > <i class="mdi mdi-close font-18"></i> </a>
            <a href="{{route('admin.get.order.showContent',[$ord->id])}}"    class="text-muted"   title="Show" > <i class="mdi mdi-eye font-18"></i> </a>

        </td>
        
<?php /* ?>
    </tr>
@endforeach
<?php */ ?>