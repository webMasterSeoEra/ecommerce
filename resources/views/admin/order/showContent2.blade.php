<!DOCTYPE html>
 <html lang="ar">

    
<head>

    	<!-- Metas -->
        <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="byt-elhekma" />
		<meta name="keywords" content="byt-elhekma" />
		<meta name="description" content="byt-elhekma" />
		<meta name="author" content="" />

		<link rel="stylesheet" media="print" href="{{aurl()}}/print.css" >

		<!-- Title  -->
		<title>بيت الحكمة</title>

    </head>

    <body>


	<button  id="btn" style="padding:10px; background-color:lightblue; color:#222; font-size:20px; 
  border:0; display:block; width:120px; margin:auto; margin-bottom:50px;margin-top:50px; cursor:pointer; border:1px solid #222; ">طباعة</button>
  <hr>
<table style="width:960px;margin:0 auto;border:5px solid #ddd;line-height:1.8" id="printarea">
      <tbody>
          <tr>
              <td style="text-align:center;background:#f5f5f5">
				  <font face="Tahoma">
					<img src="{{getImg(SETTINGS_PATH.$set->logo1)}}" class="CToWUd a6T" tabindex="0">
						<div class="a6S" dir="rtl" style="opacity: 0.01; left: 300px; top: 163px;">
							<div aria-label="" data-tooltip-class="a1V" data-tooltip="">
								<div class="aSK J-J5-Ji aYr"></div>
							</div>
						</div>	
					</font>
				</td>
          </tr>
          <tr>
              <td style="padding:15px">
                  <p style="text-align:right">
					  <font face="Tahoma"><span style="background-color:transparent">&nbsp;</span>
						  <strong style="background-color:transparent">{{$row->client->name}} </strong>
						  <span style="background-color:transparent"> : مرحباً</span>
					  </font>
				  </p>
                  <div dir="rtl" style="color:rgb(34,34,34);font-family:Arial,Helvetica,sans-serif;font-size:small">
					<font face="Tahoma">تفاصيل الطلب من موقع بيت الحكمة للعطارة :</font>
				  </div>
                  <div style="text-align:center"><font face="Tahoma"><br></font></div>
                 
				 <table style="width: 100%;">
                      <tbody>
                          <tr>
                              <td>
							  <font face="Tahoma">
								<table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
									<thead>
										<tr>
											<td style="width:50%;font-size:12px;border-bottom:1px solid #dddddd;background-color:#efefef;;padding:7px;"></td>
											<td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">تفاصيل الطلب</td>
										</tr>

									</thead>
									<tbody>
										<tr>

											<td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
												<p style="direction: rtl;margin: 0;">رقم الطلب : <strong>{{$row->id}}</strong> </p>
												<p style="direction: rtl;margin: 0;">تاريخ الطلب : <strong>{{date('Y-m-d',strtotime($row->created_at))}}</strong> </p>
												<p style="direction: rtl;margin: 0;">طريقة الدفع : <strong>الدفع عند الاستلام</strong> </p>
												<p style="direction: rtl;margin: 0;"> سعر الشحن  : <strong>  {{$row->shippCampany->price}}  @lang('admin.langPrice')  </strong> </p>
											</td>

											<td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
												<p style="direction: rtl;margin: 0;">البريد الالكتروني : <strong>{{$row->client->email}}</strong> </p>
												<p style="direction: rtl;margin: 0;">رقم الهاتف او الجوال : <strong>{{$row->client->mobile}}</strong> </p>
												<p style="direction: rtl;margin: 0;">عنوان الاي بي : <strong>31.167.122.135</strong> </p>
												<p style="direction: rtl;margin: 0;">حالة الطلب : <strong> {{trans('admin.'.$row->status)}}  </strong> </p>
											</td>

										</tr>


									</tbody>
								</table>
								<br>
							  </font>
							  </td>
                          </tr>
                      </tbody>
                  </table>

				  <table style="width: 100%;">
					  <tbody>
					  <tr>
						  <td>
							  <font face="Tahoma">
								  <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
									  <thead>
									  <tr>
										  <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">عنوان الدفع</td>
										  <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">عنوان الشحن</td>
									  </tr>

									  </thead>
									  <tbody>
									  <tr>
										  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
											  <p style="direction: rtl;margin: 0;"> {{$row->client->name}}   </p>
											  <p style="direction: rtl;margin: 0;"> {{$row->bulding_number}}  -  {{$row->street_name}} - {{$row->district_name}} </p>
											  <p style="direction: rtl;margin: 0;"> {{$row->cityName->name}} </p>
											  <p style="direction: rtl;margin: 0;"> السعودية </p>
										  </td>
										  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
											  <p style="direction: rtl;margin: 0;"> {{$row->client->name}}  </p>
											  <p style="direction: rtl;margin: 0;">{{$row->bulding_number}}  -  {{$row->street_name}} - {{$row->district_name}} </p>
											  <p style="direction: rtl;margin: 0;"> {{$row->cityName->name}} </p>
											  <p style="direction: rtl;margin: 0;"> السعودية </p>
										  </td>
									  </tr>
									  </tbody>
								  </table>
								  <br>
							  </font>
						  </td>
					  </tr>
					  </tbody>
				  </table>



				  <table style="float:right;width: 100%;">
					  <tbody>
					  <tr>
						  <td>
							  <font face="Tahoma">
								  <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px;direction: rtl">
									  <thead>
									  <tr>
										  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">المنتج</td>
										  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">النوع</td>
										  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">الوزن</td>
										  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">الكمية</td>
										  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">السعر</td>
										  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">الاجمالي</td>
									  </tr>
									  </thead>
									  <tbody>
                                      @foreach($content as $co)
										  <tr>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px"> {{$co->product->name}} </td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">{{$co->product->cat->name}}</td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">@if($co->product->size) {{$co->product->fit}}    @endif  </td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">{{$co->qty}}<</td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">{{$co->price}} @lang('admin.langPrice')</td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">{{$co->qty * $co->price}} @lang('admin.langPrice')   </td>
                                          </tr>
                                    @endforeach
						

										  <tr>
											  <td></td>
											  <td></td>
											  <td></td>
											  <td></td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px;font-weight: bold;">الاجمالى : </td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px;font-weight: bold;"> {{$row->total_price}}  @lang('admin.langPrice')   </td>
										  </tr>

										  <tr>
											  <td></td>
											  <td></td>
											  <td></td>
											  <td></td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px;font-weight: bold;"> سعر الشحن : </td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px;font-weight: bold;"> {{$row->shippCampany->price}} </td>
										  </tr>

										  <tr>
											  <td></td>
											  <td></td>
											  <td></td>
											  <td></td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px;font-weight: bold;">رسوم الدفع عند الاستلام : </td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px;font-weight: bold;">{{$row->total_price + $set->additional_value + $row->shippCampany->price}}  @lang('admin.langPrice')</td>
										  </tr>

										  <tr>
											  <td></td>
											  <td></td>
											  <td></td>
											  <td></td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px;font-weight: bold;">القيمة المضافة : </td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px;font-weight: bold;">{{$set->additional_value}}  @lang('admin.langPrice') </td>
										  </tr>

										  <tr>
											  <td></td>
											  <td></td>
											  <td></td>
											  <td></td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px;font-weight: bold;background: #b6ce5b;"> الاجمالى النهائي:  </td>
											  <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px;font-weight: bold;background: #b6ce5b;">  {{$row->total_price + $set->additional_value + $row->shippCampany->price }}  @lang('admin.langPrice')   </td>
										  </tr>

									  </tbody>
								  </table>
								  <br>
							  </font>
						  </td>
					  </tr>
					  </tbody>
				  </table>
				  

				  
  

				
              </td>
          </tr>
      </tbody>
  </table>




  <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
	<script>
	
		$("#btn").click(function () {
			
    		window.print();
		});

		
	
	</script>

	@if(isset($print))

		<script>

			window.print();

		</script>
	@endif

    </body>

</html>