@extends('admin.main')

@section('content')


        
     
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <!-- <h4 class="mt-0 header-title"> @lang('admin.export') </h4> -->
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>@lang('admin.reverceNumber')</th>
                                        <th>@lang('admin.orderCode')</th>
                                        <th>@lang('admin.orderClient')</th>
                                        <th>@lang('admin.paymentMethod')</th>
                                        <th>@lang('admin.orderStatus')</th>
                                        <th>@lang('admin.orderComponent')</th>
                                        <th>@lang('admin.orderDate')</th>
                                        <th>@lang('admin.orderReverceDate')</th>
                                        <th>@lang('admin.actions')</th>
                                   
                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($orders as $ord)
                                    <tr>
                                        <td>{{$ord->id}}</td>
                                        <td>{{$ord->id * 250}}</td>
                                        <td>{{$ord->client->name}}</td>
                                        <td>{{trans('admin.'.$ord->payment_method)}}</td>
                                        <td>{{trans('admin.'.$ord->status)}} </td>
                                        <td>{{$ord->content->count()}}</td>
                                        <td>{{date('Y-m-d',strtotime($ord->created_at))}}</td>
                                        <td>{{date('Y-m-d',strtotime($ord->created_at))}}</td>
                                        <td>
                                            <a href="javascript:void(0);"    class="text-muted   add-to-delete"  data-route="{{route('admin.get.ordere.addToDelete',[$ord->id])}}" title="Delete" > <i class="mdi mdi-close font-18"></i> </a>
                                            <a href="{{route('admin.get.order.showContent',[$ord->id])}}"    class="text-muted"   title="Show" > <i class="mdi mdi-eye font-18"></i> </a>

                                        </td>
                                        
                                    </tr>
                                @endforeach
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>

            <div class="row">
                <div class="col-sm-12">
                {{$orders->appends(request()->query())->links()}}
                </div>
            </div>
  


@endsection


@section('script')


    <!-- Required datatable js -->
    <script src="{{aurl()}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="{{aurl()}}/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/jszip.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/pdfmake.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/vfs_fonts.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.html5.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.print.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="{{aurl()}}/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/responsive.bootstrap4.min.js"></script>
    <!-- Datatable init js -->
    <script src="{{aurl()}}/assets/pages/datatables.init.js"></script>




@endsection