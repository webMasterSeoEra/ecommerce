@extends('admin.main')

@section('content')


        
     
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">


                            <h3 class="alert  text-danger">
                                @lang('admin.clientName') : {{$row->client->name}}
                            </h3>
                            <hr>
                            <h3 class="alert  text-danger">
                                @lang('admin.orderCode') : {{$row->id * 250}}
                            </h3>
                            <hr>
                            <h3 class="alert  text-danger">
                                @lang('admin.orderDate') : {{date('Y-m-d',strtotime($row->created_at)) }}
                            </h3>
                            <hr>

                            <h3 class="alert  text-danger">
                                @lang('admin.status') : {{trans('admin.'.$row->status) }}
                            </h3>
                            <hr>


                            <!-- <h4 class="mt-0 header-title"> @lang('admin.export') </h4> -->
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>

                                        <th class="bg-success text-light">@lang('admin.productName')</th>
                  
                                   
                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($content as $co)
                                    <tr>
                                        <td class="bg-primary text-light">{{$co->product->name}}</td>
                                        
                                    </tr>
                                @endforeach
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>

            <div class="row">
                <div class="col-sm-12">
                {{$content->appends(request()->query())->links()}}
                </div>
            </div>
  


@endsection


@section('script')


@endsection