@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">
                <h4 class="mt-0 header-title">  @lang('admin.messagesVisitor')  </h4>
             
                <div class="table-responsive">
                    <div id="coverloadingTable">
                        <img src="{{aurl()}}/loader.gif">
                    </div>
                    <table class="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('admin.name')</th>
                                <th>@lang('admin.email')</th>
                        
                                <th>@lang('admin.actions')</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($messages as $row)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$row->name}}</td>
                                <td>{{$row->email}}</td>
                                <td class="text-center">
                                    <a href="javascript:void(0);"   data-name="{{$row->name}}" data-email="{{$row->email}}"   class="m-r-15 text-muted edit-row" data-toggle="modal" data-target=".edit-modal" data-toggle="tooltip" data-placement="top" title="" data-original-title="Show"><i class="mdi mdi-eye font-18"></i></a>
                                    <textarea style="display:none;">{!!$row->message!!}</textarea>                                    
                                    <a href="javascript:void(0);" data-route="{{route('admin.get.message.delete',[$row->id])}}" class="text-muted  delete-row " data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-close font-18"></i></a>
                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->


    @include('admin.messages.forms.show')

@endsection

@section('script')
  
    @include('admin.inc.ajax.delete')

    <script>
        $(document).on("click",".edit-row",function(){
            
            var el = $(this);
            $("#name").html(el.attr("data-name"));
            $("#email").html(el.attr("data-email"));
            $("#message").html(el.next("textarea").val());

        }); 

    </script>



@endsection