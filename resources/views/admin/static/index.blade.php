@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">
                <h4 class="mt-0 header-title">  @lang('admin.staticPages')  </h4>
                <h4 class="mt-0 header-title"> 
                    <button type="button" data-toggle="modal" data-target=".add-modal" class="btn btn-secondary btn-sm waves-effect fontLight">@lang('admin.addNew')</button> 
                </h4>
                <div class="table-responsive">
                    <div id="coverloadingTable">
                        <img src="{{aurl()}}/loader.gif">
                    </div>
                    <table class="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('admin.namePage')</th>
                                <th>@lang('admin.type')</th>
                                <th>@lang('admin.actions')</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($staticpages as $row)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$row->name}}</td>
                                <td>{{trans('admin.'.$row->type) }}</td>
                                <td class="text-center">
                                    <a href="javascript:void(0);"  data-name="{{$row->name}}" 
                                        data-id="{{$row->id}}" data-type="{{$row->type}}"  
                                        data-slug="{{$row->slug}}"  data-slug="{{$row->slug}}"
                                        data-img="{{getImg(STATIC_PATH.$row->img)}}"  
                                         class="m-r-15 text-muted edit-row" data-toggle="modal" data-target=".edit-modal" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-pencil font-18"></i></a>
                                    <textarea style="display:none;">{!!$row->desc!!}</textarea>                                                                        
                                    <a href="javascript:void(0);" data-route="{{route('admin.get.static.delete',[$row->id])}}" class="text-muted  delete-row " data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-close font-18"></i></a>
                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->


    @include('admin.static.forms.add')
    @include('admin.static.forms.edit')

@endsection

<?php $routeAdd = route('admin.post.static.store'); ?>
<?php $routeEdit = route('admin.put.static.update'); ?>
@section('script')


@include('admin.inc.ajax.delete')

<script src="{{aurl()}}/plugins/ckeditor/ckeditor.js"></script>
@include('admin.inc.ajax.store')

<script>
        CKEDITOR.replace('desc');
        CKEDITOR.replace('descEdit');
        CKEDITOR.editorConfig = function( config ) 
        {
        // Define changes to default configuration here. For example:
            config.language = 'ar';
            config.uiColor = '#AADC6E';
        };

        $(".submit-btn").click(function()
        {
            for (instance in CKEDITOR.instances) 
            {
                CKEDITOR.instances[instance].updateElement();
            }
        })

</script>


<script>
    $(document).on("click",".edit-row",function(){
        
        var el = $(this);
        $("#name").val(el.attr("data-name"));
        $("#slug").val(el.attr("data-slug"));
        $("#type").val(el.attr("data-type"));
        $("#id").val(el.attr("data-id"));
        CKEDITOR.instances['desc2'].setData(el.next("textarea").val())
        for (instance in CKEDITOR.instances) 
        {
            CKEDITOR.instances[instance].updateElement();
        }

            // alert(el.attr("data-img"));
        $("#imgContainer").html('');
        $("#imgContainer").append('<img src="'+ el.attr("data-img") +'" class="img-thumbnail" >');


    }); 

    $(".btn-edit").click(function(){

        for (instance in CKEDITOR.instances) 
        {
            CKEDITOR.instances[instance].updateElement();
        }
    });

</script>

    @include('admin.inc.ajax.update')

@endsection