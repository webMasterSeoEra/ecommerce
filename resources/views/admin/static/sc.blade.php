@extends('admin.main')

@section('content')


<!-- end col -->
<div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">


                <form id="FormEdit">
                        <div class="coverLoading"><img src="{{aurl()}}/loader.gif"></div>
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="form-group row">
                            <ul id="errorsEdit"></ul>
                        </div>
                        <input type="hidden" name="id" id="id">

  

                        <div class="row">
                                <div class="col-lg-12">
                                    <div class="card m-b-20">
                                        <div class="card-body">
                                            <h4 class="mt-0 header-title">  @lang('admin.content')  </h4>
                                            <div class="m-t-30">
                                                <div id="accordion">
                                                    <div class="card">
                                                        <div class="card-header bg-primary border-bottom-0 p-3 " id="headingOne">
                                                            <h5 class="mb-0 mt-0 font-16 font-light text-whight bg-primary">
                                                                <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="text-light">
                                                                    @lang('admin.topBar')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.topBar')
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header bg-success border-bottom-0 p-3" id="headingTwo">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                @lang('admin.homePageContent')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.home')
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header bg-primary border-bottom-0 p-3" id="headingThree">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                @lang('admin.loginPageContent')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.login')
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="card">
                                                        <div class="card-header bg-success border-bottom-0 p-3" id="headingCart">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-toggle="collapse" data-target="#collapseCart" aria-expanded="false" aria-controls="collapseCart">
                                                                @lang('admin.cartPageContent')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseCart" class="collapse" aria-labelledby="headingCart" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.cart')
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="card">
                                                        <div class="card-header bg-primary border-bottom-0 p-3" id="headingCheck">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-toggle="collapse" data-target="#collapseCheck" aria-expanded="false" aria-controls="collapseCheck">
                                                                @lang('admin.checkoutPageContent')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseCheck" class="collapse" aria-labelledby="headingCheck" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.checkout')
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="card">
                                                        <div class="card-header bg-success border-bottom-0 p-3" id="headingProfile">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-toggle="collapse" data-target="#collapseProfile" aria-expanded="false" aria-controls="collapseProfile">
                                                                @lang('admin.profilePageContent')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseProfile" class="collapse" aria-labelledby="headingProfile" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.profile')
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="card">
                                                        <div class="card-header bg-primary border-bottom-0 p-3" id="headingEditProfile">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-toggle="collapse" data-target="#collapseEditProfile" aria-expanded="false" aria-controls="collapseEditProfile">
                                                                @lang('admin.editProfilePageContent')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseEditProfile" class="collapse" aria-labelledby="headingEditProfile" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.editProfile')
                                                            </div>
                                                        </div>
                                                    </div>





                                                    <div class="card">
                                                        <div class="card-header bg-success border-bottom-0 p-3" id="headingchangePassword">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-target="#collapsechangePassword" aria-controls="collapsechangePassword"  data-toggle="collapse" aria-expanded="false" >
                                                                @lang('admin.changePasswordPageContent')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapsechangePassword" class="collapse" aria-labelledby="headingchangePassword" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.changePassword')
                                                            </div>
                                                        </div>
                                                    </div>




                                                    <div class="card">
                                                        <div class="card-header bg-primary border-bottom-0 p-3" id="headingAddress">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-target="#collapseAddress" aria-controls="collapseAddress"  data-toggle="collapse" aria-expanded="false" >
                                                                @lang('admin.addressPage')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseAddress" class="collapse" aria-labelledby="headingAddress" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.address')
                                                            </div>
                                                        </div>
                                                    </div>




                                                    <div class="card">
                                                        <div class="card-header bg-success border-bottom-0 p-3" id="headingOrder">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-target="#collapseOrder" aria-controls="collapseOrder"  data-toggle="collapse" aria-expanded="false" >
                                                                @lang('admin.ordersPage')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseOrder" class="collapse" aria-labelledby="headingOrder" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.orders')
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="card">
                                                        <div class="card-header bg-primary border-bottom-0 p-3" id="headingOrderContent">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-target="#collapseOrderContent" aria-controls="collapseOrderContent"  data-toggle="collapse" aria-expanded="false" >
                                                                @lang('admin.ordersContentPage')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseOrderContent" class="collapse" aria-labelledby="headingOrderContent" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.orderContent')
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="card">
                                                        <div class="card-header bg-success border-bottom-0 p-3" id="headingRefOrder">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-target="#collapseRefOrder" aria-controls="collapseRefOrder"  data-toggle="collapse" aria-expanded="false" >
                                                                @lang('admin.refusedOrdersPage')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseRefOrder" class="collapse" aria-labelledby="headingRefOrder" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.refOrder')
                                                            </div>
                                                        </div>
                                                    </div>




                                                    <div class="card">
                                                        <div class="card-header bg-primary border-bottom-0 p-3" id="headingAddRefuse">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-target="#collapseAddRefuse" aria-controls="collapseAddRefuse"  data-toggle="collapse" aria-expanded="false" >
                                                                @lang('admin.AddRefusePage')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseAddRefuse" class="collapse" aria-labelledby="headingAddRefuse" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.addRefuse')
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="card">
                                                        <div class="card-header bg-success border-bottom-0 p-3" id="headingFooter">
                                                            <h5 class="mb-0 mt-0 font-16 font-light">
                                                                <a href="#" class="text-light collapsed" data-target="#collapseFooter" aria-controls="collapseFooter"  data-toggle="collapse" aria-expanded="false" >
                                                                @lang('admin.footerContent')
                                                                </a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseFooter" class="collapse" aria-labelledby="headingFooter" data-parent="#accordion">
                                                            <div class="card-body text-muted">
                                                                @include('admin.static.inc.footer')
                                                            </div>
                                                        </div>
                                                    </div>




 



                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>

                        
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-success fontLight submit-btn"  type="submit"  >
                                        @lang('admin.save')
                                </button>
                            </div>
                        </div>
                </form>



                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


@endsection

<?php $routeEdit = route('admin.put.static.updateSc'); ?>
@section('script')
<script src="{{aurl()}}/plugins/ckeditor/ckeditor.js"></script>


@include('admin.inc.ajax.update')
@endsection