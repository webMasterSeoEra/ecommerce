        </div>
        <!-- end container -->
    </div>
    <!-- end wrapper -->
    <!-- Footer -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">2017 - 2019 © seoera <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by seoera</span></div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->
    <!-- jQuery  -->
    <script src="{{ aurl() }}/assets/js/jquery.min.js"></script>
    <script src="{{ aurl() }}/assets/js/bootstrap.bundle.min.js"></script>
    <script src="{{ aurl() }}/assets/js/modernizr.min.js"></script>
    <script src="{{ aurl() }}/assets/js/waves.js"></script>
    <script src="{{ aurl() }}/assets/js/jquery.slimscroll.js"></script>
    <script src="{{ aurl() }}/assets/js/jquery.nicescroll.js"></script>
    <script src="{{ aurl() }}/assets/js/jquery.scrollTo.min.js"></script>
    <!-- App js -->
    <!-- sweet alert  -->
    <script src="{{furl()}}/js/sweetalert2@8.js"></script>

    <script>
        
        $(".delete-row").click(function(){

            if (confirm('هل انت متأكد من الحذف ! ')) 
            {
                return true;
            } 
            else 
            {
                return false;
            }
        })
    
    </script>
    
    @yield('script')
    
    <script src="{{ aurl() }}/assets/js/app.js"></script>



</body>
<!-- Mirrored from themesbrand.com/admiria/layouts/horizontal-rtl/pages-blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Jul 2019 14:16:39 GMT -->

</html>