<!doctype html>
<html lang="en">
<!-- Mirrored from themesbrand.com/admiria/layouts/horizontal-rtl/pages-blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Jul 2019 14:16:39 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title> بيت الحكمة  </title>
    <meta content="Admin Dashboard" name="description">
    <meta content="Themesbrand" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- App Icons -->
    <link rel="shortcut icon" href="{{ aurl() }}/assets/images/favicon.ico">
    <!-- App css -->
    <link href="{{ aurl() }}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{{ aurl() }}/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="{{ aurl() }}/assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="{{ aurl() }}/assets/css/admin.css" rel="stylesheet" type="text/css">

    @yield('style')

</head>

<body>
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>
