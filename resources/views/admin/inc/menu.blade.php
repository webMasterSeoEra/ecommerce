

    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid">
                <!-- Logo container-->
                <div class="logo">
                    <!-- Text Logo -->
                    <!--<a href="index.html" class="logo">-->
                        <!-- Search -->
                        <!-- Fullscreen -->
                    <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item dropdown notification-list hide-phone"><a class="nav-link waves-effect" href="#" id="btn-fullscreen"><i class="mdi mdi-fullscreen noti-icon"></i></a></li>
                    
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><img src="{{ aurl() }}/assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                <!-- <a class="dropdown-item" href="#"><i class="dripicons-user text-muted"></i> Profile</a>  -->
                              
                                <a class="dropdown-item" href="{{route('admin.get.settings.index')}}"> <i class="dripicons-gear text-muted"></i> @lang('admin.settings')</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('admin.get.authadmin.logout')}}"><i class="dripicons-exit text-muted"></i> @lang('admin.logout')</a></div>
                        </li>

                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"  title="@lang('admin.managerDepartment')">  <i class="mdi mdi-account-star" style=" color: #fff;"></i> </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                                <a class="dropdown-item" href="{{route('admin.get.admin.index')}}"> <i class="dripicons-gear text-muted"></i> @lang('admin.managers')</a>
                            </div>
                                
                        </li>

                        <li class="menu-item list-inline-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines"><span></span> <span></span> <span></span></div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>
                    </ul>
                </div>


                 <!-- End Logo container-->
                 <div class="menu-extras topbar-custom">
                    <!-- Search input -->
                    <div class="search-wrap" id="search-wrap">
                        <div class="search-bar">
                            <input class="search-input" type="search" placeholder="Search"> <a href="#" class="close-search toggle-search" data-target="#search-wrap"><i class="mdi mdi-close-circle"></i></a></div>
                    </div>
                    <ul class="list-inline float-right mb-0">
                        <!-- notification-->
                        <li class="list-inline-item dropdown notification-list"><a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"><i class="ion-ios7-bell noti-icon"></i>
                         <span class="badge badge-danger noti-icon-badge">{{getNotifications()->count()}}</span></a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5> @lang('admin.notifications') ({{getNotifications()->count()}})</h5></div>

                                @foreach(getNotifications() as $not)

                                    @if($not->type == "order")
                                    <!-- item-->
                                    <a href="{{route('admin.get.order.index',['pending'])}}" class="dropdown-item notify-item ">
                                        <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                        <p class="notify-details"><b>طلب جديد</b><small class="text-muted"> قام احد العملاء بعمل طلب جديد </small></p>
                                    </a>
                                    @endif

                                    @if($not->type == "client")
                                    <!-- item-->
                                    <a href="{{route('admin.get.client.index')}}" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning"><i class="mdi mdi-account-check"></i></div>
                                        <p class="notify-details"><b> عضو جديد  </b><small class="text-muted">  تم تسجيل عضوية جديدة  </small></p>
                                    </a>

                                    @endif


                                    @if($not->type == "complaint")
                                    <!-- item-->
                                    <a href="{{route('admin.get.message.index')}}" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning"><i class="mdi mdi-account-check"></i></div>
                                        <p class="notify-details"><b>   رسالة جديدة  </b><small class="text-muted">  تم ارسال رسالة جديدة </small></p>
                                    </a>

                                    @endif


                                    @if($not->type == "cancelled")
                                    <!-- item-->
                                    <a href="{{route('admin.get.order.index',['refused'])}}" class="dropdown-item notify-item ">
                                        <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                        <p class="notify-details"><b>طلب ملغى</b><small class="text-muted"> قام احد العملاء بعمل  الغاء لطلب </small></p>
                                    </a>
                                    @endif


                                    @if($not->type == "refused")
                                    <!-- item-->
                                    <a href="{{route('admin.get.order.reverced')}}" class="dropdown-item notify-item ">
                                        <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                        <p class="notify-details"><b>طلب مرفوض</b><small class="text-muted"> قام احد العملاء بعمل  بالرفض لطلب </small></p>
                                    </a>
                                    @endif

                                    

                                @endforeach
                              
                        </li>
                    </ul>
                 </div>




                <!-- end menu-extras -->
                <div class="clearfix"></div>
            </div>
            <!-- end container -->
        </div>
        <!-- end topbar-main -->
        <!-- MENU Start -->
        <div class="navbar-custom">
            <div class="container-fluid">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">

                        <li class="has-submenu">
                            <a href="#" class="{{SA('category')}}"><i class="mdi mdi-view-dashboard"></i>
                                @lang('admin.departments')
                            </a>
                            <ul class="submenu">
                                <li><a href="{{route('admin.get.category.index')}}">@lang('admin.category')</a></li>
                                <li><a href="{{route('admin.get.subcat.index')}}">@lang('admin.subCategory')</a></li>
                            </ul>
                        </li>


                        <li class="has-submenu">
                            <a href="#" class="{{SA('slider')}}"><i class="mdi mdi-view-dashboard"></i>
                                @lang('admin.siteContent')
                            </a>
                            <ul class="submenu">
                                <li><a href="{{route('admin.get.settings.index')}}">@lang('admin.settings')</a></li>
                               
                                <li><a href="{{route('admin.get.slider.index')}}">@lang('admin.slider')</a></li>
                          
                                <li><a href="{{route('admin.get.branch.index')}}">@lang('admin.branches')</a></li>
                           
                                <li><a href="{{route('admin.get.static.index')}}">@lang('admin.static')</a></li>
                                <li><a href="{{route('admin.get.settings.cond')}}">@lang('admin.condUsed')</a></li>
                                
                                <li><a href="{{route('admin.get.static.sc')}}">@lang('admin.staticWords')</a></li>
                                <li><a href="{{route('admin.get.settings.seo')}}">@lang('admin.seoData')</a></li>
                            </ul>
                        </li>

                        <li class="has-submenu">
                            <a href="#" class="{{SA('product')}}"><i class="mdi mdi-view-dashboard"></i>
                                @lang('admin.products')
                            </a>
                            <ul class="submenu">
                                <li><a href="{{route('admin.get.size.index')}}">@lang('admin.sizes')</a></li>
                                <li><a href="{{route('admin.get.product.add')}}">@lang('admin.addProduct')</a></li>
                                <li><a href="{{route('admin.get.product.index')}}">@lang('admin.viewProducts')</a></li>
                                <li><a href="{{route('admin.get.cupon.index')}}">@lang('admin.cupons')</a></li>
                                <li><a href="{{route('admin.get.shipp.index')}}">@lang('admin.shppmentCompany')</a></li>
                                <li><a href="{{route('admin.get.shippWay.index')}}">@lang('admin.shippmentWay')</a></li>
                            </ul>
                        </li>


                        <li class="has-submenu">
                            <a href="#" class="{{SA('order')}}"><i class="mdi mdi-view-dashboard"></i>
                                @lang('admin.orders')
                            </a>
                            <ul class="submenu">
                                <li><a href="{{route('admin.get.order.index',['pending'])}}">@lang('admin.ordersPending')</a></li>
                                <li><a href="{{route('admin.get.order.index',['shipping'])}}">@lang('admin.ordersShipping')</a></li>
                                <li><a href="{{route('admin.get.order.index',['accepted'])}}">@lang('admin.ordersAccepted')</a></li>
                                <li><a href="{{route('admin.get.order.index',['refused'])}}">@lang('admin.ordersRefused')</a></li>
                                <li><a href="{{route('admin.get.order.reverced')}}">@lang('admin.ordersRverced')</a></li>
                                <li><a href="{{route('admin.get.city.index')}}">@lang('admin.cities')</a></li>
                            </ul>
                        </li>

                        <li class="has-submenu">
                            <a href="#" class="{{SA('message')}}"><i class="mdi mdi-view-dashboard"></i>
                                @lang('admin.messages')
                            </a>
                            <ul class="submenu">
                                <li><a href="{{route('admin.get.message.index')}}">@lang('admin.messagesVisitor')</a></li>
                          
                                <li><a href="{{route('admin.get.newsletter.index')}}">@lang('admin.newsletter')</a></li>
                        
                            </ul>
                        </li>
                   
                        <li><a href="{{route('admin.get.client.index')}}" ><i class="mdi mdi mdi-account-multiple"></i> @lang('admin.clients') </a></li>
                        <li><a href="{{url('/')}}" target="_blank"><i class="mdi mdi-airplane"></i>@lang('admin.visitSite')</a></li>
                    </ul>
                    <!-- End navigation menu -->
                </div>
                <!-- end #navigation -->
            </div>
            <!-- end container -->
        </div>
        <!-- end navbar-custom -->
    </header>
    <!-- End Navigation Bar-->





    <div class="wrapper">
        <div class="container-fluid">


        @include('admin.inc.breadcrumb')