
<!-- Store New Data  -->


<script type="text/javascript">

  
  $("#FormAdd").submit(function(e) 
    {
      var el = $(this); 

        e.preventDefault();
        var formData  = new FormData(jQuery('#FormAdd')[0]);


      $.ajax({
          type: "POST",
          url: "{{ $routeAdd }}",
          data:formData,
          contentType: false,
          processData: false,
          beforeSend:function()
          {
              el.find(".coverloading").css({"display":"block"});
              el.find("button").prop( "disabled", true );
          },
          success: function (data) 
          {
            el.find(".coverloading").css("display","none");
              el.find("button").prop( "disabled", false );
              console.log("asdfsadfsadf");

              el.find(".form-group input:not(.doNotRemoveData)").val('');
              el.find(".form-group textarea").val('');
              Swal.fire(
              {
                position: 'top-end',
                type: 'success',
                title: "{{trans('admin.msg.addedSuccess')}}",
                showConfirmButton: false,
                timer: 2000
              })
            //   alert("{{ trans('admin.msg.addedSuccess') }}")

              $("#errorsAdd").html('');
              $("#errorsAdd").append("<li class='alert alert-success text-center'>{{ trans('admin.msg.addedSuccess') }}</li>")
              $('.add-modal').modal('toggle');
              window.location.reload()
          }, error: function (xhr, status, error) 
          {
              
              $("#errorsAdd").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errorsAdd").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
              el.find(".coverloading").css("display","none");
              el.find("button").prop( "disabled", false );
            
          }
      });

});

</script>
