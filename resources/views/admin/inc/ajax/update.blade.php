

<script type="text/javascript">

  
$("#FormEdit").submit(function(e) 
  {
    var el = $(this); 

    
  

    e.preventDefault();
      var formData  = new FormData(jQuery('#FormEdit')[0]);


    $.ajax({
        type: "POST",
        url: "{{ $routeEdit }}",
        data:formData,
        contentType: false,
        processData: false,
        beforeSend:function()
        {
            $("FormEdit .coverLoading").css("display","block");
            el.find("button").prop( "disabled", true );
        },
        success: function (data) 
        {

            $("#formEdit .coverloading").css("display","none");
            el.find("button").prop( "disabled", false );
            $("#errorsEdit").html('');
            $("#errorsEdit").append("<li class='alert alert-success text-center'>{{trans('admin.msg.editedSuccess')}}</li>")

            Swal.fire(
            {
                position: 'top-end',
                type: 'success',
                title: "{{trans('admin.msg.editedSuccess')}}",
                showConfirmButton: false,   
                timer: 2000
            })
            $('.edit-modal').modal('toggle');
            window.location.reload()
            //  

        }, error: function(xhr, status, error) 
        {
            $("#formEdit .coverloading").css("display","none");
            el.find("button").prop( "disabled", false );

            $("#errorsEdit").html('');
            $.each(xhr.responseJSON.errors, function (key, item) 
            {
              $("#errorsEdit").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
            });
          
        }
    });

});

</script>