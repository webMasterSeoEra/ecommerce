@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">
                <h4 class="mt-0 header-title">  @lang('admin.category')  </h4>
                <h4 class="mt-0 header-title"> 
                    <button type="button" data-toggle="modal" data-target=".add-modal" class="btn btn-secondary btn-sm waves-effect fontLight">@lang('admin.addNew')</button> 
                </h4>
                <div class="table-responsive">
                    <div id="coverloadingTable">
                        <img src="{{aurl()}}/loader.gif">
                    </div>
                    <form action="{{ route('admin.post.category.sort') }}" method="post" id="sortForm">@csrf</form>
                    <table class="table table-bordered mb-0 table-checkable table-sort">
                        <thead>
                            <tr >
                                <th>#</th>
                                <th>@lang('admin.name')</th>
                                <th>@lang('admin.actions')</th>
                                <th>@lang('admin.seo') </th>
                            </tr>
                        </thead>
                        <tbody class="connected-sortable droppable-area1">

                            @foreach($categories as $row)
                            <tr class="odd gradeX draggable-item">
                                <input type="hidden" name="sort[]" multiple value="{{ $row->id }}" form="sortForm">
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$row->name}}</td>
                                <td class="text-center">
                                    <a href="javascript:void(0);"  data-id="{{$row->id}}" data-name="{{$row->name}}" data-slug="{{$row->slug}}"  class="m-r-15 text-muted edit-row" data-toggle="modal" data-target=".edit-modal" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-pencil font-18"></i></a>
                                    <a href="javascript:void(0);" data-route="{{route('admin.get.category.delete',[$row->id])}}" class="text-muted  delete-row " data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-close font-18"></i></a>

                                </td>
                                <td>
                                    <a href="{{route('admin.get.category.seo',[$row->id])}}" class="btn btn-info btn-sm">  @lang('admin.seo') <i class="fa fa-search-plus"></i> </a> 

                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>

                    <br>
                    <button  class="btn btn-success btn-sm pull-right sort" type="submit" form="sortForm" >       
                          @lang('admin.sort') 
                    </button>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
        {{$categories->appends(request()->query())->links()}}
        </div>
    </div>



    <!-- end col -->


    @include('admin.category.forms.add')
    @include('admin.category.forms.edit')

@endsection

<?php $routeAdd = route('admin.post.category.store'); ?>
<?php $routeEdit = route('admin.put.category.update'); ?>
@section('script')
    @include('admin.inc.ajax.store')
    @include('admin.inc.ajax.delete')

    <script>
        $(document).on("click",".edit-row",function(){
            
            var el = $(this);
            $(".name").val(el.attr("data-name"));
            $(".slug").val(el.attr("data-slug"));
            $(".id").val(el.attr("data-id"));

        }); 

    </script>

    @include('admin.inc.ajax.update')

    
    <script type="text/javascript" src="{{aurl()}}/plugins/sort/jquery-ui.min.js"></script>

    <script type="text/javascript" src="{{aurl()}}/plugins/sort/sortAndDataTable.js"></script>


@endsection