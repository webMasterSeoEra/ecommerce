@extends('admin.main')

@section('content')


<!-- end col -->
<div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">


                <form id="FormEdit">
                        <div class="coverLoading"><img src="{{aurl()}}/loader.gif"></div>
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="form-group row">
                            <ul id="errorsEdit"></ul>
                        </div>
                        <input type="hidden" name="id" id="id">

  

                        <div class="row">
                                <div class="col-lg-12">
                                    <div class="card m-b-20">
                                        <div class="card-body">
                                            <h4 class="mt-0 header-title">Frequently Asked Questions</h4>
                                            <div class="m-t-30">
                                                <div id="accordion">
                                                    <div class="card">
                                                        <div class="card-header bg-white border-bottom-0 p-3" id="headingOne">
                                                            <h5 class="mb-0 mt-0 font-16 font-light"><a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="text-dark">Q.1 What is Lorem Ipsum?</a></h5></div>
                                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                            <div class="card-body text-muted">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header bg-white border-bottom-0 p-3" id="headingTwo">
                                                            <h5 class="mb-0 mt-0 font-16 font-light"><a href="#" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Q.2 Is safe use Lorem Ipsum?</a></h5></div>
                                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                                            <div class="card-body text-muted">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header bg-white border-bottom-0 p-3" id="headingThree">
                                                            <h5 class="mb-0 mt-0 font-16 font-light"><a href="#" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Q.3 Why use Lorem Ipsum?</a></h5></div>
                                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                            <div class="card-body text-muted">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header bg-white border-bottom-0 p-3" id="headingFour">
                                                            <h5 class="mb-0 mt-0 font-16 font-light"><a href="#" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Q.4 When can be used?</a></h5></div>
                                                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                                            <div class="card-body text-muted">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header bg-white border-bottom-0 p-3" role="tab" id="headingFive">
                                                            <h5 class="mb-0 mt-0 font-16 font-light"><a href="#" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Q.5 How many variations exist?</a></h5></div>
                                                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                                            <div class="card-body text-muted">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header bg-white border-bottom-0 p-3" role="tab" id="headingSix">
                                                            <h5 class="mb-0 mt-0 font-16 font-light"><a href="#" class="text-dark collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">Q.6 License & Copyright</a></h5></div>
                                                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                                            <div class="card-body text-muted">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>

                        
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-success fontLight submit-btn"  type="submit"  >
                                        @lang('admin.save')
                                </button>
                            </div>
                        </div>
                </form>



                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


@endsection

<?php $routeEdit = route('admin.post.settings.updateCond'); ?>
@section('script')
<script src="{{aurl()}}/plugins/ckeditor/ckeditor.js"></script>

<script>
        CKEDITOR.replace('descEdit');
        CKEDITOR.editorConfig = function( config ) 
        {
        // Define changes to default configuration here. For example:
            config.language = 'ar';
            config.uiColor = '#AADC6E';
        };

        $(".submit-btn").click(function()
        {
            for (instance in CKEDITOR.instances) 
            {
                CKEDITOR.instances[instance].updateElement();
            }
        })

</script>

@include('admin.inc.ajax.update')
@endsection