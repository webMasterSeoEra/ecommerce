@extends('admin.main')

@section('content')


<!-- end col -->
<div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">


                <form id="FormEdit">
                        <div class="coverLoading"><img src="{{aurl()}}/loader.gif"></div>
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="form-group row">
                            <ul id="errorsEdit"></ul>
                        </div>
                        <input type="hidden" name="id" id="id">

  

                        <div class="form-group row">
                            <label for="example-search-input" class="col-sm-2 col-form-label">@lang('admin.contentPage')</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" required name="descEdit"  id="desc2">{!! $row->cond_used !!}</textarea>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-success fontLight submit-btn"  type="submit"  >
                                        @lang('admin.save')
                                </button>
                            </div>
                        </div>
                </form>



                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


@endsection

<?php $routeEdit = route('admin.post.settings.updateCond'); ?>
@section('script')
<script src="{{aurl()}}/plugins/ckeditor/ckeditor.js"></script>

<script>
        CKEDITOR.replace('descEdit');
        CKEDITOR.editorConfig = function( config ) 
        {
        // Define changes to default configuration here. For example:
            config.language = 'ar';
            config.uiColor = '#AADC6E';
        };

        $(".submit-btn").click(function()
        {
            for (instance in CKEDITOR.instances) 
            {
                CKEDITOR.instances[instance].updateElement();
            }
        })

</script>

@include('admin.inc.ajax.update')
@endsection