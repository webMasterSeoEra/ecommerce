@extends('admin.main')

@section('content')


<!-- end col -->
<div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">

                <form id="FormEdit" enctype="multipart/form-data" >
                        <div class="coverLoading"><img src="{{aurl()}}/loader.gif"></div>
                        @csrf
                        <div class="form-group row">
                            <ul id="errorsُEdit"></ul>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.name')</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="{{$row->name}}" name="name" type="search"  id="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.email')</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="{{$row->email}}" name="email" type="email"  id="">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.mobile')</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="{{$row->mobile1}}" name="mobile1" type="text"  id="">
                            </div>
                        </div>

                        <hr>
                        <hr>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.facebook')</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="{{$row->facebook}}" name="facebook" type="url"  id="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.instagram')</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="{{$row->instagram}}" name="instagram" type="url"  id="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.twitter')</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="{{$row->twitter}}" name="twitter" type="url"  id="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.youtube')</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="{{$row->youtube}}" name="youtube" type="url"  id="">
                            </div>
                        </div>

                        <hr>
                        <hr>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.fax')</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="fax" value="{{$row->fax}}" type="search"  id="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.address')</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="address1" value="{{$row->address1}}" type="search"  id="">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-search-input" class="col-sm-2 col-form-label">@lang('admin.logo')</label>
                            <div class="col-sm-10">
                                <input type="file" class="filestyle" name="logo1" data-input="false" data-buttonname="btn-secondary">
                            </div>
                            <div class="col-sm-12">
                                @if($row->logo1)
                                <hr>
                                    <img src="{{getImg(SETTINGS_PATH.$row->logo1)}}" style="width:250px; height:150px;" >
                                <hr>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.additionalValue')</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="additional_value" value="{{$row->additional_value}}" type="search"  id="">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.map')</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="map"  >{!!$row->map!!}</textarea>
                            </div>
                        </div>

                        



                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary fontLight submit-btn"  type="submit"  >
                                        @lang('admin.save')
                                </button>
                            </div>
                        </div>
                </form>
                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


@endsection

<?php $routeEdit = route('admin.post.settings.update'); ?>
@section('script')

@include('admin.inc.ajax.update')
@endsection