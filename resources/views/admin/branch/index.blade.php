@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">
                <h4 class="mt-0 header-title">  @lang('admin.branches')  </h4>
                <h4 class="mt-0 header-title"> 
                    <button type="button" data-toggle="modal" data-target=".add-modal" class="btn btn-secondary btn-sm waves-effect fontLight">@lang('admin.addNew')</button> 
                </h4>
                <div class="table-responsive">
                    <div id="coverloadingTable">
                        <img src="{{aurl()}}/loader.gif">
                    </div>
                    <table class="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('admin.name')</th>
                                <th>@lang('admin.mobile')</th>
                                <th>@lang('admin.address')</th>
                                <th>@lang('admin.actions')</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($branches as $bra)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$bra->name}}</td>
                                <td>{{$bra->mobile1}}</td>
                                <td>{{$bra->address}}</td>
                                <td class="text-center">
                                    <a href="javascript:void(0);"  data-id="{{$bra->id}}" data-name="{{$bra->name}}" data-mobile="{{$bra->mobile1}}" data-address="{{$bra->address}}"  class="m-r-15 text-muted edit-row" data-toggle="modal" data-target=".edit-modal" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-pencil font-18"></i></a>
                                    <textarea style="display:none;">{!!$bra->map!!}</textarea>                                    
                                    <a href="javascript:void(0);" data-route="{{route('admin.get.branche.delete',[$bra->id])}}" class="text-muted  delete-row " data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-close font-18"></i></a>
                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->


    @include('admin.branch.forms.add')
    @include('admin.branch.forms.edit')

@endsection

<?php $routeAdd = route('admin.post.branch.store'); ?>
<?php $routeEdit = route('admin.put.branch.update'); ?>
@section('script')
    @include('admin.inc.ajax.store')
    @include('admin.inc.ajax.delete')

    <script>
        $(document).on("click",".edit-row",function(){
            
            var el = $(this);
            $("#name").val(el.attr("data-name"));
            $("#mobile1").val(el.attr("data-mobile"));
            $("#address").val(el.attr("data-address"));
            $("#map").val(el.next("textarea").val());
            $("#id").val(el.attr("data-id"));

        }); 

    </script>

    @include('admin.inc.ajax.update')

@endsection