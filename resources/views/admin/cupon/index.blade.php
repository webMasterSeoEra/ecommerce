@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">
                <h4 class="mt-0 header-title">  @lang('admin.cupons')  </h4>
                <h4 class="mt-0 header-title"> 
                    <button type="button" data-toggle="modal" data-target=".add-modal" class="btn btn-secondary btn-sm waves-effect fontLight">@lang('admin.addNew')</button> 
                </h4>
                <div class="table-responsive">
                    <div id="coverloadingTable">
                        <img src="{{aurl()}}/loader.gif">
                    </div>
                    <table class="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('admin.cuponNumber')</th>
                                <th>@lang('admin.percent')</th>
                                <th>@lang('admin.actions')</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($cupons as $row)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$row->cupon_number}}</td>
                                <td>{{$row->percent}} % </td>
                                <td class="text-center">
                                    <a href="javascript:void(0);"  data-id="{{$row->id}}" data-percent="{{$row->percent}}" data-cnumber="{{$row->cupon_number}}"  class="m-r-15 text-muted edit-row" data-toggle="modal" data-target=".edit-modal" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="mdi mdi-pencil font-18"></i></a>
                                    <a href="javascript:void(0);" data-route="{{route('admin.get.cupon.delete',[$row->id])}}" class="text-muted  delete-row " data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="mdi mdi-close font-18"></i></a>
                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
        {{$cupons->appends(request()->query())->links()}}
        </div>
    </div>



    <!-- end col -->


    @include('admin.cupon.forms.add')
    @include('admin.cupon.forms.edit')

@endsection

<?php $routeAdd = route('admin.post.cupon.store'); ?>
<?php $routeEdit = route('admin.put.cupon.update'); ?>
@section('script')
    @include('admin.inc.ajax.store')
    @include('admin.inc.ajax.delete')

    <script>
        $(document).on("click",".edit-row",function(){
            
            var el = $(this);
            $("#cupon_number").val(el.attr("data-cnumber"));
            $("#percent").val(el.attr("data-percent"));
            $("#id").val(el.attr("data-id"));

        }); 

    </script>

    @include('admin.inc.ajax.update')

@endsection