@extends('admin.main')

@section('style')
<!--Chartist Chart CSS -->
<link rel="stylesheet" href="{{aurl()}}/plugins/chartist/css/chartist.min.css">

@endsection

@section('content')


   <!-- end page title end breadcrumb -->
   
   <div class="row">




                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-success mr-0 float-right"><i class="mdi mdi-basket"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{$pending}}</span> @lang('admin.ordersPending') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>




                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-success mr-0 float-right"><i class="mdi mdi-basket"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{ $cancelled  }}</span> @lang('admin.ordersRefused') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>
                


                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-success mr-0 float-right"><i class="mdi mdi-basket"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{$accepted}}</span> @lang('admin.ordersAccepted') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>



                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-success mr-0 float-right"><i class="mdi mdi-basket"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{$shipping}}</span> @lang('admin.ordersShipping') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>



                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-success mr-0 float-right"><i class="mdi mdi-cart"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{ $refused  }}</span> @lang('admin.ordersRverced') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>

                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-success mr-0 float-right"><i class="mdi mdi-basket"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{$ordersCount}}</span> @lang('admin.totalOrders') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>

                <div class="col-sm-12"></div>

                



                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-danger mr-0 float-right"><i class="mdi mdi-coin"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">@lang('admin.langPrice')  {{$allPrice}}  </span> @lang('admin.priceCount') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>
                
               

                
                <!--  START PRICES  (DAY - WEEK - MONTH - ALL) -->


                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-danger mr-0 float-right"><i class="mdi mdi-coin"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">@lang('admin.langPrice')  {{$priceToday}}  </span> @lang('admin.PriceToday') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>




                




                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-danger mr-0 float-right"><i class="mdi mdi-coin"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">@lang('admin.langPrice')  {{$priceWeek}}  </span> @lang('admin.PriceWeek') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>



                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-danger mr-0 float-right"><i class="mdi mdi-coin"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">@lang('admin.langPrice')  {{$priceMonth}}  </span> @lang('admin.PriceMonth') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>


                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-primary mr-0 float-right"><i class="mdi mdi-cart"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{$products}}</span> @lang('admin.products') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>


                  

                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-purple mr-0 float-right"><i class=" mdi mdi-delta"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{ $topsaleProduct->name  }}</span> @lang('admin.bestOrder') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>

                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-success mr-0 float-right"><i class="mdi mdi-eye"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{ $topViewProduct->name  }}</span> @lang('admin.bestView') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>

                <div class="col-sm-12"></div>




                

                <!--  END PRICES  -->

                <!--  START PRODUCTS  AND VIEWS -->

                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-danger mr-0 float-right"><i class="fa fa-users"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{$clients}}</span> @lang('admin.clients') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>

                <div class="col-md-6 col-xl-3">
                    <div class="mini-stat clearfix bg-white"><span class="mini-stat-icon bg-success mr-0 float-right"><i class="mdi mdi-message"></i></span>
                        <div class="mini-stat-info text-left"><span class="counter text-purple">{{ $messages  }}</span> @lang('admin.messages') </div>
                        <div class="clearfix"></div>
                        <!-- <p class="mb-0 m-t-20 text-muted text-left">Total income: $22506 </p> -->
                    </div>
                </div>



                


               



                 <!--  END SECTION -->



                



       
    </div>




    <div class="row">
        <div class="col-sm-12">
            <!-- end col -->
           
            <div style="width:100%;">
                <canvas id="canvas"></canvas>
            </div>
         
            <!-- end col -->
        </div>
    </div>


    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 m-b-30 header-title"> @lang('admin.latestOrders') </h4>
                    <div class="table-responsive">
                        <table class="table table-vertical mb-0">
                            <tbody>
                                @foreach($orders as $ord)
                                <tr>
                                    <td># {{ $ord->id * 250 }} </td>
                                    <td><span class="badge badge-pill badge-success">{{trans('admin.'.$ord->status)}}</span></td>
                                    <td>{{$ord->content->count()}}</td>
                                    <td>{{date('Y-m-d',strtotime($ord->created_at))}}</td>
                                    <td>
                                        <a href="{{route('admin.get.order.showContent',[$ord->id])}}"    class="text-muted"   title="Show" > <i class="fa fa-eye fa-2x"></i> </a>
                                    </td>
                                </tr>
                                @endforeach
                          
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')

 <!--Chartist Chart-->
<script src="{{aurl()}}/js-statistics/Chart.min.js"></script>
<script src="{{aurl()}}/js-statistics/utils.js"></script>
<script>
		// var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        
        // monthes on this year 
        var MONTHS = [
            @foreach($pricesPerMonth as $row)
                "{{$row->months}}",                        
            @endforeach
        ];
        <?php 

        // get max number of selling
            function getMax( $array )
            {
                $max = 0;
                foreach( $array as $k => $v )
                {
                    $max = max( array( $max, $v['sums'] ) );
                }
                return $max;
            } 
            $maxTotalPrice = getMax( $pricesPerMonth );
                    
        ?>
		var randomScalingFactor = function() {
			return {{$maxTotalPrice}}
		};

		var config = {
			type: 'line',
			data: {
				labels: MONTHS,
				datasets: [{
					label: "@lang('admin.PriceMonth')",
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.green,
					data: [
                        @foreach($pricesPerMonth as $row)
                            "{{$row->sums}}",                         
                        @endforeach
					],
					fill: false,
				},]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: "@lang('admin.staticYearSelling')"
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						},
						ticks: {
							min: 0,
							max: {{$maxTotalPrice + 50}} ,

							// forces step size to be 5 units
							stepSize:50
						}
					}]
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);
		};


	</script>

@endsection 