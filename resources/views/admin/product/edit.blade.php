
@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">

                <form id="FormEdit" enctype="multipart/form-data" >
                        <div class="coverLoading"><img src="{{aurl()}}/loader.gif"></div>
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="form-group row">
                            <ul id="errorsEdit"></ul>
                        </div>
                        <input type="hidden" name="id" class="id" value="{{$row->id}}">
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.name')</label>
                            <div class="col-sm-10">
                                <input class="form-control" required value="{{$row->name}}" name="name" type="search"  id="">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">@lang('admin.baseCategory')</label>
                            <div class="col-sm-10">
                                <select class="custom-select" required name="category_id" id="cat" >
                                    <option value="">@lang('admin.chooseCategory')</option>
                                    @foreach($categories as $cat)
                                    <option value="{{$cat->id}}"  {{typeSelect($cat->id,$row->category_id)}} > {{$cat->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">@lang('admin.baseSubCategory')</label>
                            <div class="col-sm-10">
                                <select class="custom-select"  name="sub_cat_id" id="sub">
                                    <option value="">@lang('admin.chooseSubCategory')</option>
                                    @foreach($subcats as $sub)
                                    <option value="{{$sub->id}}"  {{typeSelect($sub->id,$row->sub_cat_id)}} > {{$cat->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">@lang('admin.size')</label>
                            <div class="col-sm-10">
                                <select class="custom-select" name="size_id"   >
                                    <option value="">@lang('admin.chooseSize')</option>
                                    @foreach($sizes as $size)
                                    <option value="{{$size->id}}" {{typeSelect($size->id,$row->size_id)}}> {{$size->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.productFit')</label>
                            <div class="col-sm-10">
                                <input class="form-control"  name="fit" type="text"  value="{{$row->fit}}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.price')</label>
                            <div class="col-sm-10">
                                <input class="form-control" required value="{{$row->price}}" name="price" type="search"  id="">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-search-input" class="col-sm-2 col-form-label">@lang('admin.img1') (310 * 310)</label>
                            <div class="col-sm-10">
                            <input type="file" class="filestyle"  name="img"  data-buttonname="btn-secondary">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-search-input" class="col-sm-2 col-form-label">@lang('admin.addMoreImages') (610 * 410)</label>
                            <div class="col-sm-10">
                                <a class="btn btn-primary repeat-element" ><i class="fa fa-plus fa-1x"></i></a>
                            </div>
                        </div>




                        <div class="form-group row">
                            <label for="example-search-input" class="col-sm-2 col-form-label">@lang('admin.img2')</label>
                            <div class="col-sm-10">
                            <input type="file" class="filestyle" id="images"  name="image[]"  data-buttonname="btn-secondary">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.desc')</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" required  name="desc" id="desc"  >{!!$row->desc!!}</textarea>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">@lang('admin.specialProduct')</label>
                            <div class="col-sm-10">
                                <select class="custom-select" name="special">
                                    
                                    <option value="no" @if($row->special=="no") selected @endif> @lang('admin.no') </option>
                                    <option value="yes" @if($row->special=="yes") selected @endif > @lang('admin.yes') </option>
                                 
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.productStock')</label>
                            <div class="col-sm-10">
                                <input class="form-control" required value="{{$row->stock}}" name="stock" type="number"  id="">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.tags')</label>
                            <div class="col-sm-10">
                                <input class="form-control"  required value="{{$row->tags}}" name="tags" type="search"  >
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.slug')</label>
                            <div class="col-sm-10">
                                <input class="form-control" value="{{$row->slug}}" name="slug" type="search"  >
                            </div>
                        </div>

                        



                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary fontLight submit-btn"  type="submit"  >
                                        @lang('admin.save')
                                </button>
                            </div>
                        </div>
                </form>



                
                
            </div>

                <div>
                    <div style="width:48%; display:block; margin:auto;">
                        <img src="{{getImg(PRODUCT_PATH.$row->img)}}" class="img-thumbnail" style="width:250px;display:block; margin:auto; height:300px;"  >
                    </div>
                </div>
                <hr>
                <div class="row">
                @foreach($row->images as $img)
                <div class="col-md-6 col-lg-3 cont-item">
                    <div class="product-list-box">
                        <a href="javascript:void(0);">
                            <img src="{{getImg(PRODUCT_PATH.$img->name)}}" style="height:250px; width:100%;" class="img-fluid" alt="work-thumbnail">
                        </a>
                        <div class="detail text-left">
                            <a href="" data-route="{{route('admin.get.product.deleteImage',[$img->id])}}" class="btn btn-danger btn-sm delete-row">
                                @lang('admin.delete') <i class="fa fa-close"></i> 
                            </a>
                            <!-- <a href="" class="btn btn-info btn-sm">  @lang('admin.images') <i class="fa fa-camera"></i> </a>  -->
                        </div>
                    </div>
                </div>
                @endforeach
                </div>
                


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->








@endsection


@section('script')



<script src="{{aurl()}}/plugins/ckeditor/ckeditor.js"></script>
<script src="{{aurl()}}/assets/js/jquery.repeat.js"></script>

<script>

    $(".repeat-element").click(function(){

        $('#images').repeat(1);
    })

</script>

<script>
        CKEDITOR.replace('desc');
        CKEDITOR.editorConfig = function( config ) 
        {
        // Define changes to default configuration here. For example:
            config.language = 'ar';
            config.uiColor = '#AADC6E';
        };

        $(".submit-btn").click(function()
        {
            for (instance in CKEDITOR.instances) 
            {
                CKEDITOR.instances[instance].updateElement();
            }
        })

</script>

@include('admin.inc.ajax.getSub')
<?php $routeEdit = route('admin.put.product.update'); ?>
@include('admin.inc.ajax.update')
@include('admin.inc.ajax.deleteProduct')







@endsection