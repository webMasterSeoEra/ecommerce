@extends('admin.main')

@section('content')

 
<h4 class="mt-0 header-title">  @lang('admin.products')  </h4>
    <!-- end col -->
          <!-- end row -->
        <div class="row">
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">
                        <form action="{{ route('admin.post.product.sort') }}" method="post" id="sortForm">@csrf</form>
                            <!-- <h4 class="mt-0 header-title"> @lang('admin.export') </h4> -->
                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap table-checkable table-sort" cellspacing="0" width="100%">
                            <div id="coverloadingTable">
                                <img src="{{aurl()}}/loader.gif">
                            </div>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('admin.img')</th>
                                <th>@lang('admin.name')</th>
                                <th>@lang('admin.price')</th>
                                <th>@lang('admin.category')</th>
                                <th>@lang('admin.actions')</th>
                            </tr>
                        </thead>
                        <tbody class="connected-sortable droppable-area1">

                            @foreach($products as $row)
                            <tr class="odd gradeX draggable-item">
                                <input type="hidden" name="sort[]" multiple value="{{ $row->id }}" form="sortForm">
                                <th scope="row">{{$loop->iteration}}</th>
                                <td><img src="{{getImg(PRODUCT_PATH.$row->img)}}" style="height:50px; width:100px;" class="img-fluid" alt="work-thumbnail"</td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->price}}</td>
                                <td>{{$row->cat->name}}</td>
                           
                                <td class="text-center">
                                    <a href="{{route('admin.get.product.edit',[$row->id])}}" class="btn btn-success btn-sm"> 
                                            @lang('admin.edit') <i class="fa fa-pencil"></i>
                                    </a> 
                                    <a href="" data-route="{{route('admin.get.product.delete',[$row->id])}}" class="btn btn-secondary btn-sm delete-row">
                                        @lang('admin.delete') <i class="fa fa-close"></i> 
                                    </a>
                                    <!-- <a href="" class="btn btn-info btn-sm">  @lang('admin.images') <i class="fa fa-camera"></i> </a>  -->
                                    <a href="{{route('admin.get.product.seo',[$row->id])}}" class="btn btn-info btn-sm">  @lang('admin.seo') <i class="fa fa-search-plus"></i> </a> 
                        
                                </td>
                            </tr>
                            @endforeach



                        </tbody>
                    </table>
                    <br>
                    <button  class="btn btn-success btn-sm pull-right sort" type="submit" form="sortForm" >       
                          @lang('admin.sort') 
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
        {{$products->appends(request()->query())->links()}}
        </div>
    </div>


@endsection

@section('script')


    <!-- Required datatable js -->
    <script src="{{aurl()}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="{{aurl()}}/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/jszip.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/pdfmake.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/vfs_fonts.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.html5.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.print.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="{{aurl()}}/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="{{aurl()}}/plugins/datatables/responsive.bootstrap4.min.js"></script>
    <!-- Datatable init js -->
    <script src="{{aurl()}}/assets/pages/datatables.init.js"></script>
    <script type="text/javascript" src="{{aurl()}}/plugins/sort/jquery-ui.min.js"></script>

    <script type="text/javascript" src="{{aurl()}}/plugins/sort/sortAndDataTable.js"></script>


    
    @include('admin.inc.ajax.delete')


@endsection