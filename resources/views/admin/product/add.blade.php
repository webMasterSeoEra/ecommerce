
@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">

                <form id="FormAdd" enctype="multipart/form-data" >
                        <div class="coverLoading"><img src="{{aurl()}}/loader.gif"></div>
                        @csrf
                        <div class="form-group row">
                            <ul id="errorsAdd"></ul>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.name')</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="name" type="search" required >
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">@lang('admin.baseCategory')</label>
                            <div class="col-sm-10">
                                <select class="custom-select" name="category_id" id="cat"  required >
                                    <option value="">@lang('admin.chooseCategory')</option>
                                    @foreach($categories as $cat)
                                    <option value="{{$cat->id}}"> {{$cat->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">@lang('admin.baseSubCategory')</label>
                            <div class="col-sm-10">
                                <select class="custom-select" name="sub_cat_id" id="sub">
                                    <option value="">@lang('admin.chooseSubCategory')</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">@lang('admin.size')</label>
                            <div class="col-sm-10">
                                <select class="custom-select" name="size_id" id="cat"  >
                                    <option value="">@lang('admin.chooseSize')</option>
                                    @foreach($sizes as $size)
                                    <option value="{{$size->id}}"> {{$size->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.productFit')</label>
                            <div class="col-sm-10">
                                <input class="form-control"  name="fit" type="text"  >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label ">@lang('admin.price')</label>
                            <div class="col-sm-10">
                                <input class="form-control" required name="price" type="search"  id="">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-search-input" class="col-sm-2 col-form-label">@lang('admin.img1') (310 * 310) </label>
                            <div class="col-sm-10">
                            <input type="file" class="filestyle" required name="img" data-input="false" data-buttonname="btn-secondary">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-search-input" class="col-sm-2 col-form-label">@lang('admin.addMoreImages') (610 * 410)</label>
                            <div class="col-sm-10">
                                <button class="btn btn-primary repeat-element"><i class="fa fa-plus fa-1x"></i></button>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="example-search-input" class="col-sm-2 col-form-label">@lang('admin.img2')</label>
                            <div class="col-sm-10">
                            <input type="file" class="filestyle" id="images"  name="image[]" data-input="false" data-buttonname="btn-secondary">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.desc')</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" required name="desc"  ></textarea>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">@lang('admin.specialProduct')</label>
                            <div class="col-sm-10">
                                <select class="custom-select" name="special" required>
                                    
                                    <option value="no"> @lang('admin.no') </option>
                                    <option value="yes"> @lang('admin.yes') </option>
                                 
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.productStock')</label>
                            <div class="col-sm-10">
                                <input class="form-control" required name="stock" type="number"  id="">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.tags')</label>
                            <div class="col-sm-10">
                                <input class="form-control" required name="tags" type="search"  >
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">@lang('admin.slug')</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="slug" type="search"  >
                            </div>
                        </div>

                        



                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary fontLight submit-btn"  type="submit"  >
                                        @lang('admin.addNew')
                                </button>
                            </div>
                        </div>
                </form>
                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->





@endsection


@section('script')


<script src="{{aurl()}}/assets/js/jquery.repeat.js"></script>

<script>

    $(".repeat-element").click(function(){

        $('#images').repeat(1);
    })

</script>
<script src="{{aurl()}}/plugins/ckeditor/ckeditor.js"></script>

<script>
        CKEDITOR.replace('desc');
        CKEDITOR.editorConfig = function( config ) 
        {
        // Define changes to default configuration here. For example:
            config.language = 'ar';
            config.uiColor = '#AADC6E';
        };

        $(".submit-btn").click(function()
        {
            for (instance in CKEDITOR.instances) 
            {
                CKEDITOR.instances[instance].updateElement();
            }
        })

</script>

@include('admin.inc.ajax.getSub')


<?php $routeAdd = route('admin.post.product.store'); ?>
@include('admin.inc.ajax.store')


@endsection