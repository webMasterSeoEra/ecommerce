@extends('admin.main')

@section('content')

 
    <!-- end col -->
    <div class="col-lg-12">
        <div class="card m-b-20">
            <div class="card-body">
                <h4 class="mt-0 header-title">  @lang('admin.category')  </h4>
                <h4 class="mt-0 header-title"> 
                    <a href="{{route('admin.get.product.add')}}" class="btn btn-secondary btn-sm waves-effect fontLight">
                        @lang('admin.addNew')
                    </a> 
                </h4>








                <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">

                                @foreach($products as $row)
                                <div class="col-md-6 col-lg-3 cont-item">
                                    <div class="product-list-box">
                                        <a href="javascript:void(0);">
                                            <img src="{{getImg(PRODUCT_PATH.$row->img)}}" style="height:250px; width:100%;" class="img-fluid" alt="work-thumbnail">
                                        </a>
                                        <div class="detail">
                                            <h4 class="font-16">
                                                <a href="#" class="text-dark">{{$row->name}}</a>
                                            </h4>
                                            <a href="{{route('admin.get.product.edit',[$row->id])}}" class="btn btn-success btn-sm"> 
                                                 @lang('admin.edit') <i class="fa fa-pencil"></i>
                                            </a> 
                                            <a href="" data-route="{{route('admin.get.product.delete',[$row->id])}}" class="btn btn-secondary btn-sm delete-row">
                                                @lang('admin.delete') <i class="fa fa-close"></i> 
                                            </a>
                                            <!-- <a href="" class="btn btn-info btn-sm">  @lang('admin.images') <i class="fa fa-camera"></i> </a>  -->
                                            <a href="{{route('admin.get.product.seo',[$row->id])}}" class="btn btn-info btn-sm">  @lang('admin.seo') <i class="fa fa-search-plus"></i> </a> 

                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            
                            </div>

                            <div class="row">
                                {{$products->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>













            </div>
        </div>
    </div>
    <!-- end col -->


@endsection


@section('script')

    @include('admin.inc.ajax.deleteProduct')

@endsection