<?php 

return [


        // Top Bar 
        'home'                         => 'الرئيسية ',
        'contactUs'                    => ' تواصل معنا  ',
        'whoUs'                        => '  من نحن ',
        'menu'                         => '   القائمة ',
        'register'                     => '   تسجيل جديد ',
        'login'                        => '   دخول ',
        'myaccount'                    => '   حسابي ',
        'myorders'                     => '   طلباتى ',
        'myfavorits'                   => '   المفضلة ',
        'logout'                       => '   خروج ',
        'prodInCart'                   => '    منتجات داخل السلة  ',
        'welcome'                      => '    مرحبا ',
        'showCart'                     => '    عرض السلة ',
        'checkout'                     => '   انهاء الشراء ',
        'cart'                         => '   سلة الشراء  ',
        
        
        
        
        // index page 
        'addToCart'                    => '   أضف الى السلة  ',
        'policy'                       => '     شروط الاستخدام  ',

        
        // products page
        
        'choose'                         => '    اختر ....  ',
        'moreOrder'                         => '    الاكثر طلباً   ',
        'priceLess'                         => '      الاقل سعرا ',
        'moreViews'                         => '    الاكثر ظهوراً    ',
        
        
        
        
        // login page  
        'clientname'                        => 'الاسم ',
        'email'                             => ' البريد الالكتروني',
        'password'                          => ' كلمة المرور ',
        'confirmPassword'                   => '  تأكيد كلمة المرور ',
        
        
        
        
        
        
        // cart page 
        'total'                         => '  الاجمالي ',
        'additionalValue'               => '    القيمة المضافة ',
        'subTotal'                      => '    الاجمالي النهائي ',
        'CompleetBuy'                   => '   اتمام عملية الشراء',
        'continuBuy'                    => '      متابعة الشراء  ',



        // profile 

        'pending'                       => 'تم الطلب',
        'shipping'                      => 'تم الشحن',
        'accepted'                      => 'تم الاستلام',
        'refused'                       => 'تم الغاء الطلب',
        'searchNotFound'                => '  لا يوجد ماهو مطابق لبحثك ',








        // footer
        'acount'                      => '   الحساب الشخصى ',
        'myAccount'                    => '   حسابى   ',
        'myOrders'                    => '  طلباتى   ',
        'myFavorit'                   => '   قائمة رغباتى  ',
        'customerService'             => '   خدمة العملاء  ',
        'customerInfo'                => '    معلومات  ',
        'branches'                    => '    الفروع  ',














        'productsSpecial'              => '   منتجات خاصة  ',
        'namePrice'                    => '  ريال   ',
        'productErrorRespond'          => '  استلمت منتج بالخطأ  ',
        'productExpired'               => '  المنتج وصلني تالف  ',
        'ProductError'                 => '  منتج خاطئ  ',
        'productErrorExist'            => '  يوجد خلل، الرجاء التوضيح  ',
        'productErrorOther'            => '     أخرى، الرجاء التوضيح  ',
        'prductStatusError'            => '       سبب ارجاع الطلب  ',
        'detailsReverceOrder'          => '          تفاصيل الارجاع   ',






        'msg' => [

                'sucssesAddCart'        => " تم الاضافة الى السلة بنجاح ",
                'newsletterSuccess'     => "  تم اشتراكك فى القائمة البريدية بنجاح ",
                'register_success'      =>  "   تم الاشتراك بنجاح , قم بتفعيل حسابك من خلال البريد الالكترونى الذى ادخلته  ",
                'notCorrect'            => " البيانات التى ادخلتها غير صحيحة ",
                'cartSuccess'           => " تم اضافة المنتج الى السلة بنجاح ",
                'orderSuccess'          => "  تم تسجيل طلبك بنجاح  ",
                'messageSuccess'        => "  تم ارسال رسالتك لنجاح   ",
                'wishlistSuccess'       => "     تم اضافة المنتج الى المفضلة   ",
                'haveBeenAdded'         => "    تمت الاضافة  مسبقا  ",
                'notCorrectEmail'       => "   حدذ خطأ اثناء ارسال التفعيل للبريد الالكترونى ",
                'notFoundData'          => "  لا توجد بيانات ! ",
                'checkYourMail'         => "  تم ارسال كود التفعيل الى بريدك الالكترونى ",
                'passwordChanged'       => "  تم تغيير كلمة المرور بنجاح , يمكنك تسجيل الدخول الان ",
                'reverceOrderSuccess'   => "  تم ارسال الرسالة بنجاح  ",
                'rateSuccess'           => "  تم   تقييم المنتج بنجاح   ",
                'stockFinish'           => " المنتج غير متوفر   ",
                'orderReverced'         => "   الطلبات الملغية    ",
                'orderRefused'          => "   الطلبات المرفوضة    ",

        ],




];