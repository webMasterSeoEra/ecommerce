<?php 

return [


        // menu 
        'home'                          => 'الرئيسية ',
        'category'                      => '  الاقسام الرئيسية ',
        'subCategory'                   => '  الاقسام الفرعية ',
        'departments'                   => '  الاقسام  ',
        'subcats'                       => '    الاقسام الفرعية  ',
        'slider'                        => '   عارض الصور  ',
        'siteContent'                   => '   محتوى الموقع  ',
        'static'                        => '    صفحات الموقع  ',
        'branches'                      => '    الفروع  ',
        'branche'                       => '    الفرع  ',
        'staticWords'                   => '    كلمات الموقع  ',
        'orders'                        => '    الطبات    ',
        'ordersPending'                 => '    الطبات الحديثة   ',
        'ordersShipping'                => '     الطلبات المشحونة  ',
        'ordersAccepted'                => '     الطلبات المقبولة  ',
        'ordersRefused'                 => '    الطلبات الملغية   ',
        'export'                        => '     تصدير   ',
        'settings'                      => '     الاعدادات   ',
        'clients'                       => '     العملاء   ',
        'ordersRverced'                 => '    طلبات الالغاء من العملاء   ',
        'orderReverceDate'              => '      وقت طلب الالغاء    ',
        'reverceNumber'                 => '        رقم الطلب    ',
        'cupons'                        => '        الكوبونات     ',
        'condUsed'                      => '        شروط الاستخدام     ',
        'logout'                        => '         خروج     ',
        'visitSite'                     => '         زيارة الموقع     ',
        'shppmentCompany'               => '          شركات الشحن     ',
        'shippmentWay'                  => '           طرق الشحن     ',
        'notifications'                 => '             الاشعارات الجديدة     ',
        'managers'                      => ' المديرين ',
        'managerDepartment'             => ' ادارة الموقع ',
      







        // form 
        'name'                          => '  الاسم   ',
        'actions'                       => '  كنترول   ',
        'addNew'                        => '  اضافة جديد   ',
        'editData'                      => '   تعديل البيانات    ',
        'slug'                          => ' رابط صداقة محركات البحث   ',
        'save'                          => '    حفظ   ',
        'chooseCategory'                => '    اختر القسم الرئيسى   ',
        'link'                          => '      الرابط   ',
        'links'                         => '      الروابط   ',
        'small_desc'                    => '      وصف بسيط معبر   ',
        'img'                           => '        الصورة   ',
        'imgs'                          => '        الصور   ',
        'img1'                          => '        الصورة الاولى   ',
        'img2'                          => '        الصورة الثانية   ',
        'map'                           => '       الخريطة من جوجل ماب   ',
        'message'                       => '        الرسالة      ',
        'messages'                      => '       الرسائل       ',
        'messagesVisitor'               => '       رسائل الزائرين      ',
        'newsletter'                    => '          القائمة البريدية   ',
        'email'                         => '           البريد الالكترونى   ',
        'namePage'                      => '   اسم الصفحة   ',
        'namePages'                     => '    الصفحات الثابتة   ',
        'type'                          => '     النوع   ',
        'info'                          => '     معلومات   ',
        'service'                       => '     خدمة   ',
        'staticPages'                   => '     الصفحات الثابتة   ',
        'size'                          => '      الحجم او الوزن    ',
        'sizes'                         => '        الاحجام او الاوزان    ',
        'products'                      => '         المنتجات     ',
        'product'                       => '          المنتج    ',
        'desc'                          => '          الوصف    ',
        'addProduct'                    => '          اضافة منتج    ',
        'editProduct'                    => '           تعديل بيانات المنتج    ',
        'viewProducts'                  => '          عرض المنتجات    ',
        'baseCategory'                  => '   القسم الرئيسى ',
        'baseSubCategory'               => '   القسم الفرعى ',
        'chooseSubCategory'             => '    اختر القسم الفرعى ',
        'price'                         => '      السعر ',
        'special'                       => '     مميز ',
        'specialProduct'                => '     منتج مميز ',
        'yes'                           => '      نعم ',
        'no'                            => '      لا ',
        'productStock'                  => '      المخزون  ',
        'tags'                          => '      الكلمات الدلالية للبحث  ',
        'edit'                          => '  تعديل ',
        'delete'                        => '  حذف ',
        'images'                        => '  الصور ',
        'facebook'                      => '  رابط الفيسبوك ',
        'instagram'                     => '  رابط انستجرام ',
        'twitter'                       => '  رابط تويتر ',
        'youtube'                       => '  رابط يوتيوب ',
        'fax'                           => '   الفاكس ',
        'logo'                          => '  اللوجو ',
        'cuponNumber'                   => '  رقم الكوبون ',
        'percent'                       => '  نسبة الخصم ',
        'chooseSize'                    => '  اختر الحجم او الوزن ',
        'additionalValue'               => '    القيمة المضافة  ',
        'content'                       => '     المحتوى  ',
        'contentPage'                   => '     محتوى الصفحة  ',
        'productFit'                    => '     العبوة   ',
        'addMoreImages'                 => '     اضافة المزيد من الصور    ',
        'minNumberDiscount'             => '    اقل رقم للخصم   ',
        'discountValue'                 => '   قيمة الخصم بالريال    ',
        'textDiscount'                  => '    نص رسالة الخصم   ',
        'cities'                        => '      المدن   ',







        //  content like table and pages

        'mobile'                                => '        رقم الجوال   ',
        'mobile1'                               => '        رقم الجوال الرئيسى   ',
        'address'                               => '        العنوان   ',
        'orderCode'                             => '        كود الطلب   ',
        'orderClient'                           => '        العميل    ',
        'orderAddress'                          => '        عنوان الطلب    ',
        'paymentMethod'                         => '        طريقة الدفع   ',
        'orderStatus'                           => '        حالة الطلب   ',
        'cash'                                  => '       كاش   ',
        'online'                                => '         دفع بالبطاقة   ',
        'refused'                               => '       تم الرفض   ',
        'pending'                               => '       مطلوب حديثا   ',
        'shipping'                              => '       تم الشحن   ',
        'accepted'                              => '       تم القبول   ',
        'orderDate'                             => '       تاريخ الطلب   ',
        'accepted'                              => '       تم القبول   ',
        'orderComponent'                        => '       عدد منتجات الطلب   ',
        'productName'                           => '         اسم المنتج   ',
        'orderCode'                             => '          كود الطلب    ',
        'orderDate'                             => '          تاريخ الطلب   ',
        'clientName'                            => '          اسم العميل   ',
        'orderContent'                          => '           محتوى الطلب   ',
        'notFoundData'                          => '            لا توجد بيانات   ',
        'status'                                => '            الحالة     ',
        'clientOrder'                           => '            طلبات العميل     ',
        'cupon'                                 => '            كوبون     ',
        'latestOrders'                          => '           احدث الطلبات     ',
        'totalOrders'                           => '            مجموع الطلبات     ',
        'productErrorRespond'                   => '  استلمت منتج بالخطأ  ',
        'productExpired'                        => '  المنتج وصلني تالف  ',
        'ProductError'                          => '  منتج خاطئ  ',
        'productErrorExist'                     => '  يوجد خلل، الرجاء التوضيح  ',
        'productErrorOther'                     => '     أخرى، الرجاء التوضيح  ',
        'prductStatusError'                     => '       سبب ارجاع الطلب  ',
        'detailsReverceOrder'                   => '          تفاصيل الارجاع   ',
        'unknown'                               => '          غير معروف    ',
        'orderReverced'                         => '           طلبات الالغاء    ',
        'topBar'                                => '           القسم العلوى من الموقع    ',
        'homePageContent'                       => '              محتوى الصفحة الرئيسية    ',
        'loginPageContent'                      => '              محتوى  صفحة التسجيل       ',
        'cartPageContent'                       => '              محتوى  صفحة سلة الشراء       ',
        'checkoutPageContent'                   => '               محتوى  صفحة  اتمام عملية الشراء         ',
        'profilePageContent'                    => '   الصفحة الشخصية  ',
        'editProfilePageContent'                => '   تعديل البيانات الشخصية  ',
        'changePasswordPageContent'             => '  تغيير كلمة المرور  ',
        'ordersPage'                            => '   محتوى صفحة طلبات العميل   ',
        'addressPage'                           => '   محتوى صفحة العناوين   ',
        'ordersContentPage'                     => '   محتوى صفحة مكونات الطلب   ',
        'refusedOrdersPage'                     => '   محتوى صفحة الطلبات الملغاة   ',
        'AddRefusePage'                         => '   صفحة الغاء الطلب    ',
        'footerContent'                         => '     محتوى الفووتر    ',
        'seo'                                   => '       SEO    ',
        'seoProduct'                            => '    SEO للعنصر     ',
        'seoData'                               => '     بيانات ال SEO      ',
        'seoHeader'                             => '    كود ال SEO للهيدر      ',
        'seoFooter'                             => '    كود ال SEO  للفووتر      ',
        'orderPrice'                            => '     السعر الكلى للطلب         ',
        'langPrice'                             => ' ريال ',
        'orderCity'                             => ' المدينة  ',
        'shippCampany'                          => ' شركة الشحن ',
        'shippWay'                              => ' طريقة الشحن ',
        'admins'                                => '  المديرين ',
        'password'                              => '  كلمة المرور ',
        'priceCount'                            => '   المبلغ الاجمالى للمبيعات ',
        'bestSelling'                           => '    المنتج الاكثر مبيعا ',
        'bestOrder'                             => '    المنتج الاكثر طلبا ',
        'bestView'                              => '    المنتج الاكثر مشاهدة ',
        'PriceToday'                            => '       اجمالى مبيعات اليوم ',
        'PriceWeek'                             => '   اجمالى مبيعات الاسبوع  ',
        'PriceMonth'                            => '    اجمالى مبيعات الشهر  ',
        'staticYearSelling'                     => ' احصائية المبيعات السنوية  ',
        'sort'                                  => ' ترتيب ',
        'print'                                 => '  طباعة ',
        'changeStatus'                          => '  تغيير الحالة ',















        'msg' => [

                'notCorrect'            => " البيانات غير صحيحة ",
                'addedSuccess'          => "  تمت الاضافة بنجاح ",
                'deletedSuccess'        => "  تم الحذف بنجاح   ",
                'editedSuccess'         => "   تم تعديل البيانات بنجاح   ",
                'orderShipping'         => "  تم اضافة المنتج الى  طلبات الشحن   ",
                'orderAccepted'         => "      تمت الاضافة الى الطلبات المقبولة   ",
                'orderRefused'          => "       تمت الاضافة الى الطلبات المرفوضة   ",

        ],

];