<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



//  
Route::namespace('Front')->name('front.')->group(function()
{

    
    Route::group(['middleware'=>'check:client','namespace'=>'Client'],function()
    {
        Route::get('/دخول','AuthClientController@login')->name('get.authClient.login');
        Route::post('/login','AuthClientController@do_login')->name('get.authClient.do_login');
        Route::get('/تسجيل-جديد','AuthClientController@register')->name('get.authClient.register');
        Route::post('/register','AuthClientController@do_register')->name('get.authClient.do_register');


        //  forgot password
        Route::get('/forgot-password','AuthClientController@forgot')->name('get.authClient.forgot');
        Route::post('/forgot-password','AuthClientController@checkForgot')->name('post.authClient.checkForgot');
        
        // new password
        Route::get('/change-password/{token}','AuthClientController@chPassword')->name('get.authClient.chPassword');
        Route::post('/new-password','AuthClientController@newPassword')->name('post.authClient.newPassword');

        

    });



    // cart 
    Route::get('/add-to-cart','Client\CartController@add')->name('get.cart.add');
    Route::get('/search','ProductController@search')->name('get.product.search');



    Route::group(['middleware'=>'client:client'],function()
    {
        // rating
        Route::get('/rate','ProductController@rate')->name('get.product.rate');
    });
    //  client is auth  ( function client() => this function return object of client if he is authunicated ) 
    Route::group(['middleware'=>'client:client','namespace'=>'Client'],function()
    {
        // logout
        Route::get('/خروج','AuthClientController@logout')->name('get.authClient.logout');

        Route::get('/حسابى','ClientController@account')->name('get.client.account');
        Route::get('/account/{page}','ClientController@page')->name('get.client.page');
        Route::post('/edit-information','ClientController@editInfo')->name('post.client.editInfo');
        Route::post('/change-password','ClientController@changePassword')->name('post.client.changePassword');
        Route::post('/edit-address','ClientController@editAddress')->name('post.client.editAddress');
        Route::get('/orders/{id}','OrderController@showOrder')->name('get.order.showOrder');
        



        Route::get('/طلباتى','ClientController@myOrders')->name('get.client.myOrders');
        Route::get('/add-to-wishlist/{id}','ClientController@wishlist')->name('get.client.wishlist');
        Route::get('/المفضلة','ClientController@myFavorits')->name('get.client.myFavorits');


        // cart 
        Route::get('/سلة-الشراء','CartController@show')->name('get.cart.show');
        Route::get('/cart/delete/{id}','CartController@delete')->name('get.cart.delete');
        Route::get('/cart/edit','CartController@update')->name('get.cart.update');
        Route::get('/اتمام-الشراء','CheckoutController@show')->name('get.checkout.show');
        Route::post('/checkout','CheckoutController@do_checkout')->name('post.checkout.do_checkout');


        //  reverce order 
        Route::get('/order/reverce/{id}','OrderController@reverce')->name('get.order.reverce');
        Route::get('/order/delete/{id}','OrderController@delete')->name('get.order.delete');
        Route::post('/do-reverce','OrderController@doReverce')->name('post.order.doReverce');





    });




    Route::get('/','HomeController@index')->name('get.home.index');
    Route::get('/autosearch','HomeController@autosearch')->name('get.home.autosearch'); // auto search 
    Route::get('/شروط-الاستخدام','HomeController@cond')->name('get.home.cond');
    Route::get('/activation/{code}','HomeController@activation')->name('get.home.activation');
    Route::get('/about','HomeController@about')->name('get.home.about');
    Route::get('/branches','HomeController@branches')->name('home.get.branches');
    Route::get('/contact-us','HomeController@contact')->name('get.home.contact');
    Route::get('/site/{slugPage}','HomeController@site')->name('get.home.site');
    Route::post('/suscribe/','MessageController@subscribe')->name('post.message.subscribe');
    Route::post('/send-message/','MessageController@sendMessage')->name('post.message.sendMessage');


 




    // category
    Route::get('/filter','CatController@filter')->name('get.cat.filter');
    Route::get('/sub-category/{slugSubCat}','CatController@showSubCat')->name('get.cat.showSubCat');
    Route::get('/{slugCat}','CatController@showCat')->name('get.cat.showCat');
    Route::get('/{slugProduct}/{slugCat}','ProductController@showProd')->name('get.product.showProd');



    











});
