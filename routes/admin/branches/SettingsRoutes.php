<?php 

//  categories routes
//  
Route::group(['prefix'=>'settings'],function()
{
	
	// show all data an data table 
	Route::get('/','SettingsController@index')->name('get.settings.index'); 
	// store data of item 
	Route::post('/update','SettingsController@update')->name('post.settings.update'); 


	// condition use
	Route::get('/condition','SettingsController@cond')->name('get.settings.cond'); 
	// 
	Route::put('/condition','SettingsController@updateCond')->name('post.settings.updateCond'); 


	Route::get('/seo','SettingsController@seo')->name('get.settings.seo');
	Route::put('/seo','SettingsController@seoUpdate')->name('put.settings.seoUpdate'); 


});