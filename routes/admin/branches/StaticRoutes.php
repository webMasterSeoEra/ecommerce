<?php 

//  categories routes
//  
Route::group(['prefix'=>'static'],function()
{
	
	// show all data an data table 
	Route::get('/all','StaticController@index')->name('get.static.index'); 
	// store data of item 
	Route::post('/store','StaticController@store')->name('post.static.store'); 
	// update data of specific item 
	Route::put('/update','StaticController@update')->name('put.static.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','StaticController@delete')->name('get.static.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','StaticController@deleteMulti')->name('post.static.deleteMulti'); 
	




	// words 
	Route::get('/words','StaticController@sc')->name('get.static.sc'); 
	Route::put('/words','StaticController@updatesc')->name('put.static.updateSc'); 


});