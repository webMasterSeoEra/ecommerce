<?php 

//  categories routes
//  
Route::group(['prefix'=>'product'],function()
{
	
	// show all data an data table 
	Route::get('/all','ProductController@index')->name('get.product.index'); 
	// store data of item 
	Route::get('/add','ProductController@add')->name('get.product.add'); 
	Route::post('/store','ProductController@store')->name('post.product.store'); 
	Route::get('/get-sub','ProductController@getSub')->name('get.product.getSub');

	// update data of specific item   
	Route::get('/edit/{id}','ProductController@edit')->name('get.product.edit')->where('id', '[0-9]+');
	Route::put('/update','ProductController@update')->name('put.product.update'); 
	Route::get('/seo/{id}','ProductController@seo')->name('get.product.seo')->where('id', '[0-9]+');
	Route::put('/seo','ProductController@seoUpdate')->name('put.product.seoUpdate'); 
	// delete data of specific item
	Route::get('/delete/{id}','ProductController@delete')->name('get.product.delete')->where('id', '[0-9]+');
	Route::get('/delete/image/{id}','ProductController@deleteImage')->name('get.product.deleteImage')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','ProductController@deleteMulti')->name('post.product.deleteMulti'); 


	// sort elements
	Route::post('/sort','ProductController@sort')->name('post.product.sort'); 

	
	

});