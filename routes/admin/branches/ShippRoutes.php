<?php 

//  categories routes
//  
Route::group(['prefix'=>'shipp'],function()
{
	
	// show all data an data table 
	Route::get('/all','ShippController@index')->name('get.shipp.index'); 
	// store data of item 
	Route::post('/store','ShippController@store')->name('post.shipp.store'); 
	// update data of specific item 
	Route::put('/update','ShippController@update')->name('put.shipp.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','ShippController@delete')->name('get.shipp.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','ShippController@deleteMulti')->name('post.shipp.deleteMulti'); 



	Route::get('/seo/{id}','ShippController@seo')->name('get.shipp.seo')->where('id', '[0-9]+');
	Route::put('/seo','ShippController@seoUpdate')->name('put.shipp.seoUpdate'); 
	

});