<?php 

//  categories routes
//  
Route::group(['prefix'=>'shippWay'],function()
{
	
	// show all data an data table 
	Route::get('/all','ShippWayController@index')->name('get.shippWay.index'); 
	// store data of item 
	Route::post('/store','ShippWayController@store')->name('post.shippWay.store'); 
	// update data of specific item 
	Route::put('/update','ShippWayController@update')->name('put.shippWay.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','ShippWayController@delete')->name('get.shippWay.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','ShippWayController@deleteMulti')->name('post.shippWay.deleteMulti'); 



	Route::get('/seo/{id}','ShippWayController@seo')->name('get.shippWay.seo')->where('id', '[0-9]+');
	Route::put('/seo','ShippWayController@seoUpdate')->name('put.shippWay.seoUpdate'); 
	

});