<?php 

//  categories routes
//  
Route::group(['prefix'=>'branch'],function()
{
	
	// show all data an data table 
	Route::get('/all','BranchController@index')->name('get.branch.index'); 
	// store data of item 
	Route::post('/store','BranchController@store')->name('post.branch.store'); 
	// update data of specific item 
	Route::put('/update','BranchController@update')->name('put.branch.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','BranchController@delete')->name('get.branche.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','BranchController@deleteMulti')->name('post.branch.deleteMulti'); 
	

});