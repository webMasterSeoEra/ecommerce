<?php 

//  categories routes
//  
Route::group(['prefix'=>'cupon'],function()
{
	
	// show all data an data table 
	Route::get('/all','CuponController@index')->name('get.cupon.index'); 
	// store data of item 
	Route::post('/store','CuponController@store')->name('post.cupon.store'); 
	// update data of specific item 
	Route::put('/update','CuponController@update')->name('put.cupon.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','CuponController@delete')->name('get.cupon.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','CuponController@deleteMulti')->name('post.cupon.deleteMulti'); 
	

});