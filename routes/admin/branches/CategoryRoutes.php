<?php 

//  categories routes
//  
Route::group(['prefix'=>'category'],function()
{
	
	// show all data an data table 
	Route::get('/all','CategoryController@index')->name('get.category.index'); 
	// store data of item 
	Route::post('/store','CategoryController@store')->name('post.category.store'); 
	// update data of specific item 
	Route::put('/update','CategoryController@update')->name('put.category.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','CategoryController@delete')->name('get.category.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','CategoryController@deleteMulti')->name('post.category.deleteMulti'); 



	Route::get('/seo/{id}','CategoryController@seo')->name('get.category.seo')->where('id', '[0-9]+');
	Route::put('/seo','CategoryController@seoUpdate')->name('put.category.seoUpdate'); 


	// sort elements
	Route::post('/sort','CategoryController@sort')->name('post.category.sort'); 
	
	

});