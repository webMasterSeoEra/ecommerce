<?php 

//  categories routes
//  
Route::group(['prefix'=>'order'],function()
{
	
	// reverced 
	Route::get('/reverced','OrderController@reverced')->name('get.order.reverced'); 
	Route::get('/reverced/{id}','OrderController@revercedDelete')->name('get.order.revercedDelete');
	Route::get('/change-status','OrderController@changeStatus')->name('get.order.changeStatus');
	




	Route::get('/show-content/{id}','OrderController@showContent')->name('get.order.showContent'); 
	Route::get('/print/{id}','OrderController@print')->name('get.order.print'); 
	Route::get('/client/{id}','OrderController@client')->name('get.order.client'); 
	Route::get('/{type?}','OrderController@index')->name('get.order.index'); 
	Route::get('/add-to-shipping/{id}','OrderController@addToShipping')->name('get.ordere.addToShipping'); 
	Route::get('/add-to-accepted/{id}','OrderController@addToAccepted')->name('get.ordere.addToAccepted'); 
	Route::get('/add-to-refused/{id}','OrderController@addToRefused')->name('get.ordere.addToRefused'); 
	Route::get('/add-to-delete/{id}','OrderController@addToDelete')->name('get.ordere.addToDelete'); 

	

});