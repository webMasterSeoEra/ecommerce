<?php 

//  categories routes
//  
Route::group(['prefix'=>'admin'],function()
{
	
	// show all data an data table 
	Route::get('/all','AdminController@index')->name('get.admin.index'); 
	// store data of item 
	Route::post('/store','AdminController@store')->name('post.admin.store'); 
	// update data of specific item 
	Route::put('/update','AdminController@update')->name('put.admin.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','AdminController@delete')->name('get.admin.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','AdminController@deleteMulti')->name('post.admin.deleteMulti'); 

	

});