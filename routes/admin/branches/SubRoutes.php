<?php 

//  categories routes
//  
Route::group(['prefix'=>'subcat'],function()
{
	
	// show all data an data table 
	Route::get('/all','SubController@index')->name('get.subcat.index'); 
	// store data of item
	Route::post('/store','SubController@store')->name('post.subcat.store'); 
	// update data of specific item 
	Route::put('/update','SubController@update')->name('put.subcat.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','SubController@delete')->name('get.subcat.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','SubController@deleteMulti')->name('post.subcat.deleteMulti'); 




	Route::get('/seo/{id}','SubController@seo')->name('get.subcat.seo')->where('id', '[0-9]+');
	Route::put('/seo','SubController@seoUpdate')->name('put.subcat.seoUpdate'); 
	

});