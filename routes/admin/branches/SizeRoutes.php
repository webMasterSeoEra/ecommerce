<?php 

//  categories routes
//  
Route::group(['prefix'=>'size'],function()
{
	
	// show all data an data table 
	Route::get('/all','SizeController@index')->name('get.size.index'); 
	// store data of item 
	Route::post('/store','SizeController@store')->name('post.size.store'); 
	// update data of specific item 
	Route::put('/update','SizeController@update')->name('put.size.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','SizeController@delete')->name('get.size.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','SizeController@deleteMulti')->name('post.size.deleteMulti'); 
	

});