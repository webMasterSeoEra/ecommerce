<?php 

//  categories routes
//  
Route::group(['prefix'=>'client'],function()
{
	
	// show all data an data table 
	Route::get('/all','ClientController@index')->name('get.client.index'); 
	// store data of item 
	// Route::post('/store','SizeController@store')->name('post.client.store'); 
	// // update data of specific item 
	// Route::put('/update','SizeController@update')->name('put.client.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','ClientController@delete')->name('get.client.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','ClientController@deleteMulti')->name('post.client.deleteMulti'); 
	

});