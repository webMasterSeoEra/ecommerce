<?php 

//  categories routes
//  
Route::group(['prefix'=>'city'],function()
{
	
	// show all data an data table 
	Route::get('/all','CityController@index')->name('get.city.index'); 
	// store data of item 
	Route::post('/store','CityController@store')->name('post.city.store'); 
	// update data of specific item 
	Route::put('/update','CityController@update')->name('put.city.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','CityController@delete')->name('get.city.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','CityController@deleteMulti')->name('post.city.deleteMulti'); 




});