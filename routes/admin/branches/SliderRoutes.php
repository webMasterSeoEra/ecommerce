<?php 

//  categories routes
//  
Route::group(['prefix'=>'slider'],function()
{
	
	// show all data an data table 
	Route::get('/all','SliderController@index')->name('get.slider.index'); 
	// store data of item 
	Route::post('/store','SliderController@store')->name('post.slider.store'); 
	// update data of specific item 
	Route::put('/update','SliderController@update')->name('put.slider.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','SliderController@delete')->name('get.slider.delete')->where('id', '[0-9]+');
	// delete multi  item
	Route::post('/delete/multi','SliderController@deleteMulti')->name('post.slider.deleteMulti'); 
	

});