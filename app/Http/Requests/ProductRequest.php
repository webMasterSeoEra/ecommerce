<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) 
        {

            // adding data 
            case 'POST':

                    return [
                        'name'              => 'required|string|max:190',
                        'category_id'       => 'required|numeric|exists:categories,id',
                        'sub_cat_id'        => 'nullable|numeric|exists:sub_cats,id',
                        'size_id'           => 'nullable|numeric|exists:sizes,id',                        
                        'price'             => 'required|numeric|digits_between:1,5',
                        'img'               => 'required|image|mimes:png,jpg,jpeg,gif|max:5000',
                        // 'img2'              => 'required|image|mimes:png,jpg,jpeg,gif|max:5000',
                        'image'             => 'nullable',
                        'image.*'           => 'image|mimes:jpeg,png,jpg,gif,svg|max:100000',
                        'desc'              => 'required|string|max:100000',
                        'special'           => 'required|string|in:yes,no',
                        'stock'             => 'nullable|numeric|digits_between:1,6',
                        'tags'              => 'required|string|max:190',
                        'fit'               => 'nullable|string|max:190',
                        'slug'              => 'nullable|string|unique:categories,slug|max:150',
                    ];


                break;


            //  editing data 
            case 'PUT':
                        
                        return [
                        'name'              => 'required|string|max:190',
                        'category_id'       => 'required|numeric|exists:categories,id',
                        'sub_cat_id'        => 'nullable|numeric|exists:sub_cats,id',
                        'size_id'           => 'nullable|numeric|exists:sizes,id',  
                        'price'             => 'required|numeric|digits_between:1,5',
                        'img'               => 'nullable|image|mimes:png,jpg,jpeg,gif|max:5000',
                        // 'img2'              => 'nullable|image|mimes:png,jpg,jpeg,gif|max:5000',
                        'image'             => 'nullable',
                        'image.*'           => 'image|mimes:jpeg,png,jpg,gif,svg|max:100000',
                        'desc'              => 'required|string|max:100000',
                        'special'           => 'required|string|in:yes,no',
                        'stock'             => 'nullable|numeric|digits_between:1,6',
                        'tags'              => 'required|string|max:190',
                        'fit'               => 'nullable|string|max:190',
                        'slug'              => 'nullable|string|max:150|unique:categories,slug,' . Request('id'),
                    ];


                break;
        }



    }
}
