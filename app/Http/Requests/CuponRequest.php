<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CuponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) 
        {

            // adding data 
            case 'POST':

                    return [

                        'cupon_number'      => 'required|string|unique:cupons,cupon_number,email|max:190',
                        'percent'           => 'required|numeric|max:190',
                     
                    ];


                break;


            //  editing data 
            case 'PUT':
                        
                        return [

                            'cupon_number'      => 'required|string|max:190|unique:cupons,cupon_number,' . Request('id'),
                            'percent'           => 'required|numeric|max:190',
                     
                    ];


                break;
        }



    }
}
