<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) 
        {

            // adding data 
            case 'POST':

                    return [
                        'name'              => 'required|string|max:190',
                        'address'           => 'required|string|max:190',
                        'mobile1'           => 'required|string|max:190',
                        'map'               => 'nullable|string|max:3000',
                    ];


                break;


            //  editing data 
            case 'PUT':
                        
                        return [
                       
                        'name'              => 'required|string|max:190',
                        'address'           => 'required|string|max:190',
                        'mobile1'           => 'required|string|max:190',
                        'map'               => 'nullable|string|max:100000',
                    ];


                break;
        }



    }
}
