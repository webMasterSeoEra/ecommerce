<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) 
        {

            // adding data 
            case 'POST':

                    return [
                        'name'              => 'required|string|max:190',
                        'min_number'        => 'nullable|string|between:0,9999.99',
                        'discount_value'    => 'nullable|string|between:0,9999.99',
                        'discount_text'     => 'nullable|string|max:199990',

                    ];


                break;


            //  editing data 
            case 'PUT':
                        
                        return [
                            'name'              => 'required|string|max:190',
                            'min_number'        => 'nullable|string|between:0,9999.99',
                            'discount_value'    => 'nullable|string|between:0,9999.99',
                            'discount_text'     => 'nullable|string|max:199990',
                    ];


                break;
        }



    }
}
