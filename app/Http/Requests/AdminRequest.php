<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) 
        {

            // adding data 
            case 'POST':

                    return [
                        'name'              => 'required|string|max:190',
                        'email'             => 'required|email|unique:admins,email',
                        'password'          => 'required|string|max:190',


                    ];


                break;


            //  editing data 
            case 'PUT':
                        
                        return [
                            'name'              => 'required|string|max:190',
                            'email'             => 'required|email|unique:admins,email,' . Request('id'),
                            'password'          => 'nullable|string|max:190',
                    ];


                break;
        }



    }
}
