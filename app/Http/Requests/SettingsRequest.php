<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) 
        {

            // adding data 
            case 'POST':

                    return [
                        'name'                  => 'nullable|string|max:190',
                        'email'                 => 'nullable|email|max:190',
                        'mobile1'               => 'nullable|string|max:190',
                        'facebook'              => 'nullable|url|max:190',
                        'instagram'             => 'nullable|url|max:190',
                        'twitter'               => 'nullable|url|max:190',
                        'youtube'               => 'nullable|url|max:190',
                        'fax'                   => 'nullable|string|max:190',
                        'address1'              => 'nullable|string|max:190',
                        'logo1'                 => 'nullable|image|mimes:png,jpg,jpeg,gif|max:5000',
                        'map'                   => 'nullable|max:2000',
                        'additional_value'      => 'required',
                        
                    ];


                break;


        }



    }
}
