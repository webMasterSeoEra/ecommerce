<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SubCat;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Size;

use Image;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Classes\UploadClass;

class ProductController extends Controller
{
    


    public function index()
    {
        $data['basePage']       = trans('admin.home');
        $data['secondName']     = trans('admin.products');
        

        $data['products'] = Product::orderBy('sort','ASC')->paginate(50);
        return view('admin.product.products')->with($data);
    }






    public function add()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName']     = trans('admin.products');  
        $data['thirdName']      = trans('admin.addProduct');
        $data['routeSecond']    = route('admin.get.product.index');

        $data['categories'] = Category::select('id','name')
        ->orderBy('id','DESC')->get();
        $data['sizes'] = Size::select('id','name')
        ->orderBy('id','DESC')->get();
        return view('admin.product.add')->with($data);
    }




    public function getSub(Request $request)
    {
        Category::findOrFail($request->cat);
        $data['subCats'] = SubCat::select('id','name')
        ->where('category_id',$request->cat)->get();
        return view('admin.product.inc.getSub')->with($data);
    }





     // storing data in db 
     public function store(ProductRequest $request)
     {
         $data = $request->validated();

         // upload image of this module
        if($request->hasFile('img'))
        {
            $img = UploadClass::uploaImage($request,'img',UPLOADS_PATH.PRODUCT_PATH);
            $data['img'] = $img;
            // $img2 = UploadClass::uploaImage($request,'img2',UPLOADS_PATH.PRODUCT_PATH);
            // $data['img2'] = $img2;
        }

         if($request->slug != ''){$data['slug'] = slug($request->slug);}
         else{$data['slug'] = slug($request->name).uniqid();}
         $prod = Product::create($data); // inserting data 


         if($request->hasfile('image'))
         {
            foreach($request->file('image') as $image)
            { 
                if($image)
                {
                    $prodImage = new ProductImage();
                    // upload image using intervention without resizeing
                    $img = Image::make($image);
                    // save image
                    $img->save(UPLOADS_PATH.PRODUCT_PATH.$image->hashName());

                    $prodImage->product_id = $prod->id;
                    $prodImage->name = $image->hashName();
                    $prodImage->save();
                }

            }
         }



         $msg['success'] = trans('admin.addedSuccess');
         return response()->json($msg);
        
     }









    public function edit($id)
    {
        $data['row'] = Product::findOrFail($id);
        $data['basePage'] = trans('admin.home');
        $data['secondName']     = trans('admin.products');  
        $data['thirdName']      = trans('admin.editProduct');
        $data['routeSecond']    = route('admin.get.product.index');

        $data['categories'] = Category::select('id','name')
        ->orderBy('id','DESC')->get();
        $data['sizes'] = Size::select('id','name')
        ->orderBy('id','DESC')->get();
        $data['subcats'] = SubCat::select('id','name')
        ->where('category_id',$data['row']['category_id'])
        ->orderBy('id','DESC')->get();
        return view('admin.product.edit')->with($data);
    }



     // storing data in db 
    public function update(ProductRequest $request)
    {

        
        $data = $request->validated();
        $old = Product::findOrFail($request->id);
          // upload image of this module
          if($request->hasFile('img'))
          {   
              if($old->img)
              {
                  Storage::disk('public_uploads')->delete(PRODUCT_PATH.$old->img);
              }
              $data['img']  = UploadClass::uploaImage($request,'img',UPLOADS_PATH.PRODUCT_PATH);               
          
          }


          
          
        //  slug
        if($request->slug != ''){$data['slug'] = slug($request->slug);}
        else{$data['slug'] = slug($request->name).uniqid();}
        // updating data in db
        $data = $request->except('image','_token','id','_method');
        Product::where('id', $request->id)->update($data);
        // dd($request->all());

        
        if($request->hasfile('image'))
        {
            foreach($request->file('image') as $image)
            { 
                if($image)
                {
                    $prodImage = new ProductImage();
                    // upload image using intervention without resizeing
                    $img = Image::make($image);
                    // save image
                    $img->save(UPLOADS_PATH.PRODUCT_PATH.$image->hashName());

                    $prodImage->product_id = $request->id;
                    $prodImage->name = $image->hashName();
                    $prodImage->save();
                }

            }
        }


    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }






    // deleteing row from db  
    public function delete($id)
    {
        $old = Product::findOrFail($id);
        // delete old image from server
        if($old->img)
        {
            Storage::disk('public_uploads')->delete(PRODUCT_PATH.$old->img);
        }
        
        
        Product::findOrFail($id)->delete();

    	$msg['success'] = trans('admin.msg.deleted_success');
        return response()->json($msg);

    }






     // deleteing row from db   ( delete image )
     public function deleteImage($id)
     {
         $old = ProductImage::findOrFail($id);
         // delete old image from server
         if($old->name)
         {
             Storage::disk('public_uploads')->delete(PRODUCT_PATH.$old->name);
         }
         
         
         ProductImage::findOrFail($id)->delete();
  
         $msg['success'] = trans('admin.msg.deleted_success');
         return response()->json($msg);
 
     }
 



















    // seo 

    public function seo($id)
    {
        $data['row'] = Product::findOrFail($id);
        $data['basePage'] = trans('admin.home');
        $data['secondName']     = trans('admin.products');  
        $data['thirdName']      = trans('admin.seoProduct') . ' / '.$data['row']['name'];
        $data['routeSecond']    = route('admin.get.product.index');

        $data['seoData'] =  json_decode($data['row']['seo']);

        return view('admin.product.seo')->with($data);
    }




     // storing data in db 
     public function seoUpdate(Request $request)
     {
         $data = Product::findOrFail($request->id);
         $row['seoHeader'] = nl2br($request->seoHeader);
         $row['seoFooter'] = nl2br($request->seoFooter);
         $allData = json_encode($row);

         $data->seo = $allData;
         $data->save();
         // updating data in db
        //  Product::where('id', $request->id)->update($data);
         $msg['success'] = trans('admin.msg.EditedSuccess');
         return response()->json($msg);
     }














     //  sorting elements     
     public function sort(Request $request)
     {
         $i = 1;
         foreach ($request->sort as  $value) 
         {
             $data = Product::find($value);
             $data->sort = $i;
             $data->save();
             $i++;
         }
         return back();
     }

  
 

     

















}
