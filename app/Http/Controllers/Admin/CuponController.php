<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cupon;
use App\Http\Requests\CuponRequest;

class CuponController extends Controller
{
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.cupons');

        $data['cupons'] = Cupon::orderBy('id','DESC')->paginate(25);
        return view('admin.cupon.index')->with($data);
    }





     // storing data in db 
     public function store(CuponRequest $request)
     {
         $data = $request->validated();

         Cupon::create($data); // inserting data 
         $msg['success'] = trans('admin.addedSuccess');
         return response()->json($msg);
        
     }










     // storing data in db 
    public function update(CuponRequest $request)
    {
    	$data = $request->validated();
    
    	// updating data in db
    	Cupon::where('id', $request->id)->update($data);
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }






     // deleteing row from db  ( soft delete )
    public function delete($id)
    {
         
        Cupon::findOrFail($id);
        Cupon::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }





}
