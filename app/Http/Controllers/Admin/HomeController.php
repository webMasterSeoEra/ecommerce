<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderReverce;
use App\Models\Product;
use App\Models\Client;
use App\Models\Message;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['orders'] = Order::orderBy('id','DESC')->take(15)->get();
        $data['allPrice'] = Order::where('status','accepted')->sum('total_price');
        $data['refused'] = OrderReverce::count();
        $data['products'] = Product::count();
        $data['ordersCount'] = Order::count();
        $data['clients'] = Client::count();



        $data['pending'] = Order::where('status','pending')->count();
        $data['accepted'] = Order::where('status','accepted')->count();
        $data['cancelled'] = Order::where('status','refused')->count();
        $data['shipping'] = Order::where('status','shipping')->count();
  


        // the most selling product
        $data['topsaleProduct'] = DB::table('order_contents')
           ->leftJoin('products','products.id','=','order_contents.product_id')
           ->select('products.id','products.name','order_contents.product_id',
                DB::raw('SUM(order_contents.qty) as total'))
           ->groupBy('products.id','order_contents.product_id','products.name')
           ->orderBy('total','desc')
           ->first();

        // the most view product
         $data['topViewProduct'] = DB::table('products')
           ->select('products.id','products.name',
                DB::raw('SUM(products.views) as total'))
           ->groupBy('products.id','products.name')
           ->orderBy('total','desc')
           ->first();

          //  get number of messages to the sestem 
          $data['messages'] = Message::count();


          // get selling of past day
          $yesterday          = date("Y-m-d",strtotime("yesterday"));
          $data['priceToday'] = Order::where('status','accepted')
          ->where('created_at','>',$yesterday)->sum('total_price');


          // get selling of past week
          $lastWeek          = date("Y-m-d",strtotime("-1 week"));
          $data['priceWeek'] = Order::where('status','accepted')
          ->where('created_at','>',$lastWeek)->sum('total_price');

          $lastMonth = date("Y-m-d", strtotime("-1 month"));
          $data['priceMonth'] = Order::where('status','accepted')
          ->where('created_at','>',$lastMonth)->sum('total_price');


          $firstDayYear  = date('Y-m-d', strtotime('first day of january this year'));       
          $data['pricesPerMonth'] = Order::select(
            DB::raw('sum(total_price) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"),
            DB::raw('sum(created_at) as ides'))->where('created_at','>',$firstDayYear)
          ->groupBy('months')
          ->orderBy('ides','ASC')
          ->get();
         

        return view('admin.index')->with($data);
    }
}
