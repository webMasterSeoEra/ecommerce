<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Size;
use App\Http\Requests\SizeRequest;

class SizeController extends Controller
{
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.size');

        $data['sizes'] = Size::orderBy('id','DESC')->paginate(25);
        return view('admin.size.index')->with($data);
    }





     // storing data in db 
     public function store(SizeRequest $request)
     {
         $data = $request->validated();
         Size::create($data); // inserting data 
         $msg['success'] = trans('admin.addedSuccess');
         return response()->json($msg);
        
     }










     // storing data in db 
    public function update(SizeRequest $request)
    {
    	$data = $request->validated();
    	// updating data in db
    	Size::where('id', $request->id)->update($data);
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }






     // deleteing row from db  ( soft delete )
    public function delete($id)
    {
         
        Size::findOrFail($id);
        Size::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }





}
