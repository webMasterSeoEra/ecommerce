<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\SliderRequest;
use App\Http\Controllers\Controller;
use App\Models\Slider;

use Image;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Classes\UploadClass;

class SliderController extends Controller
{
    public function index()
    {

    	$data['sliders'] = Slider::orderBy('id','DESC')
    	->paginate(25);
    	return view('admin.slider.index')->with($data);
    }



    // return view of adding data 
    public function add()
    {
    	return view('admin.slider.add');
    }





    // storing data in db 
    public function store(SliderRequest $request)
    {
    	$data = $request->validated();

        // upload image of this module
        if($request->hasFile('img'))
        {
            // UPLOADS_PATH.SLIDER_PATH 
            // -> static path ( you can change it from helpers/files/basehelper)

            $img = UploadClass::uploaImage($request,'img',UPLOADS_PATH.SLIDER_PATH);
            //$img = UploadClass::uploadFile($request,'img',UPLOADS_PATH.SLIDER_PATH,$all=true,$big=1340);
            $data['img'] = $img;
        }
    	Slider::create($data); // inserting data 
    	$msg['success'] = trans('admin.addedSuccess');
        return response()->json($msg);
    }








    // storing data in db 
    public function update(SliderRequest $request)
    {
    	$data = $request->validated();

        // upload image of this module
        if($request->hasFile('img'))
        {
            $old = Slider::findOrFail($request->id);
            // delete old image from server
            if($old->img)
            {
                Storage::disk('public_uploads')->delete(SLIDER_PATH.$old->img);
            }

            $img = UploadClass::uploaImage($request,'img',UPLOADS_PATH.SLIDER_PATH);
            $data['img'] = $img;
        }
    	// updating data in db
    	Slider::where('id', $request->id)->update($data);
    	$msg['success'] = trans('admin.addedSuccess');
        return response()->json($msg);
    }



















    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
        $old = Slider::findOrFail($id);
        // delete old image from server
        if($old->img)
        {
            Storage::disk('public_uploads')->delete(SLIDER_PATH.$old->img);
        }
        
        
        Slider::findOrFail($id)->delete();
 
    	$msg['success'] = trans('admin.msg.deleted_success');
        return response()->json($msg);

    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{
            $old = Slider::findOrFail($value);
            // delete old image from server
            if($old->img)
            {
                Storage::disk('public_uploads')->delete(SLIDER_PATH.$old->img);
            }

            Slider::findOrFail($value)->delete();
	    	
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return back();
    }












}
