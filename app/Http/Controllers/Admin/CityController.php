<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Http\Requests\CityRequest;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Classes\UploadClass;


class CityController extends Controller
{
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.cities');

        $data['rows'] = City::orderBy('id','DESC')->paginate(25);
        return view('admin.city.index')->with($data);
    }





     // storing data in db 
     public function store(CityRequest $request)
     {
         $data = $request->validated();
         City::create($data); // inserting data 
         $msg['success'] = trans('admin.addedSuccess');
         return response()->json($msg);
        
     }










     // storing data in db 
    public function update(CityRequest $request)
    {
    	$data = $request->validated();

    	// updating data in db
    	City::where('id', $request->id)->update($data);
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }






     // deleteing row from db  ( soft delete )
    public function delete($id)
    {
         
        City::findOrFail($id);
        City::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }











         








}
