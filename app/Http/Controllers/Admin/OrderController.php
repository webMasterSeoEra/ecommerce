<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderContent;

use App\Models\OrderReverce;
use App\Mail\OrderShipping;
use App\Mail\OrderAccepted;
use App\Mail\OrderRefused;
use Mail;
use App\Models\Client;
use App\Models\Product;
use App\Models\Notification;


class OrderController extends Controller
{


    public function index($type = null)
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName']     = trans('admin.orders');  
        $data['routeSecond']    = route('admin.get.order.index');

        switch ($type) {
            case 'pending':

            //  notification of order
            Notification::where('status','0')->where('type','order')->update(['status'=>'1']);
            $data['thirdName']      = trans('admin.ordersPending');
            $data['orders'] = Order::where('status','pending')->orderBy('id','DESC')->paginate(50);
                
                break;


            case 'shipping':

                $data['thirdName']      = trans('admin.ordersShipping');
                $data['orders'] = Order::where('status','shipping')->orderBy('id','DESC')->paginate(50);
                    
                    break;

            case 'accepted':

                $data['thirdName']      = trans('admin.ordersAccepted');
                $data['orders'] = Order::where('status','accepted')->orderBy('id','DESC')->paginate(50);
                    
                    break;

            case 'refused':
                //  notification of order
                Notification::where('status','0')->where('type','cancelled')->update(['status'=>'1']);
                $data['thirdName']      = trans('admin.ordersRefused');
                $data['orders'] = Order::where('status','refused')->orderBy('id','DESC')->paginate(50);
                        
                        break;
        }
        if(!$type)
        {
            $data['thirdName']      = trans('admin.ordersPending');
            $data['orders'] = Order::where('status','pending')->orderBy('id','DESC')->paginate(50);
        }
        
        return view('admin.order.index')->with($data);
    }




    public function showContent($id)
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName']     = trans('admin.orders');  
        $data['routeSecond']    = route('admin.get.order.index');
        $data['thirdName']      = trans('admin.orderContent');

        $data['row'] = Order::findOrFail($id);
        $data['content'] = OrderContent::where('order_id',$id)->paginate(50);
        return view('admin.order.showContent2')->with($data);
    }



    public function print($id)
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName']     = trans('admin.orders');  
        $data['routeSecond']    = route('admin.get.order.index');
        $data['thirdName']      = trans('admin.orderContent');

        $data['row'] = Order::findOrFail($id);
        $data['print'] = "print";
        $data['content'] = OrderContent::where('order_id',$id)->paginate(50);
        return view('admin.order.showContent2')->with($data);
    }





    // add order to shipping
    public function addToShipping($id)
    {
        
        $data = Order::findOrFail($id);
        $data->status = "shipping";
        $data->save();
        $message['status'] = true;

        $msg['success'] = "Added Success";
        $msg['code'] = $id * 250;
        $msg['order'] = $data;
        $msg['orderContent'] = OrderContent::where('order_id',$id)->get();
        Mail::to($data->client->email)->queue(new OrderShipping($msg));

        return response()->json($message);

    }



    // add order to shipping
    public function addToAccepted($id)
    {
        
        $data = Order::findOrFail($id);
        $data->status = "accepted";
        $data->save();
        $message['status'] = true;
        $msg['success'] = "Added Success";
        $msg['code'] = $id * 250;
        $msg['order'] = $data;
        $content = OrderContent::where('order_id',$id)->get();
        $msg['orderContent']  = $content;
        Mail::to($data->client->email)->queue(new OrderAccepted($msg));

        foreach($content as $co)
        {
            $prod = Product::where('id',$co->product_id)->first();
            $prod->stock = ($prod->stock - $co->qty);
            $prod->save();
        }

        

        return response()->json($message);

    }


    // add order to refused
    public function addToRefused($id)
    {
        
        $data = Order::findOrFail($id);
        $data->status = "refused";
        $data->save();
        $message['status'] = true;
        $msg['success'] = "Added Success";
        $msg['code'] = $id * 250;
        $msg['order'] = $data;
        $msg['orderContent'] = OrderContent::where('order_id',$id)->get();
        Mail::to($data->client->email)->queue(new OrderRefused($msg));
        return response()->json($message);

    }


    // deleteing row from db  
    public function addToDelete($id)
    {
        
        Order::findOrFail($id);
        Order::findOrFail($id)->delete();
        $message['status'] = true;
        return response()->json($message);

    }



    


    public function reverced()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName']     = trans('admin.orders');  
        $data['routeSecond']    = route('admin.get.order.index');

        $data['thirdName']      = trans('admin.orderReverced');
        $data['orders'] = OrderReverce::paginate(50);
        Notification::where('status','0')->where('type','refused')->update(['status'=>'1']);

        return view('admin.order.orderReverced')->with($data);
    }


    public function revercedDelete($id)
    {
        
        OrderReverce::findOrFail($id)->delete();
        $message['status'] = true;
        return response()->json($message);

    }


    



    

    public function changeStatus(Request $request)
    {
        
        $data = Order::findOrFail($request->id);
        // dd($request->id);
        $request->validate(['status'=>'required|string|in:pending,shipping,accepted,refused']);
        $data->status = $request->status;
        $data->save();
        return back();
        

    }






    // ccient orders 
    public function client($id)
    {
        $row = Client::findOrFail($id);
        $data['basePage'] = trans('admin.home');
        $data['secondName']     = trans('admin.orders');  
        $data['routeSecond']    = route('admin.get.order.index');
        $data['thirdName']      = trans('admin.clientOrder');

        $data['orders'] = Order::where('client_id',$id)->paginate(50);
        return view('admin.order.clientOrder')->with($data);
    }






    


}
