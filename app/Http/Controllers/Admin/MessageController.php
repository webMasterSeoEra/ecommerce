<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\Newsletter;
use App\Models\Notification;


class MessageController extends Controller
{
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.messagesVisitor');

        //  notification of order
        Notification::where('status','0')->where('type','complaint')->update(['status'=>'1']);
        $data['messages'] = Message::orderBy('id','DESC')->paginate(25);
        return view('admin.messages.index')->with($data);
    }






     // deleteing row from db  ( soft delete )
    public function delete($id)
    {
         
        Message::findOrFail($id);
        Message::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }






    public function newsletter()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.newsletter');

        $data['newsletter'] = Newsletter::orderBy('id','DESC')->paginate(25);
        return view('admin.messages.newsletter')->with($data);
    }






     // deleteing row from db  ( soft delete )
    public function deleteNews($id)
    {
         
        Newsletter::findOrFail($id);
        Newsletter::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }





}
