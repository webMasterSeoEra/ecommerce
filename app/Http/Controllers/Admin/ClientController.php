<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Notification;



class ClientController extends Controller
{



    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.clients');
        // update notifications for client
        Notification::where('status','0')->where('type','client')->update(['status'=>'1']);

        $data['clients'] = Client::orderBy('id','DESC')->paginate(25);
        return view('admin.clients.index')->with($data);
    }









     // deleteing row from db  ( soft delete )
    public function delete($id)
    {
         
        Client::findOrFail($id);
        Client::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }





}
