<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShippWay;
use App\Http\Requests\ShippWayRequest;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Classes\UploadClass;


class ShippWayController extends Controller
{
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.shippmentWay');

        $data['rows'] = ShippWay::orderBy('id','DESC')->paginate(25);
        return view('admin.shippmentWay.index')->with($data);
    }





     // storing data in db 
     public function store(ShippWayRequest $request)
     {
         $data = $request->validated();
         if($request->slug != ''){$data['slug'] = slug($request->slug);}
         else{$data['slug'] = slug($request->name).uniqid();}

         // upload image of this module
        if($request->hasFile('img'))
        {
            $img = UploadClass::uploaImage($request,'img',UPLOADS_PATH.CATEGORY_PATH);
            $data['img'] = $img;
        }



         ShippWay::create($data); // inserting data 
         $msg['success'] = trans('admin.addedSuccess');
         return response()->json($msg);
        
     }










     // storing data in db 
    public function update(ShippWayRequest $request)
    {
    	$data = $request->validated();
        //  slug
        if($request->slug != ''){$data['slug'] = slug($request->slug);}
        else{$data['slug'] = slug($request->name).uniqid();}
        // upload image of this module
        if($request->hasFile('img'))
        {
            $old = ShippWay::findOrFail($request->id);
            // delete old image from server
            if($old->img)
            {
                Storage::disk('public_uploads')->delete(CATEGORY_PATH.$old->img);
            }

            $img = UploadClass::uploaImage($request,'img',UPLOADS_PATH.CATEGORY_PATH);
            $data['img'] = $img;
        }
    	// updating data in db
    	ShippWay::where('id', $request->id)->update($data);
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }






     // deleteing row from db  ( soft delete )
    public function delete($id)
    {
         
        ShippWay::findOrFail($id);
        ShippWay::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }
















        // seo 

        public function seo($id)
        {
            $data['row'] = ShippWay::findOrFail($id);
            $data['basePage']       = trans('admin.home');
            $data['secondName']     = trans('admin.category');  
            $data['thirdName']      = trans('admin.seoData') . ' / '.$data['row']['name'];
            $data['routeSecond']    = route('admin.get.shppmentWay.index');
    
            $data['seoData'] =  json_decode($data['row']['seo']);
    
            return view('admin.shppmentWay.seo')->with($data);
        }
    
    
    
    
         // storing data in db 
         public function seoUpdate(Request $request)
         {
             $data = ShippWay::findOrFail($request->id);
             $row['seoHeader'] = nl2br($request->seoHeader);
             $row['seoFooter'] = nl2br($request->seoFooter);
             $allData = json_encode($row);
    
             $data->seo = $allData;
             $data->save();
             // updating data in db
            //  Product::where('id', $request->id)->update($data);
             $msg['success'] = trans('admin.msg.EditedSuccess');
             return response()->json($msg);
         }
     
    
     
         








}
