<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Classes\UploadClass;


class CategoryController extends Controller
{
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.category');

        $data['categories'] = Category::orderBy('sort','ASC')->paginate(25);
        return view('admin.category.index')->with($data);
    }





     // storing data in db 
     public function store(CategoryRequest $request)
     {
         $data = $request->validated();
         if($request->slug != ''){$data['slug'] = slug($request->slug);}
         else{$data['slug'] = slug($request->name).uniqid();}

         // upload image of this module
        if($request->hasFile('img'))
        {
            $img = UploadClass::uploaImage($request,'img',UPLOADS_PATH.CATEGORY_PATH);
            $data['img'] = $img;
        }



         Category::create($data); // inserting data 
         $msg['success'] = trans('admin.addedSuccess');
         return response()->json($msg);
        
     }










     // storing data in db 
    public function update(CategoryRequest $request)
    {
    	$data = $request->validated();
        //  slug
        if($request->slug != ''){$data['slug'] = slug($request->slug);}
        else{$data['slug'] = slug($request->name).uniqid();}
        // upload image of this module
        if($request->hasFile('img'))
        {
            $old = Category::findOrFail($request->id);
            // delete old image from server
            if($old->img)
            {
                Storage::disk('public_uploads')->delete(CATEGORY_PATH.$old->img);
            }

            $img = UploadClass::uploaImage($request,'img',UPLOADS_PATH.CATEGORY_PATH);
            $data['img'] = $img;
        }
    	// updating data in db
    	Category::where('id', $request->id)->update($data);
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }






     // deleteing row from db  ( soft delete )
    public function delete($id)
    {
         
        Category::findOrFail($id);
        Category::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }
















        // seo 

        public function seo($id)
        {
            $data['row'] = Category::findOrFail($id);
            $data['basePage']       = trans('admin.home');
            $data['secondName']     = trans('admin.category');  
            $data['thirdName']      = trans('admin.seoData') . ' / '.$data['row']['name'];
            $data['routeSecond']    = route('admin.get.category.index');
    
            $data['seoData'] =  json_decode($data['row']['seo']);
    
            return view('admin.category.seo')->with($data);
        }
    
    
    
    
         // storing data in db 
         public function seoUpdate(Request $request)
         {
             $data = Category::findOrFail($request->id);
             $row['seoHeader'] = nl2br($request->seoHeader);
             $row['seoFooter'] = nl2br($request->seoFooter);
             $allData = json_encode($row);
    
             $data->seo = $allData;
             $data->save();
             // updating data in db
            //  Product::where('id', $request->id)->update($data);
             $msg['success'] = trans('admin.msg.EditedSuccess');
             return response()->json($msg);
         }














        //  sorting elements     
        public function sort(Request $request)
        {
            $i = 1;
            foreach ($request->sort as  $value) 
            {
                $data = Category::find($value);
                $data->sort = $i;
                $data->save();
                $i++;
            }
            return back();
        }

     
    
     
         








}
