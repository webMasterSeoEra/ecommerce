<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Http\Requests\BranchRequest;

class BranchController extends Controller
{
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.branches');

        $data['branches'] = Branch::orderBy('id','DESC')->paginate(25);
        return view('admin.branch.index')->with($data);
    }





     // storing data in db 
     public function store(BranchRequest $request)
     {
         $data = $request->validated();
         Branch::create($data); // inserting data 
         $msg['success'] = trans('admin.addedSuccess');
         return response()->json($msg);
        
     }










     // storing data in db 
    public function update(BranchRequest $request)
    {
    	$data = $request->validated();
    	// updating data in db
    	Branch::where('id', $request->id)->update($data);
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }






     // deleteing row from db  ( soft delete )
    public function delete($id)
    {
         
        Branch::findOrFail($id);
        Branch::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }





}
