<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SubCat;
use App\Http\Requests\SubCatRequest;
use App\Models\Category;


class SubController extends Controller
{
    


    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.subcats');

        $data['categories'] = Category::select('id','name')
        ->orderBy('id','DESC')->get();
        $data['subcats'] = SubCat::orderBy('id','DESC')->paginate(25);
        return view('admin.subcat.index')->with($data);
    }





     // storing data in db 
     public function store(SubCatRequest $request)
     {
         $data = $request->validated();
         if($request->slug != ''){$data['slug'] = slug($request->slug);}
         else{$data['slug'] = slug($request->name).uniqid();}
         SubCat::create($data); // inserting data 
         $msg['success'] = trans('admin.addedSuccess');
         return response()->json($msg);
        
     }










     // storing data in db 
    public function update(SubCatRequest $request)
    {
    	$data = $request->validated();
        //  slug
        if($request->slug != ''){$data['slug'] = slug($request->slug);}
        else{$data['slug'] = slug($request->name).uniqid();}
    	// updating data in db
    	SubCat::where('id', $request->id)->update($data);
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }






     // deleteing row from db  ( soft delete )
    public function delete($id)
    {
         
        SubCat::findOrFail($id);
        SubCat::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }














        // seo 

        public function seo($id)
        {
            $data['row']            = SubCat::findOrFail($id);
            $data['basePage']       = trans('admin.home');
            $data['secondName']     = trans('admin.subcats');  
            $data['thirdName']      = trans('admin.seoData') . ' / '.$data['row']['name'];
            $data['routeSecond']    = route('admin.get.subcat.index');
    
            $data['seoData'] =  json_decode($data['row']['seo']);
    
            return view('admin.subcat.seo')->with($data);
        }
    
    
    
    
         // storing data in db 
         public function seoUpdate(Request $request)
         {
             $data = SubCat::findOrFail($request->id);
             $row['seoHeader'] = nl2br($request->seoHeader);
             $row['seoFooter'] = nl2br($request->seoFooter);
             $allData = json_encode($row);
    
             $data->seo = $allData;
             $data->save();
             // updating data in db
            //  Product::where('id', $request->id)->update($data);
             $msg['success'] = trans('admin.msg.EditedSuccess');
             return response()->json($msg);
         }
     
    
     







}
