<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Http\Requests\AdminRequest;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Classes\UploadClass;


class AdminController extends Controller
{
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.admins');

        $data['rows'] = Admin::orderBy('id','DESC')->paginate(25);
        return view('admin.admins.index')->with($data);
    }





     // storing data in db 
     public function store(AdminRequest $request)
     {
         $data = $request->validated();
         $data['password'] = bcrypt($request->password);
         Admin::create($data); // inserting data 
         $msg['success'] = trans('admin.addedSuccess');
         return response()->json($msg);
        
     }










     // storing data in db 
    public function update(AdminRequest $request)
    {
        $data = $request->validated();

        if($request->password && $request->password != '')
        {
            $data['password'] = bcrypt($request->password);
        }
        
        
    	// updating data in db
    	Admin::where('id', $request->id)->update($data);
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }






     // deleteing row from db  ( soft delete )
    public function delete($id)
    {
         
        Admin::findOrFail($id);
        Admin::findOrFail($id)->delete();
    	$message['status'] = true;
        return response()->json($message);

    }










}
