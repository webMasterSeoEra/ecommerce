<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\StaticPage;
use App\Http\Requests\StaticPageRequest;
use App\Helpers\Classes\UploadClass;
use Illuminate\Support\Facades\Storage;



class StaticController extends Controller
{
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.staticPages');

        $data['staticpages'] = StaticPage::orderBy('id','DESC')->paginate(25);
        return view('admin.static.index')->with($data);
    }





     // storing data in db 
     public function store(StaticPageRequest $request)
     {
        $data = $request->validated();

        // upload image of this module
        if($request->hasFile('img'))
        {
            $img = UploadClass::uploaImage($request,'img',UPLOADS_PATH.STATIC_PATH);
            $data['img'] = $img;
        }
        //  slug
        if($request->slug != ''){$data['slug'] = slug($request->slug);}
        else{$data['slug'] = slug($request->name).uniqid();}
    	StaticPage::create($data); // inserting data 
    	$msg['success'] = trans('admin.addedSuccess');
        return response()->json($msg);
        
     }










     // storing data in db 
    public function update(StaticPageRequest $request)
    {
        $data = $request->validated();
        
        
        
        $data['desc'] = $request->descEdit;

        $row = StaticPage::findOrFail($request->id);
        $row->name = $request->name;
        $row->type = $request->type;
        $row->desc = $request->descEdit;
        // dd($row->desc);
        // upload image of this module
        if($request->hasFile('img'))
        {
            $old = StaticPage::findOrFail($request->id);
            // delete old image from server
            if($old->img)
            {
                Storage::disk('public_uploads')->delete(STATIC_PATH.$old->img);
            }

            $img = UploadClass::uploaImage($request,'img',UPLOADS_PATH.STATIC_PATH);
            $row->img = $img;
        }
        //  slug
        if($request->slug != ''){$row->slug = slug($request->slug);}
        else{$row->slug = slug($request->name).uniqid();}
        $row->save();


    	// updating data in db
    	// StaticPage::where('id', $request->id)->update($data);
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }







     // deleteing row from db  ( soft delete )
     public function delete($id)
     {
         $old = StaticPage::findOrFail($id);
         // delete old image from server
         if($old->img)
         {
            Storage::disk('public_uploads')->delete(STATIC_PATH.$old->img);
         }
         
         
         StaticPage::findOrFail($id)->delete();
  
         $msg['success'] = trans('admin.msg.deleted_success');
         return response()->json($msg);
 
     }


















     public function sc()
     {
         $data['basePage'] = trans('admin.home');
         $data['secondName'] = trans('admin.staticWords');
 
         $sett= Setting::first();
         if(!$sett )
         {
             $sett = new Setting();
             $sett->name = "name of site";
             $sett->save();
         }
         $data['row'] = $sett;
         return view('admin.static.sc')->with($data);
     }
 
 
 
 
 
      // storing data in db 
     public function updateSc(Request $request)
     {
         
         $data = $request->except('_token','method','id');
         $data = json_encode($data);
         $sc = Setting::first();
         $sc->site_content = $data;
         $sc->save();
         $msg['success'] = trans('admin.msg.EditedSuccess');
         return response()->json($msg);
     }

     









}
