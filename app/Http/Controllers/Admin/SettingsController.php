<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Http\Requests\SettingsRequest;
use Storage;
use App\Helpers\Classes\UploadClass;

class SettingsController extends Controller
{


    
    public function index()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.settings');

        $sett= Setting::first();
        if(!$sett )
        {
            $sett = new Setting();
            $sett->name = "name of site";
            $sett->save();
        }
        $data['row'] = $sett;
        return view('admin.settings.index')->with($data);
    }





     // storing data in db 
    public function update(SettingsRequest $request)
    {
    	$data = $request->validated();
        $row = Setting::first();
            // upload image of this module
        if($request->hasFile('logo1'))
        {
            if($row->logo1)
            {
                Storage::disk('public_uploads')->delete(SETTINGS_PATH.$row->logo1);
            }

            $logo1 = UploadClass::uploaImage($request,'logo1',UPLOADS_PATH.SETTINGS_PATH);
            $data['logo1'] = $logo1;
        }

        Setting::where('id',$row->id)->update($data);
    
    	// updating data in db
    	
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }








    public function cond()
    {
        $data['basePage'] = trans('admin.home');
        $data['secondName'] = trans('admin.condUsed');

        $sett= Setting::first();
        if(!$sett )
        {
            $sett = new Setting();
            $sett->name = "name of site";
            $sett->save();
        }
        $data['row'] = $sett;
        return view('admin.settings.cond')->with($data);
    }





     // storing data in db 
    public function updateCond(Request $request)
    {
    	$request->validate([

            'descEdit' => 'required|string|max:10000',

        ]);
        $row = Setting::first();
        $row->cond_used = $request->descEdit;
        $row->save();
    
    	// updating data in db
    	
    	$msg['success'] = trans('admin.msg.EditedSuccess');
        return response()->json($msg);
    }








    // seo 

    public function seo()
    {
 
        $sett = Setting::first();
        if(!$sett)
        {
            $sett = new Setting();
            $sett->name = "name of site";
            $sett->save();
        }


        $data['basePage']       = trans('admin.home');
        $data['secondName']     = trans('admin.settings');  
        $data['thirdName']      = trans('admin.seoData');
        $data['routeSecond']    = route('admin.get.settings.index');
        $data['row'] = $sett;
        $data['seoData'] =  json_decode($sett->seo);
        return view('admin.settings.seo')->with($data);
    }




        // storing data in db 
        public function seoUpdate(Request $request)
        {
            $data = Setting::findOrFail($request->id);
            $row['seoHeader'] = nl2br($request->seoHeader);
            $row['seoFooter'] = nl2br($request->seoFooter);
            $allData = json_encode($row);

            $data->seo = $allData;
            $data->save();
            // updating data in db
        //  Product::where('id', $request->id)->update($data);
            $msg['success'] = trans('admin.msg.EditedSuccess');
            return response()->json($msg);
        }
         


   





}
