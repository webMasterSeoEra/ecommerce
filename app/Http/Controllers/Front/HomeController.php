<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\Product;
use App\Models\Company;
use App\Models\StaticPage;
use App\Models\Branch;
use App\Models\Client;
use Cart;
use function GuzzleHttp\json_decode;
use App\Models\Setting;
use App\Models\Notification;


class HomeController extends Controller
{
    public function index()
    {

        
        // Cart::clear();
        $data['sliders'] = Slider::select('name','link','img','small_desc')
        ->take(10)->get();

        $data['companies'] = Company::select('img')
        ->take(10)->get();


        // special products 
        $data['specials'] = Product::select('id','name','img','img2','price','slug','category_id')
        ->where('special','yes')->take(8)
        ->orderBy('id','DESC')->get();



        // views products 
        $data['viewProducts'] = Product::select('id','name','img','price','slug','category_id')
        ->take(4)->orderBy('views','DESC')->get();

        // sell number of products 
        $data['sellNum'] = Product::select('id','name','img','price','slug','category_id')
        ->take(4)->orderBy('selll_number','DESC')->get();

        // latest products 
        $data['latest'] = Product::select('id','name','img','price','slug','category_id')
        ->take(4)->latest()->get();

        // dd($seo_setting->seo);
    
       

        return view('front.index')->with($data);
    }




    public function activation($activation)
    {
        $row = Client::where('token',$activation)->first();
        if($row)
        {
            $row->status = "yes";
            $row->save();
        }
        return redirect(route('front.get.authClient.login'));
    }

















    


    public function cond()
    {
        return view('front.static.cond');
    }




    public function about()
    {
        return view('front.static.about');
    }



    public function contact()
    {
        return view('front.static.contact');
    }



    public function site($slugPage)
    {
        $check = StaticPage::where('slug',$slugPage)->first();
       

        if($check)
        {
            $data['row'] = $check;
            return view('front.static.site')->with($data);
        }
        
    }




    public function branches()
    {
        $data['branches'] = Branch::orderBy('id','DESC')->paginate(12);
        return view('front.static.branches')->with($data);
    }













    public function autosearch(Request $request)
    {
        // dd($request->name);
        if($request->name != null)
        {
            $data['rows'] = Product::where('name','LIKE','%'.$request->name.'%')->orderBy('id','DESC')->get();
            return view('front.inc.ajaxSearch')->with($data);
        }
        return "";
        
    }









}
