<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Models\SubCat;

class CatController extends Controller
{
    public function showCat($slugCat)
    {
        $check = Category::where('slug',$slugCat)->first();
        if($check)
        {
            $data['products'] = Product::where('category_id',$check->id)->paginate(12);
            $data['row'] = $check;
            return view('front.product.cat')->with($data);
        }
        
    }





    public function showSubCat($slugSubCat)
    {
        $check = SubCat::where('slug',$slugSubCat)->first();
        if($check)
        {
            $data['products'] = Product::where('sub_cat_id',$check->id)->paginate(12);
            $data['row'] = $check;
            return view('front.product.sub')->with($data);
        }
        
    }





    public function filter(Request $request)
    {
        if($request->value)
        {
            switch ($request->value) 
            {
                case 'order':
                    $data['products'] = Product::orderBy('selll_number','DESC')->paginate(9);
                    break;
                case 'price':
                    $data['products'] = Product::orderBy('price','ASC')->paginate(9);
                    break;
                case 'view':
                    $data['products'] = Product::orderBy('views','DESC')->paginate(9);
                    break;
                
                default:
                     $data['products'] = Product::orderBy('id','DESC')->paginate(9);
                    break;
            }
        }
        else
        {
            $data['products'] = Product::orderBy('id','DESC')->paginate(9);
        }

        $data['row'] = (object) ['name'=>"تصفية",'id'=>1];
        return view('front.product.cat')->with($data);
        
    }






}
