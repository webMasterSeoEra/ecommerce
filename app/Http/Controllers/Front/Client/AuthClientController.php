<?php

namespace App\Http\Controllers\Front\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Setting;

use Cart;
use Mail;
use App\Mail\ActivationMail;
use App\Mail\ForgotMail;
use App\Models\Notification;



class AuthClientController extends Controller
{
    

    // show login blade 
    public function login()
    {

        $data['page'] = "login";
    	return view('front.client.auth.login')->with($data);


    }// end show login function 



     // login authencation 
  	public function do_login(Request $request)
    {

            $request->validate([
                'email'=>'required|email|max:100',
                'password'=>'required|max:50',
            ]);
            $remember = request()->has('rememberme') ? true: false;
            if(\Auth::guard('client')->attempt(['email'=>request('email'),'password'=>request('password'),
            'status'=>'yes'],$remember))
            {
                if(Cart::getContent()->count())
                {
                    return redirect(route('front.get.cart.show'));
                }
                return redirect(route('front.get.home.index'));
            }
            else
            {
                return back()->withErrors(trans('front.msg.notCorrect'));
            }

    }// end do login function 
















    // show register blade 
    public function register()
    {
        $data['page'] = "reg";
    	return view('front.client.auth.login')->with($data);

    }// end show login function 




    // storing data in db 
    public function do_register(Request $request)
    {
    
            $request->validate([
                    'name'                      => 'required|string|max:190',
                    'email'                     => 'required|email|unique:clients,email|max:100',
                    'password'                  => 'required|string|min:6|max:50',
                    'confirm_password'          => 'required|string|min:6|same:password|max:50',
                ]);
            $data = $request->except('confirm_password','_token');
            $token = str_random(32);
            $data['password'] = bcrypt($request->password);
            $data['token'] = $token;
     
            try 
            {
                $dataMail['token'] = $token;
                Mail::to($request->email)->send(new ActivationMail($dataMail));
                //  notification of order
                $not = new Notification();
                $not->type = "client";
                $not->save();
            } catch (Exception $ex) {
                return back()->withErrors(trans('front.msg.notCorrectEmail'));
            }



            
            Client::create($data); // inserting data 

            session()->flash('status',trans('front.msg.register_success'));
            return back();
   
    }









    public function forgot()
    {
        return view('front.client.auth.forgot');
    }


    public function checkForgot(Request $request)
    {

            $row = $request->validate([
                'email'=>'required|email|max:100',
            ]);
            if($row)
            {
                try 
                {
                    $dataMail['token'] = str_random(32);
                    Mail::to($request->email)->send(new ForgotMail($dataMail));
                    \DB::table('password_resets')->insert(['email'=>$request->email,
                    'token'=>$dataMail['token']]);

                    session()->flash('status',trans('front.msg.checkYourMail'));
                    return back();
                    
                } catch (Exception $ex) 
                {
                    return back()->withErrors(trans('front.msg.notCorrectEmail'));
                }
            }
            else
            {
                return back()->withErrors(trans('front.msg.emailNotFound'));
            }

    }// end do login function 



    public function chPassword($token)
    {
        $check = \DB::table('password_resets')->where('token',$token)->first();
        if($check)
        {
            $data['token'] = $check->token;
            $data['email'] = $check->email;
            return view('front.client.auth.newPassword')->with($data);
        }
    }



    public function newPassword(Request $request)
    {

           $request->validate([
                'password'=>'required|string|max:50|min:6',
                'confirm_password'=>'required|string|max:50|min:6|same:password',
            ]);


            $check = \DB::table('password_resets')
            ->where('token',$request->token)->where('email',$request->email)->first();
            if($check)
            {
                $row = Client::where('email',$request->email)->first();
                if($row)
                {
                    $row->password = bcrypt($request->password);
                    $row->save();

                    \DB::table('password_resets')->where('email',$request->email)->delete();

                    return redirect(route('front.get.authClient.login'));
                    // session()->flash('status',trans('front.msg.passwordChanged'));
                    // return back();
                }
                
            } 

            return redirect(route('front.get.home.index'));



    }// end 



    




    // admin logout 

    public function logout()
    {
       
    	clientAuth()->logout();
    	return redirect(route('front.get.home.index'));
    } // end logout function 






}
