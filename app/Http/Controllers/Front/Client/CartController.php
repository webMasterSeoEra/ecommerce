<?php

namespace App\Http\Controllers\Front\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Product;
use App\Models\Order;
use Cart;

class CartController extends Controller
{
    public function add(Request $request)
    {

        // dd($request->all());
        $product = Product::findOrFail($request->prodId);

        if($request->qty > $product->stock)
        {
            $data['message'] = trans("front.msg.stockFinish");
            return response()->json($data);
        }
        // array format
        if(!Cart::get($request->prodId))
        {
            Cart::add(array(
                'id' => $request->prodId,
                'name' => $product->name,
                'price' => $product->price,
                'quantity' => $request->qty,
                'attributes' => array('img'=> $product->img)
            ));

        }
        else
        {
            Cart::remove($request->prodId);
            Cart::add(array(
                'id' => $request->prodId,
                'name' => $product->name,
                'price' => $product->price,
                'quantity' => $request->qty,
                'attributes' => array('img'=> $product->img)
            ));

        }

        return view('front.product.ajax.cartContent');
    }




    // show cart 
    public function show()
    {
        return view('front.product.cart');
    }



    // delete item from cart  
    public function update(Request $request)
    {
        $id = $request->prodId;
        $type = $request->type;
        // check from product ( if exist or not )
        Product::findOrFail($id);

        if($type == "p")
        {
            //  update a product's quantity
            Cart::update($id, array(
                'quantity' => 1, 
              ));
        }
        else
        {
            //  update a product's quantity
            Cart::update($id, array(
                'quantity' => -1, 
              ));
        }

        $setting = Setting::first();

        

        $data['total'] = Cart::getSubTotal();
        $data['subtotal'] = Cart::getSubTotal() + $setting->additional_value;
        $data['itemPrice']  = Cart::get($id)->getPriceSum();
        // $data['subtotal'] = Cart::getSubTotal();
        return response()->json($data);
        
    }






    // delete item from cart  
    public function delete($id)
    {
        // check from product ( if exist or not )
        Product::findOrFail($id);
        Cart::remove($id);
        return back();
    }



    








}
