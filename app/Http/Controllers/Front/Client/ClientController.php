<?php

namespace App\Http\Controllers\Front\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Favorit;
use App\Models\Client;
use App\Models\Order;
use App\Models\OrderReverce;


class ClientController extends Controller
{
    public function account()
    {
        return view('front.client.profile');
    }



    public function page($pageName)
    {
        switch ($pageName) 
        {
            case 'myorders':

                $data['orders'] = Order::where('client_id',client()->id)
                ->orderBy('id','DESC')->get();
                return view('front.client.page')->with($data);
                break;

            case 'myfavorits':

                $data['favorits'] = Favorit::where('client_id',client()->id)
                ->orderBy('id','DESC')->get();
                return view('front.client.page')->with($data);
                break;

            case 'reverced':

                $data['orders'] = OrderReverce::where('client_id',client()->id)
                ->orderBy('id','DESC')->get();

                $data['cancelled'] = Order::where('client_id',client()->id)
                ->where('status',"refused")
                ->orderBy('id','DESC')->get();

                return view('front.client.page')->with($data);
                break;
            
            default:
                # code...
                break;
        }
        return view('front.client.page');
    }




    public function editInfo(Request $request)
    {
        $request->validate([
            'name'                      => 'required|string|max:190',
            'phone'                     => 'nullable|numeric',
            'mobile'                    => 'nullable|numeric',
        ]);

        $data = Client::findOrFail(client()->id);
        $data->name = $request->name;
        $data->phone = $request->phone;
        $data->mobile = $request->mobile;
        $data->save();


        $msg['success'] = trans('admin.addedSuccess');
        return response()->json($msg);


    }






    public function changePassword(Request $request)
    {
        $request->validate([
            'old_password'                  => 'required|string|min:6|max:50',
        ]);

        if(\Hash::check($request->old_password, client()->password))
        {
            $request->validate([
                'password'                  => 'required|string|min:6|max:50',
                'confirm_password'          => 'required|string|min:6|same:password|max:50',
            ]);
    
            $data = Client::findOrFail(client()->id);
            $data->password = bcrypt($request->password);
            $data->save();
    
    
            $msg['success'] = trans('admin.addedSuccess');
            return response()->json($msg);
        }
        else
        {
            $returnData = array(
                'status' => 'error',
                'message' => ' كلمة المرور غير صحيحة !'
            );
            return \Response::json($returnData, 500);
        }

    }



    public function editAddress(Request $request)
    {
        $request->validate([
            'address'                   => 'required|string|max:190',
            'address2'                  => 'nullable|string|max:190',

        ]);


        $data = Client::findOrFail(client()->id);
        $data->address = $request->address;
        $data->address2 = $request->address2;
        $data->save();


        $msg['success'] = trans('admin.addedSuccess');
        return response()->json($msg);


    }













    // wishlist 


    public function wishlist($id)
    {

        Product::findOrFail($id);
        $data = Favorit::where('client_id',client()->id)->where('product_id',$id)->first();
        if(!$data)
        {
            $data = new Favorit();
            $data->product_id = $id;
            $data->client_id = client()->id;
            $data->save();

            $message['success'] = true;
        }
    
        $message['success2'] = false;
        return response()->json($message);
    }

    


}
