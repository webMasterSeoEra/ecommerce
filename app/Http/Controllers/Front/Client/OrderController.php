<?php

namespace App\Http\Controllers\Front\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderReverce;
use App\Models\OrderContent;
use App\Models\Notification;
use Mail;
use App\Mail\ReverceOrderMail;
use App\Mail\DeleteOrdereMail;


class OrderController extends Controller
{
    
    public function showOrder($id)
    {
        $data['row'] = Order::where('client_id',client()->id)->findOrFail($id);
        $data['products'] = OrderContent::Where('order_id',$id)->get();
        return view('front.client.order')->with($data);

    }


    //  reverce   

    public function reverce($id)
    {
        $checkReverce = OrderReverce::where('order_id',$id)->where('client_id',client()->id)->first();
        if($checkReverce){return redirect(route('front.get.home.index'));}
        $check  = Order::where('id',$id)->where('client_id',client()->id)->first();
        if($check)
        {
            $data['row'] = $check;
            return view('front.client.reverce')->with($data);
        }
    }


    public function doReverce(Request $request)
    {

        $check = Order::where('id',$request->order)->where('client_id',client()->id)->first();

        if($check)
        {
            $request->validate([
                'message'=>'required|string|max:5000',
                'type'=>'required|string|in:productErrorRespond,productExpired,ProductError,productErrorExist,productErrorOther',
                'order'=>'required|numeric|exists:orders,id',
            ]);
            $data = new OrderReverce();
            $data->message      = $request->message;
            $data->order_id     = $request->order;
            $data->type         = $request->type;
            $data->client_id    = client()->id;
            $data->save();


            //  notification of refused order
            $not = new Notification();
            $not->type = "refused";
            $not->save();

            // sending mail to client 
            $msg['order'] = $check;
            Mail::to(client()->email)->send(new ReverceOrderMail($msg));
            



            session()->flash('status',trans('front.msg.reverceOrderSuccess'));
            return redirect(route('front.get.client.page',['myorders']));
        }

        return redirect(route('front.get.home.index'));

        


    }


    public function delete($id)
    {

        $check = Order::where('id',$id)->where('client_id',client()->id)->first();
        if($check)
        {
            $check->status = "refused";
            $check->save();

            //  notification of refused order
            $not = new Notification();
            $not->type = "cancelled";
            $not->save();

            // sending mail to client 
            $msg['order'] = $check;
            Mail::to(client()->email)->send(new DeleteOrdereMail($msg));


            return back();

        }

        return back();

    }
}
