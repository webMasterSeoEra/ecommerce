<?php

namespace App\Http\Controllers\Front\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderContent;
use App\Models\Setting;
use App\Models\Cupon;
use App\Models\OrderCupon;
use App\Models\ShippCompany;
use App\Models\ShippWay;
use App\Models\City;
use Mail;
use App\Mail\OrderMail;
use App\Models\Notification;




class CheckoutController extends Controller
{
    public function show()
    {
        if(\Cart::getContent()->count())
        {
            $data['shippCompanies'] = ShippCompany::select('id','name')->get();
            $data['shippWay'] = ShippWay::select('id','name')->get();
            $data['cities'] = City::select('id','name','discount_text','min_number','discount_value')->get();
            return view('front.product.checkout')->with($data);
        }
        return redirect(route('front.get.home.index'));
        
    }



    public function do_checkout(Request $request)
    {


        $request->validate([

            'name'                  => 'required|string|max:100',
            'mobile'                => 'required|numeric|digits_between:6,14',
            'bulding_number'        => 'required|string|max:100',
            'street_name'           => 'required|string|max:100',
            'district_name'         => 'required|string|max:100',
            'city'                  => 'required|numeric|exists:cities,id',
            'notes'                 => 'nullable|string|max:500',
            'shippCompany'          => 'required|numeric|exists:shipp_companies,id',
            'shippWay'              => 'required|numeric|exists:shipp_ways,id',
            'cupon_number'          => 'nullable|string|exists:cupons,cupon_number',
            'accept'                => 'required',

        ]);


        $data = new Order();
        $data->client_id            = client()->id;
        $data->name                 = $request->name;
        $data->bulding_number       = $request->bulding_number;
        $data->street_name          = $request->street_name;
        $data->district_name        = $request->district_name;
        $data->city                 = $request->city;
        $data->mobile               = $request->mobile;
        $data->payment_method       = "cash";
        $data->notes                = $request->notes;
        $data->shipp_company_id     = $request->shippCompany;
        $data->shipp_way_id         = $request->shippWay;
        $data->additional_value_now = Setting::first()->additional_value;
        //  insert data of cupon 
        if($request->cupon_number)
        {
            $dataCupon = Cupon::where('cupon_number',$request->cupon_number)->first();
            $data->cupon_id = $dataCupon->id;
            $data->percent_now = $dataCupon->percent;
            
        }
        // get total price 
        $total_price = 0 ;
        foreach(\Cart::getContent() as $item)
        {
            $total_price += $item->quantity * $item->price;
        }
        $data->total_price = $total_price;
        $data->save();

        
        // insert content of order 
        foreach(\Cart::getContent() as $item)
        {
            $do = new OrderContent;
            $do->order_id = $data->id;
            $do->product_id = $item->id;
            $do->qty = $item->quantity;
            $do->price = $item->price;
            $do->save();
        }

        $msg['success'] = "Added Success";

        
        Mail::to(client()->email)->send(new OrderMail($msg));
        \Cart::clear();

        //  notification of order
        $not = new Notification();
        $not->type = "order";
        $not->save();
        return response()->json($msg);


    }
}
