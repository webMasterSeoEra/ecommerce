<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Models\Rate;
use App\Models\Order;

class ProductController extends Controller
{
    
    public function showProd($slugProduct,$slugCat)
    {
        $chCat = Category::where('slug',$slugCat)->first();
        $chProduct = Product::where('slug',trim($slugProduct))->first();
 
        if($chProduct && $chCat)
        {
            $data['row'] = $chProduct;
            $data['cat'] = $chCat;

            //  updating view of this product  
            $chProduct->views = $chProduct->views + 1;
            $chProduct->save();
            // special products 
            $data['specials'] = Product::select('id','name','img','img2','price','slug','category_id')
            ->where('special','yes')->take(8)
            ->orderBy('id','DESC')->get();
            return view('front.product.show')->with($data);
        }

    }




    public function search(Request $request)
    {
        if($request->search)
        {
            $data['products'] = Product::where('tags','LIKE','%'.$request->search.'%')
            ->paginate(1);
        }
        else 
        {
            $data['products'] = Product::paginate(12);
        }
        return view('front.product.search')->with($data);
    }








    // rating
    public function rate(Request $request)
    {
        // check if this product exist and if this client make an order for this product
        Product::findOrFail($request->prod);
        $checkRate = Rate::where('product_id',$request->prod)->where('client_id',client()->id)->first();
        if($checkRate)
        {
            $checkRate->rate = $request->value;
            $checkRate->save();
        }
        else 
        {
            $data = new Rate();
            $data->client_id = client()->id;
            $data->product_id = $request->prod;
            $data->rate = $request->value;
            $data->save();
        }
        $message['success'] = "success";
        return response()->json($message);
    }










}
