<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use App\Models\Message;

use Mail;
use App\Mail\SubscribeMail;
use App\Models\Notification;


class MessageController extends Controller
{


    // newsletter
    public function subscribe(Request $request)
    {

        $msg['success'] = "Added Success";
        $request->validate(['email'=> 'required|email|unique:newsletters,email']);
        Mail::to($request->email)->queue(new SubscribeMail($msg));
 
        $data = new Newsletter();
        $data->email = $request->email;
        $data->save();
        $message['success'] = "success";
        return response()->json($message);
    }









    
     // send message   
     public function sendMessage(Request $request)
     {
 
         if($request->ajax())
         {
            $request->validate([

                'name' => 'required|string|max:100',
            //  'phone' => 'required|numeric|digits_between:6,15',
                'email' => 'required|email|max:100',
                'message' => 'required|string|max:2000',
            ]);
 
            $data = $request->except("_token");
 
            Message::create($data);
            //  notification of order
            $not = new Notification();
            $not->type = "complaint";
            $not->save();
            $message['success'] = trans('frontSite.contactusMessage');
            return response()->json($message);
         }
     }
}
