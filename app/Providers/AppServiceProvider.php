<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Setting;
use App\Models\Category;
use App\Models\StaticPage;
use Illuminate\Support\Facades\Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);



        // if (!\Cache::has('setting'))
        // {
        //     Cache::put('setting', Setting::first(),60);
        //     Cache::put('cats', Category::select('id','name','slug')->orderBy('id','ASC')->get(),60);
        //     Cache::put('staticService', StaticPage::select('id','name','slug')->where('type','service')
        //     ->orderBy('id','ASC')->get(),60);
        //     Cache::put('staticInfo', StaticPage::select('id','name','slug')->where('type','info')
        //     ->orderBy('id','ASC')->get(),60);
        // }


        // \View::share([
        //     'set'=>Cache::get('setting'),
        //     'cats'=>Cache::get('cats'),
        //     'staticInfo'=>Cache::get('staticInfo'),
        //     'sc'=>json_decode(Cache::get('setting')->site_content),
        //     'staticService'=>Cache::get('staticService')]);


            // $arr_ip = geoip()->getLocation($_SERVER['REMOTE_ADDR']);
            // $arr_ip = geoip()->getLocation("2.91.255.255");

            // if($arr_ip->country == "Saudi Arabia")
            // {
            //     // dd($arr_ip);
            // }
        



        $setting = Setting::first();
        $cats = Category::select('id','name','slug')->orderBy('sort','ASC')->get();
        $staticService = StaticPage::select('id','name','slug')->where('type','service')
        ->orderBy('id','ASC')->get();
        $staticInfo = StaticPage::select('id','name','slug')->where('type','info')
        ->orderBy('id','ASC')->get();
        
        \View::share([
            'set'=>$setting,
            'cats'=>$cats,
            'staticInfo'=>$staticInfo,
            'sc'=>json_decode($setting->site_content),
            'staticService'=>$staticService]);


    }
}
