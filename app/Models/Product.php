<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Favorit;

class Product extends Model
{

    protected $fillable = ['name','img','category_id','sub_cat_id','price'
                            ,'desc','special','stock','tags','slug','size_id','fit'];


    public function cat()
    {
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    public function size()
    {
        return $this->hasOne('App\Models\Size','id','size_id');
    }



    public function sub()
    {
        return $this->hasOne('App\Models\SubCat','id','sub_cat_id');
    }


    public function images()
    {
        return $this->hasMany('App\Models\ProductImage','product_id','id');
    }




    public  function checkWishlist($prod_id,$client_id)
    {
        return Favorit::where('product_id',$prod_id)->where('client_id',$client_id)->first();
      
    }


    public function rating()
    {
        return $this->hasMany('App\Models\Rate','product_id','id');
    }






}
