<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCat extends Model
{


    protected $fillable = [
        'name','slug','category_id'
    ];


    public function cat()
    {
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    
    public function product()
    {
        return $this->hasMany('App\Models\Product','sub_cat_id','id');
    }



}
