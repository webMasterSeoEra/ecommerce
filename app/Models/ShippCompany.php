<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippCompany extends Model
{
    protected $fillable = [
        'name','img','price'
    ];
}
