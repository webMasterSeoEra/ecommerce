<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'password','token'
    ];


    public function order()
    {
        return $this->hasMany('App\Models\Order','client_id','id');
    }


}
