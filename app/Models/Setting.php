<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['name','email','mobile1','facebook',
            'instagram','twitter','youtube','address1',
            'fax','logo1','map','additional_value'];
}
