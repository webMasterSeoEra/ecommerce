<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\OrderReverce;
class Order extends Model
{


    
    public function content()
    {
        return $this->hasMany('App\Models\OrderContent','order_id','id');
    }



    


    public function client()
    {
        return $this->hasOne('App\Models\Client','id','client_id');
    }

    public function cityName()
    {
        return $this->hasOne('App\Models\City','id','city');
    }

    public function shippCampany()
    {
        return $this->hasOne('App\Models\ShippCompany','id','shipp_company_id');
    }

    public function shippWay()
    {
        return $this->hasOne('App\Models\ShippWay','id','shipp_way_id');
    }

    public function cupon()
    {
        return $this->hasOne('App\Models\Cupon','id','cupon_id');
    }

    public function reverce($order_id,$client_id)
    {
        return  OrderReverce::where('order_id',$order_id)->where('client_id',$client_id)->first();
    }
}
