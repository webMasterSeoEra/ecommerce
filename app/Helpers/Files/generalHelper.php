<?php 








// display image from uploads folder
if (!function_exists('getImg')) 
{
    function getImg($path) 
    {
        return url('uploads/'.$path.'/');
    }
}





// select option from select element
if (!function_exists('typeSelect')) 
{
    function typeSelect($select_id,$item_id) 
    {
        if($select_id == $item_id)
        {
            return "selected";
        }
    }
}




// display json data in blade 
if (!function_exists('json_data')) 
{
    function json_data($json_data,$field) 
    {
        if($json_data)
        {
            if(isset($json_data->$field))
            {
                return $json_data->$field;
            }
            else
            {
                return '';
            }
        }
    }
}




define("UPLOADS_PATH", 'uploads/');
define("SETTINGS_PATH", 'settings/');
define("CATEGORY_PATH", 'category/');
define("SUBCATEGORY_PATH", 'subCategory/');
define("PRODUCT_PATH", 'products/');
define("PIMAGE_PATH", 'products/images/');
define("COMPANY_PATH", 'company/');
define("SLIDER_PATH", 'slider/');
define("STATIC_PATH", 'static/');
define("SHIPPCOMPANY_PATH", 'shipmentComapny/');












