<?php 





// define link for assets of front ( styles and scripts )
if (!function_exists('furl')) 
{
    function furl()
    {
         return url('front');
    }
}



if (!function_exists('clientAuth')) 
{
    function clientAuth()
    {
        return auth()->guard('client');
    }
}


if (!function_exists('client')) 
{
    function client()
    {
        return auth()->guard('client')->user();
    }
}



