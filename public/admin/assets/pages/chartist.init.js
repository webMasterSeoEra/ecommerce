var chart=new Chartist.Line("#smil-animations", {
    labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"], series: [[12, 9, 7, 8, 5, 4, 6, 2, 3, 3, 4, 6], [4, 5, 3, 7, 3, 5, 5, 3, 4, 4, 5, 5], [5, 3, 4, 5, 6, 3, 3, 4, 5, 6, 3, 4], [3, 4, 5, 6, 7, 6, 4, 5, 6, 7, 6, 3]]
}

, {
    low:0, axisY: {
        position: "end"
    }
    , plugins:[Chartist.plugins.tooltip()]
}

),
seq=0,
delays=80,
durations=500;
chart.on("created", function() {
    seq=0
}

),
chart.on("draw", function(e) {
    if(seq++, "line"===e.type)e.element.animate( {
        opacity: {
            begin: seq*delays+1e3, dur: durations, from: 0, to: 1
        }
    }
    );
    else if("label"===e.type&&"x"===e.axis)e.element.animate( {
        y: {
            begin: seq*delays, dur: durations, from: e.y+100, to: e.y, easing: "easeOutQuart"
        }
    }
    );
    else if("label"===e.type&&"y"===e.axis)e.element.animate( {
        x: {
            begin: seq*delays, dur: durations, from: e.x-100, to: e.x, easing: "easeOutQuart"
        }
    }
    );
    else if("point"===e.type)e.element.animate( {
        x1: {
            begin: seq*delays, dur: durations, from: e.x-10, to: e.x, easing: "easeOutQuart"
        }
        , x2: {
            begin: seq*delays, dur: durations, from: e.x-10, to: e.x, easing: "easeOutQuart"
        }
        , opacity: {
            begin: seq*delays, dur: durations, from: 0, to: 1, easing: "easeOutQuart"
        }
    }
    );
    else if("grid"===e.type) {
        var t= {
            begin: seq*delays, dur: durations, from: e[e.axis.units.pos+"1"]-30, to: e[e.axis.units.pos+"1"], easing: "easeOutQuart"
        }
        , a= {
            begin: seq*delays, dur: durations, from: e[e.axis.units.pos+"2"]-100, to: e[e.axis.units.pos+"2"], easing: "easeOutQuart"
        }
        , i= {}
        ;
        i[e.axis.units.pos+"1"]=t, i[e.axis.units.pos+"2"]=a, i.opacity= {
            begin: seq*delays, dur: durations, from: 0, to: 1, easing: "easeOutQuart"
        }
        , e.element.animate(i)
    }
}

),
chart.on("created", function() {
    window.__exampleAnimateTimeout&&(clearTimeout(window.__exampleAnimateTimeout), window.__exampleAnimateTimeout=null), window.__exampleAnimateTimeout=setTimeout(chart.update.bind(chart), 12e3)
}

),
new Chartist.Line("#simple-line-chart", {
    labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"], series: [[12, 9, 7, 8, 5], [2, 1, 3.5, 7, 3], [1, 3, 4, 5, 6]]
}

, {
    fullWidth:!0, chartPadding: {
        right: 40
    }
    , axisY: {
        position: "end"
    }
    , plugins:[Chartist.plugins.tooltip()]
}

);
var times=function(e) {
    return Array.apply(null, new Array(e))
}

,
data=times(52).map(Math.random).reduce(function(e, t, a) {
    return e.labels.push(a+1), e.series.forEach(function(e) {
        e.push(100*Math.random())
    }
    ), e
}

, {
    labels:[], series:times(4).map(function() {
        return new Array
    }
    )
}

),
options= {
    showLine:!1,
    axisX: {
        labelInterpolationFnc:function(e, t) {
            return t%13==0?"W"+e: null
        }
    }
    ,
    axisY: {
        position: "end"
    }
}

,
responsiveOptions=[["screen and (min-width: 640px)",
{
    axisX: {
        labelInterpolationFnc:function(e, t) {
            return t%4==0?"W"+e: null
        }
    }
}

]];







new Chartist.Line("#scatter-diagram", data, options, responsiveOptions),

new Chartist.Line("#chart-with-area", {
    labels: [1, 2, 3, 4, 5, 6, 7, 8], series: [[5, 9, 7, 8, 5, 3, 5, 4]]
}

, {
    low:0, showArea:!0, axisY: {
        position: "end"
    }
    , plugins:[Chartist.plugins.tooltip()]
}

);
data= {
    labels: ["Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], series: [[5, 4, 3, 7, 5, 10, 3, 4, 8, 10, 6, 8], [3, 2, 9, 5, 4, 6, 4, 6, 7, 8, 7, 4]]
}

,
options= {
    seriesBarDistance:10,
    axisY: {
        position: "end"
    }
}

,
responsiveOptions=[["screen and (max-width: 640px)",
{
    seriesBarDistance:5,
    axisX: {
        labelInterpolationFnc:function(e) {
            return e[0]
        }
    }
}

]];



